# Listing of the tracking config file lines #

* [Go Back to README](README.MD)

###trackingpool###
This set of values is for configuration of the datasource connection to the
database. We must have each of the sub values within the __trackingpool__ tags.
Otherwise, your configuration will fail. They don't need to be in any particular
order though.

	<trackingpool>
		<timeout value="6500" />
		<minpool value="2" />
		<maxpool value="30" />
		<maxsize value="65" />
		<username value="root" />
		<password value="r00tp8ssw0rd" />
		<validationquery value="SELECT 1;" />
		<driverclass value="com.mysql.jdbc.Driver" />
		<url value="jdbc:mysql://localhost:3306/tracking_db" />
	</trackingpool>

###timeout###

	<timeout value="6500" />
	
Timeout value is how long the connection to the database can be idle before it
is considered to be timed out.

###minpool###

	<minpool value="2" />

The minimum number of simultaneous connections to the database that will be
kept alive regardless of traffic to the database.

###maxpool###

	<maxpool value="30" />

The maximum number of simultaneous connections that will be opened to the
database depending on traffic.

###username###

	<username value="root" />

The user name used to connect to the database. This is whatever you have
configured when you installed the database.

###password###

	<password value="r00tp8ssw0rd" />

This is the password used for the user above.

###validationquery###

	<validationquery value="SELECT 1;" />

This is the query sent to the database to verify that the connection is still
alive.

###driverclass###

	<driverclass value="com.mysql.jdbc.Driver" />

This is the namespace of the driver imported using the pom.xml dependencies (no
reason to change this unless the pom gets changed)

###url###

	<url value="jdbc:mysql://localhost:3306/tracking_db" />

This is the connection string used to connect to the database. If you didn't
name your schema as described in the README file, then this will need to be
changed to reflect that. Or if you want to connect to a database on a different
machine. Look up the connection string for the mysql driver for more information
on how this should be changed.

###datalocation###
	
	<datalocation value="~/Downloads/swsc_data/text_files" />

This is whre the .txt files with the object data are stored. The ~ is meant to
signal that you are starting relative to the user's home directory.

###imagedbcache###
		
	<imagedbcache max="500" />
 
 This is how many objects we will cache the image information in memory for. 
 
 ###expid###
 
	<expid value="1" />

This is used to give different labels to different runs of the tracking
program. The results of the previous run of tracking are deleted for the table
with this extension if you don't change the value.
	
###numfeatures###

	<numfeatures value="20" />

This is the number of feature values to use when doing visual comparison. The
feature ranking is first done and then however many (1-90) are set here, are
used from the top of the ranking.

#Multipliers#
These multiplier values were searched for quite exhaustively, change them and
they may improve or degrade the results. We suggest leaving them alone unless
you made significant changes to the program.

###entexmult###

	<entexitmult value="5.0" />

This is the multiplier for the enter and exit edge weights on the track graph.

###obsmult###
	
	<obsmult value="65.0" />
	
This is the multiplier for the observation edge weight on the track graph.

###assocmult###
	
	<assocmult value="85.0" />
	
This is the multiplier for the association edge weight on the track graph.

###appearweight###

	<appearweight value="0.5" />

This is a weight for how much the appearance model affects the association edge
value prior to its weight multiplier.

###skipweight###
	
	<skipweight value="0.8" />

This is a weight for how much the frame skip model affects the association edge
value prior to its weight multiplier.

###motionweight###

	<motionweight value="0.1" />
	
This is a weight for how much the motion model affects the association edge value
prior to its weight multiplier.

###startdatetime###
	
	<startdatetime value="2012-01-01 00:00:00" />

This is the beginning of the date range you wish to work with. This and the end
tells what range of objects to pull out of the database. That way you may run
trackig on a portion of the data if desired.

###enddatetime###	

	<enddatetime value="2015-01-01 00:00:01" />

Same as startdatetime but for the end of the range you want to run tracking on.
	
###indexing###

	<indexing>
		<regiondim value="128" />
		<regiondiv value="32" />
	</indexing>

This is used for indexing objects in an reduced spatial region to speed up
spatial queries on the data. All of the objects are pulled from the database and
kept in memory. The locations are index into first bins that represent where
they are within the image, and then temporally. This helps speed up looking for
objects that intersect in a spatiotemporal manner with the search region for the
next detection to possibly link to.

The __regiondim__ is what dimension the reduced region will be.  In this case
it will be a 128X128 grid.

The __regiondiv__ is what we need to divide the pixel location of the object by
to say which bin it falls in. Since 128 is 1/32 of the original space
(4096X4096), we need to divide by 32 in this case.

The objects are check with their full resolution values. This is a filter
refine type of operation.

###histograms###
	
	<histograms>
		<parameter value="1">
			<lowerlimit value="0.75" />
			<upperlimit value="8.3" />
		</parameter>
		<parameter value="2">
			<lowerlimit value="0" />
			<upperlimit value="256" />
		</parameter>
		<parameter value="3">
			<lowerlimit value="0" />
			<upperlimit value="110" />
		</parameter>
		<parameter value="4">
			<lowerlimit value="0.75" />
			<upperlimit value="2" />
		</parameter>
		<parameter value="5">
			<lowerlimit value="-0.1" />
			<upperlimit value="16" />
		</parameter>
		<parameter value="6">
			<lowerlimit value="0" />
			<upperlimit value="256" />
		</parameter>
		<parameter value="7">
			<lowerlimit value="-0.07" />
			<upperlimit value="1.1" />
		</parameter>
		<parameter value="8">
			<lowerlimit value="-0.0001" />
			<upperlimit value="0.005" />
		</parameter>
		<parameter value="9">
			<lowerlimit value="0" />
			<upperlimit value="40" />
		</parameter>
		<parameter value="10">
			<lowerlimit value="-0.1" />
			<upperlimit value="0.2" />
		</parameter>
	</histograms>

This is a list of the __lowerlimit__ and __upperlimit__ of the values for each
image parameter. These are used for min/max normalization of the parameter
values when doing visual comparison of objects.

###eventtypes###

	<eventtypes>
		<activeregion>
			<timespan value="12600" />
		</activeregion>
		<coronalhole>
			<timespan value="12600" />
		</coronalhole>
		<emergingflux>
			<timespan value="9600" />
		</emergingflux>
		<filiment>
			<timespan value="35000" />
		</filiment>
		<sigmoid>
			<timespan value="5300" />
		</sigmoid>
		<sunspot>
			<timespan value="21000" />
		</sunspot>
		<flare>
			<timespan value="480" />
		</flare>
	</eventtypes>

This set tells the tracking algorithm how long each event type is expected to
last (time between object reports). This is how it knows where to search for the
next detection in the dataset.  It is in seconds.  

