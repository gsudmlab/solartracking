/**
 * SolarTracking Copyright (C) 2017 Georgia State University
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.gsu.dmlab.databases;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import javax.imageio.ImageIO;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.sql.DataSource;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.google.common.base.Charsets;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.hash.HashCode;
import com.google.common.hash.HashFunction;

import edu.gsu.dmlab.databases.interfaces.IImageDBCreator;
import edu.gsu.dmlab.datatypes.ImageDBDateIdPair;
import edu.gsu.dmlab.datatypes.ImageDBFitsHeaderData;
import edu.gsu.dmlab.datatypes.ImageDBWaveParamPair;
import edu.gsu.dmlab.datatypes.Waveband;
import edu.gsu.dmlab.datatypes.interfaces.IEvent;
import edu.gsu.dmlab.exceptions.ExternalDatasourceException;
import edu.gsu.dmlab.imageproc.interfaces.IImgParamNormalizer;
import edu.gsu.dmlab.util.Utility;
import smile.math.matrix.DenseMatrix;
import smile.math.matrix.JMatrix;

/**
 * Wrapper class for the image database access methods. This class is set to
 * pull image parameters from the DMLab API at Georgia State University. It will
 * also pull full scale images from Helioviewer if they are not already in the
 * database upon the access of the method requesting it. This class assumes that
 * all of the tables have been created already. So, make sure you use the
 * creator to create each month of tables you are going to use prior to using
 * this.
 * 
 * @author Dustin Kempton, Data Mining Lab, Georgia State University
 * @version {@value edu.gsu.dmlab.ConstValues#major_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#minor_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#build_ver}
 */
public class DMLabPullingImageDBConnection extends BaseDBConnection {

	protected LoadingCache<CacheKey, double[][][]> cache = null;

	private IImageDBCreator creator;
	private HashMap<Integer, Integer> helioviewerWave_IdMap;
	private DateTimeFormatter dt = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");
	private DateTimeFormatter dt2 = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss");
	private DateTimeFormatter keyFormat = DateTimeFormat.forPattern("yyyy-MM-dd'T'H");
	private String url = "https://api.helioviewer.org/v2/getJP2Image/";
	private String dmLabUrl = "http://dmlab.cs.gsu.edu/dmlabapi/params/SDO/AIA/64/full/";
	private String closestIdURL = "https://api.helioviewer.org/v2/getClosestImage/";
	private String headerURL = "https://api.helioviewer.org/v2/getJP2Header/";
	private String charset = java.nio.charset.StandardCharsets.UTF_8.name();
	private DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();

	private String USER_AGENT = "GSU DMLab Java Client 0.0.1";
	private SSLContext ctx;

	private int correctParamRowCount = 4096;

	private class CacheKey {
		String key;
		IEvent event;
		Waveband wavelength;
		boolean leftSide;
		HashCode hc = null;

		public CacheKey(IEvent event, boolean leftSide, Waveband wavelength, HashFunction hashFunct) {
			StringBuilder evntName = new StringBuilder();
			evntName.append(event.getUUID().toString());
			evntName.append(wavelength);
			if (leftSide) {
				evntName.append("T");
			} else {
				evntName.append("F");
			}
			this.key = evntName.toString();
			this.hc = hashFunct.newHasher().putString(this.key, Charsets.UTF_8).hash();
			this.event = event;
			this.leftSide = leftSide;
			this.wavelength = wavelength;
		}

		@Override
		public void finalize() {
			this.hc = null;
			this.key = null;
			this.event = null;
		}

		public IEvent getEvent() {
			return this.event;
		}

		public boolean isLeftSide() {
			return this.leftSide;
		}

		public Waveband getWavelength() {
			return this.wavelength;
		}

		public String getKey() {
			return key;
		}

		@Override
		public int hashCode() {
			return hc.asInt();
		}

		@Override
		public boolean equals(Object obj) {
			if (obj instanceof CacheKey) {
				CacheKey val = (CacheKey) obj;
				return val.getKey().compareTo(this.key) == 0;
			} else {
				return false;
			}
		}

		@Override
		public String toString() {
			return this.key;
		}
	}

	/**
	 * Constructor for the image database connection.
	 * 
	 * @param dsourc
	 *            The data source connection to the database.
	 * @param creator
	 *            Database creator object used for creating tables and inserting
	 *            data into the tables when they are missing.
	 * @param normalizer
	 *            An optional normalizer for the parameter values.
	 * @param maxCacheSize
	 *            The number of cached image parameter sets we should keep for
	 *            increased speed. This is one for each wavelength used on each
	 *            event detection object.
	 */
	public DMLabPullingImageDBConnection(DataSource dsourc, IImageDBCreator creator, IImgParamNormalizer normalizer,
			int maxCacheSize) {
		super(dsourc, normalizer);
		if (creator == null)
			throw new IllegalArgumentException("IImageDBCreator cannot be null in IImageDBConnection constructor.");
		this.creator = creator;

		this.helioviewerWave_IdMap = new HashMap<Integer, Integer>();
		this.helioviewerWave_IdMap.put(Integer.valueOf(94), Integer.valueOf(8));
		this.helioviewerWave_IdMap.put(Integer.valueOf(131), Integer.valueOf(9));
		this.helioviewerWave_IdMap.put(Integer.valueOf(171), Integer.valueOf(10));
		this.helioviewerWave_IdMap.put(Integer.valueOf(193), Integer.valueOf(11));
		this.helioviewerWave_IdMap.put(Integer.valueOf(211), Integer.valueOf(12));
		this.helioviewerWave_IdMap.put(Integer.valueOf(304), Integer.valueOf(13));
		this.helioviewerWave_IdMap.put(Integer.valueOf(335), Integer.valueOf(14));
		this.helioviewerWave_IdMap.put(Integer.valueOf(1600), Integer.valueOf(15));
		this.helioviewerWave_IdMap.put(Integer.valueOf(1700), Integer.valueOf(16));
		this.helioviewerWave_IdMap.put(Integer.valueOf(4500), Integer.valueOf(17));

		CookieHandler.setDefault(new CookieManager(null, CookiePolicy.ACCEPT_ALL));
		try {
			TrustManager[] trustManagers = { new DefaultTrustManager() };
			this.ctx = SSLContext.getInstance("TLS");
			this.ctx.init(null, trustManagers, null);
			// this.dBuilder = dbFactory.newDocumentBuilder();
		} catch (NoSuchAlgorithmException | KeyManagementException e) {
			e.printStackTrace();
		}

		this.cache = CacheBuilder.newBuilder().maximumSize(maxCacheSize)
				.build(new CacheLoader<CacheKey, double[][][]>() {
					public double[][][] load(CacheKey key) {
						return fetchImageParams(key);
					}
				});
	}

	private static class DefaultTrustManager implements X509TrustManager {

		@Override
		public void checkClientTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
			// TODO Auto-generated method stub
		}

		@Override
		public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
			// TODO Auto-generated method stub
		}

		@Override
		public X509Certificate[] getAcceptedIssuers() {
			// TODO Auto-generated method stub
			return null;
		}
	}

	@Override
	public BufferedImage getFirstFullImage(Interval period, Waveband wavelength) throws SQLException, IOException {
		// public BufferedImage getFirstFullImage(Interval period, int
		// wavelength) throws SQLException, IOException {

		BufferedImage bImg = null;
		bImg = super.getFirstFullImage(period, wavelength);

		// If the DB does not contain the image then fetch from helioviewer
		if (bImg == null) {
			ImageDBDateIdPair[] pairs = super.getImageIdsForInterval(period, wavelength);
			if (pairs.length > 0) {
				int i = 0;
				while (bImg == null && i < pairs.length) {
					this.fillDBFromHelioviewer(pairs[i].period, wavelength, pairs[i].id);
					bImg = this.getFullImgForId(pairs[i].period, pairs[i].id);
					i++;
				}
			}
		}
		return bImg;
	}

	@Override
	public DenseMatrix[] getImageParamForId(Interval period, int id) {

		DenseMatrix[] result = null;
		result = super.getImageParamForId(period, id);

		// If the DB does not contain the parameter then we need to fetch it
		// from the DMLab API
		if (result == null) {
			this.fillDBWithParam(period, id);
			result = super.getImageParamForId(period, id);
		}

		return result;
	}

	private double[][][] fetchImageParams(CacheKey key) {
		IEvent event = key.getEvent();
		Waveband wavelength = key.getWavelength();
		boolean leftSide = key.isLeftSide();

		double[][][] result = super.getImageParamForWave(event, wavelength, leftSide);
		if (result == null) {
			if (leftSide) {
				// Add N=1 images from the end of the event time range. You can
				// change i to greater if desired.
				Interval evPeriod = event.getTimePeriod();
				DateTime end = evPeriod.getEnd();
				for (int i = 1; i > 0; i--) {
					Interval period = new Interval(end.minusMinutes(i * 6), end.minusMinutes((i - 1) * 6));
					this.fillDBWithWavelengthParam(period, wavelength);
				}

			} else {
				// Add N=1 images from the start of the event time range. You
				// can change i limit to greater if desired.
				Interval evPeriod = event.getTimePeriod();
				DateTime start = evPeriod.getStart();
				for (int i = 1; i < 2; i++) {
					Interval period = new Interval(start.plusMinutes((i - 1) * 6), start.plusMinutes(i * 6));
					this.fillDBWithWavelengthParam(period, wavelength);
				}
			}
		}

		result = super.getImageParamForWave(event, wavelength, leftSide);
		if (result == null)
			throw new ExternalDatasourceException("Not able to pull from db :" + key.getKey());
		return result;

	}

	private Lock loc = new ReentrantLock();
	private Condition ready = loc.newCondition();
	private HashMap<String, String> currentlyProcessing = new HashMap<String, String>();

	@Override
	public double[][][] getImageParamForWave(IEvent event, Waveband wavelength, boolean leftSide) {
		CacheKey key = new CacheKey(event, leftSide, wavelength, this.hashFunct);

		String processingWaveKey = event.getTimePeriod().getStart().toString(this.keyFormat) + wavelength;
		loc.lock();
		try {
			while (this.currentlyProcessing.containsKey(processingWaveKey))
				try {
					this.ready.await();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			this.currentlyProcessing.put(processingWaveKey, processingWaveKey);
		} finally {
			loc.unlock();
		}

		int count = 0;
		double[][][] returnValue = null;
		while (returnValue == null && count < 3) {

			if (count > 0)
				System.out.println("Failed to load from cache. Trying again for: " + processingWaveKey);

			try {
				returnValue = this.cache.get(key);
			} catch (Exception e) {
			}
			count++;
		}

		loc.lock();
		try {
			this.currentlyProcessing.remove(processingWaveKey);
			this.ready.signalAll();
		} finally {
			loc.unlock();
		}
		key = null;
		return returnValue;
	}

	@Override
	public DenseMatrix[] getImageParamForEv(IEvent event, ImageDBWaveParamPair[] params, boolean leftSide) {
		// Get the image parameters for each wavelength in the set of dimensions
		DenseMatrix[] dimMatArr = new DenseMatrix[params.length];
		for (int i = 0; i < params.length; i++) {

			double[][][] paramVals = this.getImageParamForWave(event, params[i].wavelength, leftSide);

			// If we don't have data then we need to indicate that with a null;
			if (paramVals == null)
				return null;

			int rows = paramVals.length;
			int cols = paramVals[0].length;

			// get the param indicated by the dims array at depth i
			// the param value is from 1 to 10 whare array index is 0-9
			// hence the -1
			int paramIdx = params[i].parameter - 1;
			double[][] data = new double[rows][cols];
			for (int x = 0; x < cols; x++) {
				for (int y = 0; y < rows; y++) {
					data[y][x] = paramVals[y][x][paramIdx];
				}
			}
			dimMatArr[i] = new JMatrix(data);
		}
		return dimMatArr;
	}

	@Override
	public ImageDBFitsHeaderData getHeaderForId(Interval period, int id) throws SQLException {
		ImageDBFitsHeaderData result = super.getHeaderForId(period, id);

		if (result == null) {
			Connection con = null;
			Document doc = null;
			// try and pull the info from the database
			try {
				String queryFileString = this.buildFileIdQueryString(period);
				con = this.dsourc.getConnection();
				con.setAutoCommit(true);
				PreparedStatement file_prep_stmt = con.prepareStatement(queryFileString);
				file_prep_stmt.setInt(1, id);
				ResultSet imgFileIdResults = file_prep_stmt.executeQuery();
				if (imgFileIdResults.next()) {
					Timestamp ts = imgFileIdResults.getTimestamp(1);
					int wavelength = imgFileIdResults.getInt(2);
					Interval period2 = new Interval(ts.getTime(), ts.getTime() + (1000 * 6 * 60));
					doc = this.getHeaderForId(period2.getStart(), wavelength);
				}
				imgFileIdResults.close();
				file_prep_stmt.close();
			} finally {
				if (con != null) {
					con.close();
				}
			}

			if (doc != null) {
				ImageDBFitsHeaderData header = this.readFitsHeader(doc);
				this.creator.insertHeader(header, id, period);
				result = super.getHeaderForId(period, id);
			}

		}
		return result;
	}

	private ImageDBFitsHeaderData readFitsHeader(Document headerDocument) {

		try {
			ImageDBFitsHeaderData headerData = new ImageDBFitsHeaderData();

			String x0String = headerDocument.getElementsByTagName("X0_MP").item(0).getTextContent();
			String y0String = headerDocument.getElementsByTagName("Y0_MP").item(0).getTextContent();
			String rSunString = headerDocument.getElementsByTagName("R_SUN").item(0).getTextContent();
			String dSunString = headerDocument.getElementsByTagName("DSUN_OBS").item(0).getTextContent();
			String cDeltString = headerDocument.getElementsByTagName("CDELT1").item(0).getTextContent();
			headerData.X0 = Double.parseDouble(x0String);
			headerData.Y0 = Double.parseDouble(y0String);
			headerData.R_SUN = Double.parseDouble(rSunString);
			headerData.DSUN = Double.parseDouble(dSunString);
			headerData.CDELT = Double.parseDouble(cDeltString);
			return headerData;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private void fillDBWithParam(Interval period, int id) {
		Connection con = null;
		String queryFileString = this.buildFileIdQueryString(period);
		// variables for filling the database with a fetched image.

		int wavelength = 0;
		ImageDBDateIdPair pair = new ImageDBDateIdPair();

		// try and pull the info from the database
		try {
			con = this.dsourc.getConnection();
			con.setAutoCommit(true);
			PreparedStatement file_prep_stmt = con.prepareStatement(queryFileString);
			file_prep_stmt.setInt(1, id);
			ResultSet imgFileIdResults = file_prep_stmt.executeQuery();
			if (imgFileIdResults.next()) {
				Timestamp ts = imgFileIdResults.getTimestamp(1);
				wavelength = imgFileIdResults.getInt(2);
				pair.id = id;
				pair.period = new Interval(ts.getTime(), ts.getTime() + (1000 * 6 * 60));
			}
			imgFileIdResults.close();
			file_prep_stmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

		this.insertParamUsingIdWaveAndPeriod(Utility.getWavebandFromInt(wavelength), pair.id, pair.period);
	}

	private void fillDBWithWavelengthParam(Interval period, Waveband wavelength) {

		ImageDBDateIdPair[] pairs = super.getImageIdsForInterval(period, wavelength);
		if (pairs.length == 0) {
			try {
				int id = this.creator.insertFileDescript(wavelength, period);
				this.insertParamUsingIdWaveAndPeriod(wavelength, id, period);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		} else {
			this.insertParamUsingIdWaveAndPeriod(wavelength, pairs[0].id, pairs[0].period);
		}

	}

	private void insertParamUsingIdWaveAndPeriod(Waveband wavelength, int id, Interval period) {
		String urlString = "";
		try {
			String query = String.format(
					"?wave=%s&starttime=%s", URLEncoder
							.encode(Integer.valueOf(Utility.convertWavebandToInt(wavelength)).toString(), this.charset),
					dt2.print(period.getStart()));
			urlString = this.dmLabUrl + query;
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		// Give it a couple of retries if it fails

		int count = 0;
		boolean done = false;
		while (!done && count < 3) {
			try {
				DocumentBuilder dBuilder = this.dbFactory.newDocumentBuilder();

				URL httpConnect = new URL(urlString);
				try (InputStream stream = httpConnect.openStream()) {
					Document doc = dBuilder.parse(stream);
					if (!checkParamsExist(id, period)) {
						this.creator.insertParams(doc, id, period);
					}
				}
				done = true;

			} catch (MalformedURLException | SQLException | ParserConfigurationException e) {
				e.printStackTrace();
			} catch (IOException | NullPointerException | SAXException e) {
				try {
					if (count > 1) {
						if (!checkParamsExist(id, period)) {
							System.out.println("Could not download from GSU DMLab: " + e.getMessage());
							this.creator.insertEmptyParams(id, period);
						}
						done = true;
					}
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
			count++;
		}

	}

	public boolean checkParamsExist(int id, Interval period) throws SQLException {

		Connection con = null;
		boolean exists = false;
		try {
			con = this.dsourc.getConnection();
			con.setAutoCommit(true);
			String query = this.checkImageParamQuery(period);
			PreparedStatement stmt = con.prepareStatement(query);
			stmt.setInt(1, id);
			ResultSet results = stmt.executeQuery();
			if (results.next()) {

				int numRows = results.getInt(1);
				stmt.close();
				if (numRows == this.correctParamRowCount) {
					exists = true;
				} else {
					query = this.deleteImageParamQuery(period);
					stmt = con.prepareStatement(query);
					stmt.setInt(1, id);
					stmt.executeUpdate();
					stmt.close();
				}
			} else {
				query = this.deleteImageParamQuery(period);
				stmt.close();
				stmt = con.prepareStatement(query);
				stmt.setInt(1, id);
				stmt.executeUpdate();
				stmt.close();
			}

		} finally {
			if (con != null) {
				con.close();
			}
		}
		return exists;

	}

	private String deleteImageParamQuery(Interval period) {
		Calendar calStart = Calendar.getInstance();
		calStart.setTimeInMillis(period.getStartMillis());

		StringBuilder sb = new StringBuilder();
		sb.append("DELETE FROM image_params_");
		sb.append(calStart.get(Calendar.YEAR));
		if ((calStart.get(Calendar.MONTH) + 1) < 10) {
			sb.append("0");
			sb.append(calStart.get(Calendar.MONTH) + 1);
		} else {
			sb.append(calStart.get(Calendar.MONTH) + 1);
		}

		sb.append(" WHERE file_id = ?;");

		return sb.toString();
	}

	private String checkImageParamQuery(Interval period) {
		Calendar calStart = Calendar.getInstance();
		calStart.setTimeInMillis(period.getStartMillis());

		StringBuilder sb = new StringBuilder();
		sb.append("SELECT count(*) FROM image_params_");
		sb.append(calStart.get(Calendar.YEAR));
		if ((calStart.get(Calendar.MONTH) + 1) < 10) {
			sb.append("0");
			sb.append(calStart.get(Calendar.MONTH) + 1);
		} else {
			sb.append(calStart.get(Calendar.MONTH) + 1);
		}

		sb.append(" WHERE file_id = ?;");

		return sb.toString();
	}

	@Override
	public BufferedImage getFullImgForId(Interval period, int id) throws SQLException, IOException {
		Connection con = null;
		BufferedImage bImg = null;
		bImg = super.getFullImgForId(period, id);

		// If the DB does not contain the image then fetch from helioviewer
		if (bImg == null) {
			String queryFileString = this.buildFileIdQueryString(period);

			// variables for filling the database with a fetched image.
			Waveband wavelength = null;
			ImageDBDateIdPair pair = new ImageDBDateIdPair();
			boolean gotData = false;

			// try and pull the info from the database
			try {
				con = this.dsourc.getConnection();
				con.setAutoCommit(true);
				PreparedStatement file_prep_stmt = con.prepareStatement(queryFileString);
				file_prep_stmt.setInt(1, id);
				ResultSet imgFileIdResults = file_prep_stmt.executeQuery();
				if (imgFileIdResults.next()) {
					Timestamp ts = imgFileIdResults.getTimestamp(1);
					wavelength = Utility.getWavebandFromInt(imgFileIdResults.getInt(2));
					pair.id = id;
					pair.period = new Interval(ts.getTime(), ts.getTime() + (1000 * 6 * 60));
					gotData = true;
				}
				imgFileIdResults.close();
				file_prep_stmt.close();
			} finally {
				if (con != null) {
					con.close();
				}
			}

			// fill the database and return the filled instance.
			if (gotData) {
				this.fillDBFromHelioviewer(pair.period, wavelength, pair.id);
				bImg = super.getFullImgForId(pair.period, pair.id);
			}
		}
		return bImg;
	}

	private String buildFileIdQueryString(Interval period) {
		// for constructing the table names from the year and month of
		// the event
		Calendar calStart = Calendar.getInstance();
		calStart.setTimeInMillis(period.getStartMillis());
		String calStartYear = "" + calStart.get(Calendar.YEAR);
		String calStartMonth;
		if ((calStart.get(Calendar.MONTH) + 1) < 10) {
			calStartMonth = "0" + (calStart.get(Calendar.MONTH) + 1);
		} else {
			calStartMonth = "" + (calStart.get(Calendar.MONTH) + 1);
		}

		StringBuilder queryString = new StringBuilder();
		queryString.append("SELECT startdate, wavelength FROM files_");
		queryString.append(calStartYear);
		queryString.append(calStartMonth);
		queryString.append(" WHERE id = ? ;");

		return queryString.toString();
	}

	private void fillDBFromHelioviewer(Interval period, Waveband wavelength, int id) throws SQLException {

		try {
			String query = String.format("date=%s&sourceId=%s", dt.print(period.getStart()),
					URLEncoder.encode(helioviewerWave_IdMap.get(wavelength).toString(), this.charset));

			// CookieHandler.setDefault(new CookieManager(null,
			// CookiePolicy.ACCEPT_ALL));
			HttpsURLConnection connection = (HttpsURLConnection) new URL(this.url + "?" + query).openConnection();
			connection.setSSLSocketFactory(this.ctx.getSocketFactory());

			connection.setRequestProperty("Accept-Charset", this.charset);
			connection.setRequestMethod("GET");
			connection.setRequestProperty("User-Agent", USER_AGENT);
			connection.setRequestProperty("Accept",
					"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
			connection.setRequestProperty("Accept-Language", "en-US,en;q=0.8");
			connection.setUseCaches(false);
			connection.setDoOutput(true);

			connection.connect();

			if (connection.getResponseCode() == 200) {

				InputStream response = connection.getInputStream();
				byte[] responseBytes = new byte[connection.getContentLength()];
				int bytesRead = response.read(responseBytes);
				int idx = bytesRead;
				while (bytesRead != -1 && idx < responseBytes.length - 1) {
					bytesRead = response.read(responseBytes, idx, responseBytes.length - idx);
					if (bytesRead != -1)
						idx += bytesRead;
				}

				response.close();
				connection.disconnect();

				// Jpeg200 files consist of chunks with 8 byte headers.
				// So, if it is not at least 8 bytes, it is invalid.
				if (idx > 7) {
					byte[] bytesReadFromResp = Arrays.copyOfRange(responseBytes, 0, idx + 1);
					responseBytes = null;
					if (checkJP2(bytesReadFromResp)) {
						InputStream imgStr = new ByteArrayInputStream(bytesReadFromResp);
						BufferedImage img = ImageIO.read(imgStr);

						if (img != null) {
							this.creator.insertFullImage(img, id, period);
							// System.out.println("Inserted");
						}
					} else {
						System.out.println("Failed Check.");
					}
				}
			} else {
				InputStream response = connection.getErrorStream();

				byte[] responseBytes = new byte[connection.getContentLength()];
				int bytesRead = response.read(responseBytes);
				int idx = bytesRead;
				while (bytesRead != -1 && idx < responseBytes.length - 1) {
					bytesRead = response.read(responseBytes, idx, responseBytes.length - idx);
					if (bytesRead != -1)
						idx += bytesRead;
				}
				String respStr = new String(responseBytes, StandardCharsets.UTF_8);
				System.out.println(respStr);
				response.close();
				connection.disconnect();

			}
		} catch (IOException | RuntimeException e) {
			e.printStackTrace();
		}
	}

	static final int JP2_SIGNATURE_BOX = 0x6a502020;
	static final int FILE_TYPE_BOX = 0x66747970;
	static final int FT_BR = 0x6a703220;

	private boolean checkJP2(byte[] byteBuffer) {
		// We make sure the first 12 bytes are the JP2 signature box
		int idx = 0;

		if ((idx + 4) < byteBuffer.length) {
			if (this.readInt(byteBuffer, idx) != 0x0000000c)
				return false;
			idx += 4;
		} else {
			return false;
		}

		if ((idx + 4) < byteBuffer.length) {
			if (this.readInt(byteBuffer, idx) != JP2_SIGNATURE_BOX)
				return false;
			idx += 4;
		} else {
			return false;
		}

		if ((idx + 4) < byteBuffer.length) {
			if (this.readInt(byteBuffer, idx) != 0x0d0a870a)
				return false;
			idx += 4;
		} else {
			return false;
		}

		return this.readFileTypeBox(byteBuffer, idx);
	}

	/**
	 * Reads a signed int (i.e., 32 bit) from the input. Prior to reading, the
	 * input should be realigned at the byte level.
	 * 
	 * @return The next byte-aligned signed int (32 bit) from the input.
	 */
	public final int readInt(byte[] byteBuffer, int pos) {
		return (((byteBuffer[pos++] & 0xFF) << 24) | ((byteBuffer[pos++] & 0xFF) << 16)
				| ((byteBuffer[pos++] & 0xFF) << 8) | (byteBuffer[pos++] & 0xFF));
	}

	final long readLong(byte[] byteBuffer, int pos) {
		return (((long) (byteBuffer[pos++] & 0xFF) << 56) | ((long) (byteBuffer[pos++] & 0xFF) << 48)
				| ((long) (byteBuffer[pos++] & 0xFF) << 40) | ((long) (byteBuffer[pos++] & 0xFF) << 32)
				| ((long) (byteBuffer[pos++] & 0xFF) << 24) | ((long) (byteBuffer[pos++] & 0xFF) << 16)
				| ((long) (byteBuffer[pos++] & 0xFF) << 8) | ((long) (byteBuffer[pos++] & 0xFF)));
	}

	/**
	 * This method reads the File Type box.
	 *
	 * @return false if the File Type box was not found or invalid else true
	 *
	 * @exception java.io.IOException
	 *                If an I/O error occurred.
	 * @exception java.io.EOFException
	 *                If the end of file was reached
	 */
	public boolean readFileTypeBox(byte[] byteBuffer, int pos) {
		int length;

		int nComp;
		boolean foundComp = false;

		// Read box length (LBox)
		length = this.readInt(byteBuffer, pos);
		if (length == 0) { // This can not be last box
			System.out.println("Zero-length of Profile Box");
			return false;
		}
		pos += 4;

		// Check that this is a File Type box (TBox)
		if (this.readInt(byteBuffer, pos) != FILE_TYPE_BOX) {
			System.out.println("Bad File Type Box");
			return false;
		}
		pos += 4;

		// Check for XLBox
		if (length == 1) { // Box has 8 byte length;
			System.out.println("File Too Big");
			return false;
		}

		// Read Brand field
		// in.readInt();
		pos += 4;

		// Read MinV field
		// in.readInt();
		pos += 4;

		// Check that there is at least one FT_BR entry in in
		// compatibility list
		nComp = (length - 16) / 4; // Number of compatibilities.
		for (int i = nComp; i > 0; i--) {
			if (this.readInt(byteBuffer, pos) == FT_BR)
				foundComp = true;
			pos += 4;
		}
		if (!foundComp) {
			System.out.println("No FT_BR entry.");

			return false;
		}

		return true;
	}

	private Document getHeaderForId(DateTime date, int wavelength) throws ExternalDatasourceException {
		try {
			int id = this.getClosestImageDescriptr(date, wavelength);
			if (id < 0)
				return null;
			String query = String.format("id=%s", id);

			HttpsURLConnection connection = (HttpsURLConnection) new URL(this.headerURL + "?" + query).openConnection();
			connection.setSSLSocketFactory(this.ctx.getSocketFactory());

			connection.setRequestProperty("Accept-Charset", this.charset);
			connection.setRequestMethod("GET");
			connection.setRequestProperty("User-Agent", USER_AGENT);
			connection.setRequestProperty("Accept",
					"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
			connection.setRequestProperty("Accept-Language", "en-US,en;q=0.8");
			connection.setUseCaches(false);
			connection.setDoOutput(true);

			connection.connect();

			if (connection.getResponseCode() == 200) {

				InputStream response = connection.getInputStream();
				DocumentBuilder dBuilder = this.dbFactory.newDocumentBuilder();
				Document doc = dBuilder.parse(response);
				response.close();
				connection.disconnect();
				return doc;
			}

		} catch (Exception e) {
			throw new ExternalDatasourceException(e.getMessage(), e.getCause());
		}
		return null;
	}

	private int getClosestImageDescriptr(DateTime date, int wavelength) throws ExternalDatasourceException {
		try {
			String query = String.format("date=%s&sourceId=%s", this.dt.print(date),
					URLEncoder.encode(helioviewerWave_IdMap.get(wavelength).toString(), this.charset));

			HttpsURLConnection connection = (HttpsURLConnection) new URL(this.closestIdURL + "?" + query)
					.openConnection();
			connection.setSSLSocketFactory(this.ctx.getSocketFactory());

			connection.setRequestProperty("Accept-Charset", this.charset);
			connection.setRequestMethod("GET");
			connection.setRequestProperty("User-Agent", USER_AGENT);
			connection.setRequestProperty("Accept",
					"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
			connection.setRequestProperty("Accept-Language", "en-US,en;q=0.8");
			connection.setUseCaches(false);
			connection.setDoOutput(true);

			connection.connect();

			if (connection.getResponseCode() == 200) {

				InputStream response = connection.getInputStream();
				byte[] responseBytes = new byte[connection.getContentLength()];
				int bytesRead = response.read(responseBytes);
				int idx = bytesRead;
				while (bytesRead != -1 && idx < responseBytes.length - 1) {
					bytesRead = response.read(responseBytes, idx, responseBytes.length - idx);
					if (bytesRead != -1)
						idx += bytesRead;
				}

				response.close();
				connection.disconnect();

				String resp = new String(responseBytes, this.charset);
				resp = resp.substring(1, resp.length() - 1);
				String[] respArr = resp.split(",");
				String idString = respArr[0].substring(respArr[0].indexOf(':') + 1).replace('"', ' ').trim();

				int id = Integer.parseInt(idString);
				return id;
			}

		} catch (Exception e) {
			throw new ExternalDatasourceException(e.getMessage(), e.getCause());
		}
		return Integer.MAX_VALUE;
	}
}
