/**
 * SolarTracking Copyright (C) 2017 Georgia State University
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.gsu.dmlab.databases;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.sql.DataSource;

import edu.gsu.dmlab.databases.interfaces.IFeatureDBConnection;
import edu.gsu.dmlab.datatypes.EventType;
import edu.gsu.dmlab.datatypes.ImageDBWaveParamPair;
import edu.gsu.dmlab.util.Utility;

/**
 * Class used to pull the feature rankings from the database. Also to check if
 * they are in the database already.
 * 
 * @author Dustin Kempton, Data Mining Lab, Georgia State University
 * @version {@value edu.gsu.dmlab.ConstValues#major_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#minor_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#build_ver}
 */
public class SingleFeatureDBConnection implements IFeatureDBConnection {

	private DataSource dsourc;
	private int featureCount;

	/**
	 * Constructor
	 * 
	 * @param dsourc
	 *            The database connection object that we use to query the
	 *            database.
	 * @param featureCount
	 *            The number of features that should be in the database if the
	 *            selection process has already completed.
	 */
	public SingleFeatureDBConnection(DataSource dsourc, int featureCount) {
		if (dsourc == null)
			throw new IllegalArgumentException("DataSource cannot be null in SingleFeatureDBConnection constructor.");
		this.dsourc = dsourc;
		this.featureCount = featureCount;
	}

	@Override
	public ImageDBWaveParamPair[] getBestFeatures(EventType type, int num) throws SQLException {
		Connection con = null;
		ArrayList<ImageDBWaveParamPair> resultList = new ArrayList<ImageDBWaveParamPair>();
		try {
			String queryString = this.buildQueryFeatureComboTable(type);
			con = this.dsourc.getConnection();
			con.setAutoCommit(true);

			PreparedStatement stmt = con.prepareStatement(queryString);
			stmt.setInt(1, num);
			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {
				int param = rs.getInt(1);
				int wave = rs.getInt(2);
				ImageDBWaveParamPair val = new ImageDBWaveParamPair();
				val.parameter = param;
				val.wavelength = Utility.getWavebandFromInt(wave);
				resultList.add(val);
			}

		} finally {
			if (con != null) {
				con.close();
			}
		}

		ImageDBWaveParamPair[] resultArr = new ImageDBWaveParamPair[resultList.size()];
		resultList.toArray(resultArr);
		return resultArr;
	}

	private String buildQueryFeatureComboTable(EventType type) {
		String typeString = this.getTypeString(type);

		StringBuilder sb = new StringBuilder();
		sb.append("SELECT param, wavelength FROM `");
		sb.append(typeString);
		sb.append("_features` ");
		sb.append("ORDER BY stat_val DESC LIMIT ?;");
		return sb.toString();
	}

	@SuppressWarnings("incomplete-switch")
	private String getTypeString(EventType type) {
		String typeString = "";

		switch (type) {
		case ACTIVE_REGION:
			typeString = "ar";
			break;
		case CORONAL_HOLE:
			typeString = "ch";
			break;
		case FILAMENT:
			typeString = "fi";
			break;
		case SIGMOID:
			typeString = "sg";
			break;
		case SUNSPOT:
			typeString = "ss";
			break;
		case EMERGING_FLUX:
			typeString = "ef";
			break;
		case FLARE:
			typeString = "fl";
			break;
		}
		return typeString;
	}

	@Override
	public boolean featureTableSet(EventType type) throws SQLException {
		String existQuery = "SHOW TABLES LIKE '" + this.getTypeString(type) + "_features';";
		String countQuery = "SELECT count(*) FROM `" + this.getTypeString(type) + "_features`;";
		Connection con = null;
		boolean result = false;
		try {
			con = this.dsourc.getConnection();
			con.setAutoCommit(true);

			PreparedStatement stmt = con.prepareStatement(existQuery);

			ResultSet rs = stmt.executeQuery();
			if (rs.next()) {
				stmt.close();
				stmt = con.prepareStatement(countQuery);
				rs = stmt.executeQuery();
				if (rs.next()) {
					int count = rs.getInt(1);
					if (count == this.featureCount) {
						result = true;
					}
				}
			}

		} finally {
			if (con != null) {
				con.close();
			}
		}
		return result;
	}
}
