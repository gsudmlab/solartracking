/**
 * SolarTracking Copyright (C) 2017 Georgia State University
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.gsu.dmlab.databases;

import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.sql.DataSource;

import org.joda.time.DateTime;
import org.joda.time.Interval;

import com.google.common.hash.HashFunction;
import com.google.common.hash.Hashing;

import edu.gsu.dmlab.databases.interfaces.IImageDBConnection;
import edu.gsu.dmlab.datatypes.ImageDBDateIdPair;
import edu.gsu.dmlab.datatypes.ImageDBFitsHeaderData;
import edu.gsu.dmlab.datatypes.ImageDBWaveParamPair;
import edu.gsu.dmlab.datatypes.Waveband;
import edu.gsu.dmlab.datatypes.interfaces.IEvent;
import edu.gsu.dmlab.imageproc.interfaces.IImgParamNormalizer;
import edu.gsu.dmlab.util.Utility;
import smile.math.matrix.DenseMatrix;
import smile.math.matrix.JMatrix;

/**
 * Class used to query the database for several different data objects dealing
 * with images. This one does not fill the database if the data is not there.
 * 
 * @author Dustin Kempton, Data Mining Lab, Georgia State University
 * @version {@value edu.gsu.dmlab.ConstValues#major_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#minor_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#build_ver}
 */
public abstract class BaseDBConnection implements IImageDBConnection {

	protected IImgParamNormalizer normalizer;
	protected DataSource dsourc = null;
	protected int paramDownSample = 64;
	protected int paramDim = 10;
	protected HashFunction hashFunct = Hashing.murmur3_128();

	/**
	 * Constructor for the base image database connection.
	 * 
	 * @param dsourc
	 *            The data source connection to the database.
	 * @param normalizer
	 *            An optional normalizer for the parameter values.
	 */
	public BaseDBConnection(DataSource dsourc, IImgParamNormalizer normalizer) {
		if (dsourc == null)
			throw new IllegalArgumentException("DataSource cannot be null in ImageDBConnection constructor.");

		this.dsourc = dsourc;
		this.normalizer = normalizer;

	}

	@Override
	public double[][][] getImageParamForWave(IEvent event, Waveband wavelength, boolean leftSide) {
		double[][][] retVal = null;

		String queryFileString = this.buildFileQueryString(event, leftSide);
		String queryParamString = this.buildQueryParamString(event.getTimePeriod());

		Timestamp startTime = new Timestamp(event.getTimePeriod().getStartMillis());
		Timestamp endTime = new Timestamp(event.getTimePeriod().getEndMillis());

		Rectangle tmpBbox = event.getBBox();
		int wSize;
		int xStart;
		if ((tmpBbox.width / this.paramDownSample) < 5) {
			xStart = (tmpBbox.x / this.paramDownSample) - 2;
			wSize = 5;
		} else {
			xStart = (tmpBbox.x / this.paramDownSample);
			wSize = (tmpBbox.width / this.paramDownSample);
		}

		int hSize;
		int yStart;
		if ((tmpBbox.height / this.paramDownSample) < 5) {
			yStart = (tmpBbox.y / this.paramDownSample) - 2;
			hSize = 5;
		} else {
			yStart = (tmpBbox.y / this.paramDownSample);
			hSize = (tmpBbox.height / this.paramDownSample);
		}
		Rectangle rec = new Rectangle(xStart, yStart, wSize, hSize);

		retVal = new double[rec.height][rec.width][this.paramDim];

		int tryCount = 0;
		boolean executed = false;
		while (!executed && tryCount < 3) {
			try {
				Connection con = null;
				try {
					con = this.dsourc.getConnection();
					con.setAutoCommit(true);

					PreparedStatement file_prep_stmt = con.prepareStatement(queryFileString);
					file_prep_stmt.setTimestamp(1, startTime);
					file_prep_stmt.setTimestamp(2, endTime);
					file_prep_stmt.setTimestamp(3, startTime);
					file_prep_stmt.setTimestamp(4, endTime);
					file_prep_stmt.setInt(5, Utility.convertWavebandToInt(wavelength));
					ResultSet imgFileIdResults = file_prep_stmt.executeQuery();

					ArrayList<Integer> idList = new ArrayList<Integer>();
					while (imgFileIdResults.next()) {
						int id = imgFileIdResults.getInt("id");
						idList.add(Integer.valueOf(id));
					}

					boolean gotParams = false;
					while (!gotParams && !idList.isEmpty()) {
						try {
							PreparedStatement param_prep_stmt = con.prepareStatement(queryParamString);
							int id = idList.get(0);
							idList.remove(0);
							param_prep_stmt.setInt(1, id);
							param_prep_stmt.setInt(2, (int) rec.x);
							param_prep_stmt.setInt(3, (int) rec.x + (int) rec.width - 1);
							param_prep_stmt.setInt(4, (int) rec.y);
							param_prep_stmt.setInt(5, (int) rec.y + (int) rec.height - 1);

							ResultSet imgParamRslts = param_prep_stmt.executeQuery();
							int count = 0;
							while (imgParamRslts.next()) {
								int x = imgParamRslts.getInt("x");
								x = x - (int) rec.x;

								int y = imgParamRslts.getInt("y");
								y = y - (int) rec.y;

								for (int i = 1; i < 11; i++) {
									retVal[y][x][i - 1] = imgParamRslts.getFloat("p" + i);
								}
								count++;
							}

							if (count == (int) rec.height * (int) rec.width) {
								executed = true;
								gotParams = true;
							}
						} catch (SQLException ex) {
							ex.printStackTrace();
						}
					}

				} catch (SQLException ex) {
					ex.printStackTrace();
				} finally {
					if (con != null) {
						con.close();
					}
				}
			} catch (SQLException ex) {
				System.out.println(ex.getMessage());
			}
			tryCount++;
		}

		if (this.normalizer != null)
			this.normalizer.normalizeParameterValues(retVal);
		return retVal;
	}

	@Override
	public BufferedImage getFirstImage(Interval period, Waveband wavelength) throws SQLException, IOException {
		Connection con = null;
		BufferedImage bImg = null;
		try {
			con = this.dsourc.getConnection();
			con.setAutoCommit(true);

			// for pulling out the year and month for the table name to select
			// from
			DateTime startTime = period.getStart();

			ImageDBDateIdPair[] pairs = this.getImageIdsForInterval(period, wavelength);
			if (pairs.length > 0) {

				int frameId = pairs[0].id;
				startTime = pairs[0].period.getStart();

				String queryString2 = "SELECT image_file FROM image_files_" + startTime.getYear();
				if ((startTime.getMonthOfYear()) < 10) {
					queryString2 += "0" + (startTime.getMonthOfYear());
				} else {
					queryString2 += "" + (startTime.getMonthOfYear());
				}
				queryString2 += " WHERE file_id = ? ;";
				PreparedStatement img_prep_stmt = con.prepareStatement(queryString2);
				img_prep_stmt.setInt(1, frameId);

				ResultSet imgRslts = img_prep_stmt.executeQuery();

				if (imgRslts.next()) {
					Blob blob = imgRslts.getBlob(1);
					InputStream bStream = blob.getBinaryStream();
					bImg = ImageIO.read(bStream);
					bStream.close();
				}
				imgRslts.close();
			}

		} finally {
			if (con != null) {
				con.close();
			}
		}
		return bImg;
	}

	@Override
	public BufferedImage getFirstFullImage(Interval period, Waveband wavelength) throws SQLException, IOException {
		Connection con = null;
		BufferedImage bImg = null;
		try {

			// for pulling out the year and month for the table name to select
			// from
			DateTime startTime = period.getStart();

			ImageDBDateIdPair[] pairs = this.getImageIdsForInterval(period, wavelength);
			if (pairs.length > 0) {

				con = this.dsourc.getConnection();
				con.setAutoCommit(true);

				int frameId = pairs[0].id;
				startTime = pairs[0].period.getStart();

				String queryString2 = "SELECT image_file FROM image_files_" + startTime.getYear();
				if ((startTime.getMonthOfYear()) < 10) {
					queryString2 += "0" + (startTime.getMonthOfYear());
				} else {
					queryString2 += "" + (startTime.getMonthOfYear());
				}
				queryString2 += "_full WHERE file_id = ? ;";
				PreparedStatement img_prep_stmt = con.prepareStatement(queryString2);
				img_prep_stmt.setInt(1, frameId);
				ResultSet imgRslts = img_prep_stmt.executeQuery();

				if (imgRslts.next()) {
					Blob blob = imgRslts.getBlob(1);
					InputStream bStream = blob.getBinaryStream();
					bImg = ImageIO.read(bStream);
					bStream.close();
				}
				imgRslts.close();
			}

		} finally {
			if (con != null) {
				con.close();
			}
		}
		return bImg;
	}

	private String buildFileQueryString(IEvent ev, boolean isLeftSide) {
		// for constructing the table names from the year and month of
		// the event
		DateTime startTime = ev.getTimePeriod().getStart();

		String calStartYear = "" + startTime.getYear();
		String calStartMonth;
		if ((startTime.getMonthOfYear()) < 10) {
			calStartMonth = "0" + (startTime.getMonthOfYear());
		} else {
			calStartMonth = "" + (startTime.getMonthOfYear());
		}

		StringBuilder queryString = new StringBuilder();
		queryString.append("SELECT id FROM files_");
		queryString.append(calStartYear);
		queryString.append(calStartMonth);
		queryString.append(" WHERE(((startdate BETWEEN ? AND ?) OR (enddate BETWEEN ? AND ?))");
		queryString.append(" AND (wavelength = ?)) ORDER BY startdate");
		if (isLeftSide) {
			queryString.append(" DESC;");
		} else {
			queryString.append(" ASC;");
		}
		return queryString.toString();
	}

	private String buildFileQueryString(Interval period) {
		// for constructing the table names from the year and month of
		// the event
		DateTime startTime = period.getStart();

		String calStartYear = "" + startTime.getYear();
		String calStartMonth;
		if ((startTime.getMonthOfYear()) < 10) {
			calStartMonth = "0" + (startTime.getMonthOfYear());
		} else {
			calStartMonth = "" + (startTime.getMonthOfYear());
		}

		StringBuilder queryString = new StringBuilder();
		queryString.append("SELECT id, startdate FROM files_");
		queryString.append(calStartYear);
		queryString.append(calStartMonth);
		queryString.append(" WHERE(((startdate BETWEEN ? AND ?) OR (enddate BETWEEN ? AND ?))");
		queryString.append(" AND (wavelength = ?)) ORDER BY startdate");
		queryString.append(" DESC;");
		return queryString.toString();
	}


	private String buildQueryParamString(Interval period) {
		// for constructing the table names from the year and month of
		// the event
		DateTime startTime = period.getStart();

		String calStartYear = "" + startTime.getYear();
		String calStartMonth;
		if ((startTime.getMonthOfYear()) < 10) {
			calStartMonth = "0" + (startTime.getMonthOfYear());
		} else {
			calStartMonth = "" + (startTime.getMonthOfYear());
		}
		StringBuilder queryString = new StringBuilder();
		queryString.append("SELECT x, y, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10 ");
		queryString.append("FROM image_params_");
		queryString.append(calStartYear);
		queryString.append(calStartMonth);
		queryString.append(" WHERE ( file_id = ? ) AND (x BETWEEN ? AND ? ) AND (y BETWEEN ? AND ?);");
		return queryString.toString();
	}

	@Override
	public DenseMatrix[] getImageParamForEv(IEvent event, ImageDBWaveParamPair[] params, boolean leftSide) {
		// Get the image parameters for each wavelength in the set of dimensions
		DenseMatrix[] dimMatArr = new DenseMatrix[params.length];
		for (int i = 0; i < params.length; i++) {

			double[][][] paramVals = this.getImageParamForWave(event, params[i].wavelength, leftSide);
			int rows = paramVals.length;
			int cols = paramVals[0].length;

			// get the param indicated by the dims array at depth i
			// the param value is from 1 to 10 whare array index is 0-9
			// hence the -1
			int paramIdx = params[i].parameter - 1;
			double[][] data = new double[rows][cols];
			for (int x = 0; x < cols; x++) {
				for (int y = 0; y < rows; y++) {
					data[y][x] = paramVals[y][x][paramIdx];
				}
			}
			dimMatArr[i] = new JMatrix(data);
		}
		return dimMatArr;
	}

	@Override
	public ImageDBDateIdPair[] getImageIdsForInterval(Interval period, Waveband wavelength) {
		ImageDBDateIdPair[] result = null;
		if (period == null || wavelength == null)
			return result;
		try {
			Connection con = null;
			try {
				con = this.dsourc.getConnection();
				con.setAutoCommit(true);

				DateTime startDateTime = period.getStart();
				DateTime endDateTime = period.getEnd();

				ArrayList<ImageDBDateIdPair> idList = new ArrayList<ImageDBDateIdPair>();

				boolean runAgain = false;
				do {
					Interval queryPeriod = new Interval(startDateTime, endDateTime);
					String queryFileString = this.buildFileQueryString(queryPeriod);
					Timestamp startTime = new Timestamp(queryPeriod.getStartMillis());
					Timestamp endTime = new Timestamp(queryPeriod.getEndMillis());

					PreparedStatement file_prep_stmt = con.prepareStatement(queryFileString);
					file_prep_stmt.setTimestamp(1, startTime);
					file_prep_stmt.setTimestamp(2, endTime);
					file_prep_stmt.setTimestamp(3, startTime);
					file_prep_stmt.setTimestamp(4, endTime);
					file_prep_stmt.setInt(5, Utility.convertWavebandToInt(wavelength));
					ResultSet imgFileIdResults = file_prep_stmt.executeQuery();

					while (imgFileIdResults.next()) {
						int id = imgFileIdResults.getInt("id");
						Timestamp ts = imgFileIdResults.getTimestamp(2);
						ImageDBDateIdPair pair = new ImageDBDateIdPair();
						pair.id = id;
						pair.period = new Interval(ts.getTime(), ts.getTime() + (1000 * 6 * 60));
						idList.add(pair);
					}
					imgFileIdResults.close();

					if (startDateTime.getMonthOfYear() == 12) {
						startDateTime = new DateTime(startDateTime.getYear() + 1, 1, 1, 0, 0, 0);
					} else {
						startDateTime = new DateTime(startDateTime.getYear(), startDateTime.getMonthOfYear() + 1, 1, 0,
								0, 0);
					}

					if (startDateTime.getYear() < endDateTime.getYear()) {
						runAgain = true;
					} else if (startDateTime.getYear() == endDateTime.getYear()) {
						if (startDateTime.getMonthOfYear() <= endDateTime.getMonthOfYear()) {
							runAgain = true;
						} else {
							runAgain = false;
						}
					} else {
						runAgain = false;
					}
				} while (runAgain);
				result = new ImageDBDateIdPair[idList.size()];
				for (int i = 0; i < result.length; i++)
					result[i] = idList.get(i);
			} catch (SQLException ex) {
				ex.printStackTrace();
			} finally {
				if (con != null) {
					con.close();
				}
			}
		} catch (SQLException ex) {
			System.out.println(ex.getMessage());
		}
		return result;
	}

	@Override
	public DenseMatrix[] getImageParamForId(Interval period, int id) {
		String queryParamString = this.buildQueryParamString(period);
		DenseMatrix[] dimMatArr = new DenseMatrix[10];
		try {
			Connection con = this.dsourc.getConnection();
			con.setAutoCommit(true);
			try {
				PreparedStatement param_prep_stmt = con.prepareStatement(queryParamString);

				param_prep_stmt.setInt(1, id);
				param_prep_stmt.setInt(2, (int) 1);
				param_prep_stmt.setInt(3, (int) 64);
				param_prep_stmt.setInt(4, (int) 1);
				param_prep_stmt.setInt(5, (int) 64);

				ResultSet imgParamRslts = param_prep_stmt.executeQuery();

				double[][][] retVal = new double[64][64][10];
				while (imgParamRslts.next()) {
					int x = imgParamRslts.getInt("x");

					int y = imgParamRslts.getInt("y");

					for (int i = 1; i < 11; i++) {
						retVal[y - 1][x - 1][i - 1] = imgParamRslts.getFloat("p" + i);
					}
				}

				if (this.normalizer != null)
					this.normalizer.normalizeParameterValues(retVal);
				int rows = retVal.length;
				int cols = retVal[0].length;

				// get the param indicated by the dims array at depth i
				// the param value is from 1 to 10 whare array index is 0-9
				// hence the -1
				for (int paramIdx = 0; paramIdx < 10; paramIdx++) {
					double[][] data = new double[rows][cols];
					for (int x = 0; x < cols; x++) {
						for (int y = 0; y < rows; y++) {
							data[y][x] = retVal[y][x][paramIdx];
						}
					}
					dimMatArr[paramIdx] = new JMatrix(data);
				}

			} catch (SQLException ex) {
				ex.printStackTrace();

			} finally {
				if (con != null) {
					con.close();
				}
			}
		} catch (SQLException ex) {
			System.out.println(ex.getMessage());
		}
		return dimMatArr;
	}

	@Override
	public BufferedImage getImgForId(Interval period, int id) throws SQLException, IOException {
		Connection con = null;
		BufferedImage bImg = null;
		try {
			con = this.dsourc.getConnection();
			con.setAutoCommit(true);

			// for pulling out the year and month for the table name to select
			// from
			DateTime startTime = period.getStart();
			// Calendar calStart = Calendar.getInstance();
			// calStart.setTimeInMillis(period.getStartMillis());

			String queryString2 = "SELECT image_file FROM image_files_" + startTime.getYear();
			if (startTime.getMonthOfYear() < 10) {
				queryString2 += "0" + (startTime.getMonthOfYear());
			} else {
				queryString2 += "" + (startTime.getMonthOfYear());
			}
			queryString2 += " WHERE file_id = ? ;";
			PreparedStatement img_prep_stmt = con.prepareStatement(queryString2);
			img_prep_stmt.setInt(1, id);

			ResultSet imgRslts = img_prep_stmt.executeQuery();

			if (imgRslts.next()) {
				Blob blob = imgRslts.getBlob(1);
				bImg = ImageIO.read(blob.getBinaryStream());
				blob.free();
			}
			imgRslts.close();
			img_prep_stmt.close();
		} finally {
			if (con != null) {
				con.close();
			}
		}
		return bImg;
	}

	public BufferedImage getFullImgForId(Interval period, int id) throws SQLException, IOException {
		Connection con = null;
		BufferedImage bImg = null;
		try {
			con = this.dsourc.getConnection();
			con.setAutoCommit(true);

			// for pulling out the year and month for the table name to select
			// from
			DateTime startTime = period.getStart();
			// Calendar calStart = Calendar.getInstance();
			// calStart.setTimeInMillis(period.getStartMillis());

			String queryString2 = "SELECT image_file FROM image_files_" + startTime.getYear();
			if (startTime.getMonthOfYear() < 10) {
				queryString2 += "0" + (startTime.getMonthOfYear());
			} else {
				queryString2 += "" + (startTime.getMonthOfYear());
			}
			queryString2 += "_full WHERE file_id = ? ;";
			PreparedStatement img_prep_stmt = con.prepareStatement(queryString2);
			img_prep_stmt.setInt(1, id);

			ResultSet imgRslts = img_prep_stmt.executeQuery();

			if (imgRslts.next()) {
				Blob blob = imgRslts.getBlob(1);
				bImg = ImageIO.read(blob.getBinaryStream());
				blob.free();
			}
			imgRslts.close();
			img_prep_stmt.close();
		} finally {
			if (con != null) {
				con.close();
			}
		}
		return bImg;
	}

	@Override
	public ImageDBFitsHeaderData getHeaderForId(Interval period, int id) throws SQLException {
		Connection con = null;
		ImageDBFitsHeaderData headerData = null;
		try {
			con = this.dsourc.getConnection();
			con.setAutoCommit(true);

			// for pulling out the year and month for the table name to select
			// from
			DateTime startTime = period.getStart();
			// Calendar calStart = Calendar.getInstance();
			// calStart.setTimeInMillis(period.getStartMillis());

			String queryString = "SELECT x0, y0, rSun, dSun, cdelt FROM image_header_" + startTime.getYear();
			if ((startTime.getMonthOfYear()) < 10) {
				queryString += "0" + (startTime.getMonthOfYear());
			} else {
				queryString += "" + (startTime.getMonthOfYear());
			}
			queryString += " WHERE file_id = ? ;";
			PreparedStatement hdr_prep_stmt = con.prepareStatement(queryString);
			hdr_prep_stmt.setInt(1, id);

			ResultSet hdrRslts = hdr_prep_stmt.executeQuery();

			if (hdrRslts.next()) {
				headerData = new ImageDBFitsHeaderData();
				headerData.X0 = hdrRslts.getDouble(1);
				headerData.Y0 = hdrRslts.getDouble(2);
				headerData.R_SUN = hdrRslts.getDouble(3);
				headerData.DSUN = hdrRslts.getDouble(4);
				headerData.CDELT = hdrRslts.getDouble(5);
			}
			hdrRslts.close();
			hdr_prep_stmt.close();
		} finally {
			if (con != null) {
				con.close();
			}
		}

		return headerData;
	}
}