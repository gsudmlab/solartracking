/**
 * SolarTracking Copyright (C) 2017 Georgia State University
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.gsu.dmlab.databases;

import java.util.ArrayList;

import javax.sql.DataSource;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import edu.gsu.dmlab.datatypes.AREvent;
import edu.gsu.dmlab.datatypes.CHEvent;
import edu.gsu.dmlab.datatypes.EFEvent;
import edu.gsu.dmlab.datatypes.EventType;
import edu.gsu.dmlab.datatypes.FIEvent;
import edu.gsu.dmlab.datatypes.FLEvent;
import edu.gsu.dmlab.datatypes.SGEvent;
import edu.gsu.dmlab.datatypes.SSEvent;
import edu.gsu.dmlab.datatypes.interfaces.IEvent;
import edu.gsu.dmlab.exceptions.UnknownEventTypeException;

/**
 * Class that is used for creating the database and inserting the data for all
 * events in the dataset from [Supporting Data: A large-scale dataset of solar
 * event reports from automated feature recognition
 * modules](https://doi.org/10.5281/zenodo.48187)
 * 
 * @author Dustin Kempton, Data Mining Lab, Georgia State University
 * @version {@value edu.gsu.dmlab.ConstValues#major_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#minor_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#build_ver}
 */
public class TrackDBCreator {

	private DataSource dsourc;

	/**
	 * Constructor
	 * 
	 * @param dsourc
	 *            the datasource connection to the database schema to create the
	 *            tables in.
	 */
	public TrackDBCreator(DataSource dsourc) {
		if (dsourc == null)
			throw new IllegalArgumentException("DataSource cannot be null in TrackDBCreator constructor.");
		this.dsourc = dsourc;
	}

	/**
	 * Method for inserting a list of events.
	 * 
	 * @param eventList
	 * @return True when done without errors.
	 * @throws SQLException
	 * @throws UnknownEventTypeException
	 */
	public boolean insertEvents(ArrayList<IEvent> eventList) throws SQLException, UnknownEventTypeException {
		Connection con = null;
		boolean done = false;
		try {
			con = this.dsourc.getConnection();
			con.setAutoCommit(true);

			if (eventList.size() > 0) {
				// Check for the event table and create if not there.
				String query = this.queryTableExistsFilesString(eventList.get(0).getType());
				PreparedStatement tableExistsPrepStmt = con.prepareStatement(query);
				ResultSet res = tableExistsPrepStmt.executeQuery();
				if (!res.next()) {
					query = this.createFilesTableString(eventList.get(0).getType());
					tableExistsPrepStmt = con.prepareStatement(query);
					tableExistsPrepStmt.execute();
				}

				// insert the events into the table
				query = this.insetEventString(eventList.get(0).getType());
				PreparedStatement stmt = con.prepareStatement(query);
				for (IEvent event : eventList) {
					this.setStmt(stmt, event);
					stmt.addBatch();
				}
				int[] results = stmt.executeBatch();
				for (int resVal : results)
					if (resVal == 0)
						throw new SQLException("Insert event failed, no rows affected one one of the inserts.");
			}
			done = true;

		} finally {
			if (con != null) {
				con.close();
			}
		}

		return done;
	}

	private void setStmt(PreparedStatement stmt, IEvent event) throws UnknownEventTypeException, SQLException {

		switch (event.getType()) {
		case ACTIVE_REGION:
			this.setARParams(stmt, event);
			break;
		case CORONAL_HOLE:
			this.setCHParams(stmt, event);
			break;
		case EMERGING_FLUX:
			this.setEFParams(stmt, event);
			break;
		case FILAMENT:
			this.setFIParams(stmt, event);
			break;
		case FLARE:
			this.setFLParams(stmt, event);
			break;
		case SIGMOID:
			this.setSGParams(stmt, event);
			break;
		case SUNSPOT:
			this.setSSParams(stmt, event);
			break;
		default:
			throw new UnknownEventTypeException("Unrecognized event type: " + event.getType());
		}
	}

	private String queryTableExistsFilesString(EventType type) throws UnknownEventTypeException {
		switch (type) {
		case ACTIVE_REGION:
			return "SHOW TABLES LIKE 'hekevents_ar';";
		case CORONAL_HOLE:
			return "SHOW TABLES LIKE 'hekevents_ch';";
		case EMERGING_FLUX:
			return "SHOW TABLES LIKE 'hekevents_ef';";
		case FILAMENT:
			return "SHOW TABLES LIKE 'hekevents_fi';";
		case FLARE:
			return "SHOW TABLES LIKE 'hekevents_fl';";
		case SIGMOID:
			return "SHOW TABLES LIKE 'hekevents_sg';";
		case SUNSPOT:
			return "SHOW TABLES LIKE 'hekevents_ss';";
		default:
			throw new UnknownEventTypeException("Unrecognized event type: " + type);
		}
	}

	private String createFilesTableString(EventType type) throws UnknownEventTypeException {
		switch (type) {
		case ACTIVE_REGION:
			return this.createARString();
		case CORONAL_HOLE:
			return this.createCHString();
		case EMERGING_FLUX:
			return this.createEFString();
		case FILAMENT:
			return this.createFIString();
		case FLARE:
			return this.createFLString();
		case SIGMOID:
			return this.createSGString();
		case SUNSPOT:
			return this.createSSString();
		default:
			throw new UnknownEventTypeException("Unrecognized event type: " + type);
		}
	}

	private String insetEventString(EventType type) throws UnknownEventTypeException {
		switch (type) {
		case ACTIVE_REGION:
			return this.insertARString();
		case CORONAL_HOLE:
			return this.insertCHString();
		case EMERGING_FLUX:
			return this.insertEFString();
		case FILAMENT:
			return this.insertFIString();
		case FLARE:
			return this.insertFLString();
		case SIGMOID:
			return this.insertSGString();
		case SUNSPOT:
			return this.insertSSString();
		default:
			throw new UnknownEventTypeException("Unrecognized event type: " + type);
		}
	}

	private void trySetDouble(PreparedStatement stmt, int idx, double val) throws SQLException {
		if (Double.isNaN(val)) {
			stmt.setNull(idx, java.sql.Types.REAL);
		} else {
			stmt.setDouble(idx, val);
		}
	}

	private void trySetInt(PreparedStatement stmt, int idx, int val) throws SQLException {
		if (Integer.MIN_VALUE == val) {
			stmt.setNull(idx, java.sql.Types.INTEGER);
		} else {
			stmt.setInt(idx, val);
		}
	}

	////////////////////////////// FI Functions\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
	private void setFIParams(PreparedStatement stmt, IEvent event) throws SQLException {
		FIEvent ev = (FIEvent) event;
		stmt.setString(1, "FI");
		stmt.setTimestamp(2, new Timestamp(ev.getTimePeriod().getStartMillis()));
		stmt.setTimestamp(3, new Timestamp(ev.getTimePeriod().getEndMillis()));
		stmt.setString(4, ev.getObservatory());
		stmt.setString(5, ev.getInstrument());
		stmt.setString(6, ev.getChannel());
		stmt.setString(7, ev.getHpcCenterLocation());
		stmt.setString(8, ev.getHpcBBox());
		stmt.setString(9, ev.getHpcShape());
		stmt.setDouble(10, ev.getArea_raw());
		stmt.setString(11, ev.getArea_unit());
		stmt.setDouble(12, ev.getEvent_npixels());
		stmt.setString(13, ev.getEvent_pixelunit());
		stmt.setDouble(14, ev.getFi_length());
		stmt.setString(15, ev.getFi_lengthunit());
		stmt.setDouble(16, ev.getFi_tilt());
		stmt.setDouble(17, ev.getFi_barbstot());
		stmt.setDouble(18, ev.getFi_barbsr());
		stmt.setDouble(19, ev.getFi_barbsl());
		stmt.setDouble(20, ev.getFi_chirality());
		stmt.setString(21, ev.getFi_barbsstartc1());
		stmt.setString(22, ev.getFi_barbsstartc2());
		stmt.setString(23, ev.getFi_barbsendc1());
		stmt.setString(24, ev.getFi_barbsendc2());
		stmt.setDouble(25, ev.getFrm_versionnumber());

	}

	private String insertFIString() {
		StringBuilder sb = new StringBuilder();
		sb.append("INSERT INTO `hekevents_fi` (");
		sb.append("`eventType`, ");
		sb.append("`startTime`, ");
		sb.append("`endTime`, ");
		sb.append("`observatory`, ");
		sb.append("`instrument`, ");
		sb.append("`channel`, ");
		sb.append("`center`, ");
		sb.append("`bbox`, ");
		sb.append("`ccode`, ");
		sb.append("`area_raw`, ");
		sb.append("`area_unit`, ");
		sb.append("`event_npixels`, ");
		sb.append("`event_pixelunit`, ");
		sb.append("`fi_length`, ");
		sb.append("`fi_lengthunit`, ");
		sb.append("`fi_tilt`, ");
		sb.append("`fi_barbstot`, ");
		sb.append("`fi_barbsr`, ");
		sb.append("`fi_barbsl`, ");
		sb.append("`fi_chirality`, ");
		sb.append("`fi_barbsstartc1`, ");
		sb.append("`fi_barbsstartc2`, ");
		sb.append("`fi_barbsendc1`, ");
		sb.append("`fi_barbsendc2`, ");
		sb.append("`frm_versionnumber` ) ");
		sb.append("VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);");
		return sb.toString();
	}

	private String createFIString() {
		StringBuilder sb = new StringBuilder();

		sb.append("CREATE TABLE `hekevents_fi` ( ");
		sb.append("`eventID` integer unsigned NOT NULL AUTO_INCREMENT, ");
		sb.append("`eventType` varchar(255), ");
		sb.append("`startTime` timestamp DEFAULT '1970-01-01 00:00:01', ");
		sb.append("`endTime` timestamp DEFAULT '1970-01-01 00:00:01', ");
		sb.append("`observatory` varchar(255), ");
		sb.append("`instrument` varchar(255), ");
		sb.append("`channel` varchar(255), ");
		sb.append("`center` text, ");
		sb.append("`bbox` text, ");
		sb.append("`ccode` text, ");
		sb.append("`area_raw` real, ");
		sb.append("`area_unit` varchar(255), ");
		sb.append("`event_npixels` real, ");
		sb.append("`event_pixelunit` varchar(255), ");
		sb.append("`fi_length` real, ");
		sb.append("`fi_lengthunit` varchar(255), ");
		sb.append("`fi_tilt` real, ");
		sb.append("`fi_barbstot` real, ");
		sb.append("`fi_barbsr` real, ");
		sb.append("`fi_barbsl` real, ");
		sb.append("`fi_chirality` real, ");
		sb.append("`fi_barbsstartc1` text, ");
		sb.append("`fi_barbsstartc2` text, ");
		sb.append("`fi_barbsendc1` text, ");
		sb.append("`fi_barbsendc2` text, ");
		sb.append("`frm_versionnumber` real, ");
		sb.append("PRIMARY KEY (`eventID`), ");
		sb.append("KEY `startdate_fi_idx` (`startTime`) ");
		sb.append(")ENGINE=MyISAM AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;");
		return sb.toString();
	}

	//////////////////// AR Functions\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

	private void setARParams(PreparedStatement stmt, IEvent event) throws SQLException {
		AREvent ev = (AREvent) event;

		stmt.setString(1, "AR");
		stmt.setTimestamp(2, new Timestamp(ev.getTimePeriod().getStartMillis()));
		stmt.setTimestamp(3, new Timestamp(ev.getTimePeriod().getEndMillis()));
		stmt.setString(4, ev.getObservatory());
		stmt.setString(5, ev.getInstrument());
		stmt.setString(6, ev.getChannel());
		stmt.setString(7, ev.getHpcCenterLocation());
		stmt.setString(8, ev.getHpcBBox());
		stmt.setString(9, ev.getHpcShape());
		this.trySetDouble(stmt, 10, ev.getArea_raw());
		stmt.setString(11, ev.getArea_unit());
		this.trySetDouble(stmt, 12, ev.getArea_uncert());
		this.trySetDouble(stmt, 13, ev.getArea_atdiskcenter());
		this.trySetDouble(stmt, 14, ev.getArea_atdiskcenteruncert());
		this.trySetDouble(stmt, 15, ev.getEvent_npixels());
		stmt.setString(16, ev.getEvent_pixelunit());
		this.trySetDouble(stmt, 17, ev.getIntensmin());
		this.trySetDouble(stmt, 18, ev.getIntensmax());
		this.trySetDouble(stmt, 19, ev.getIntensmean());
		this.trySetDouble(stmt, 20, ev.getIntensmedian());
		this.trySetDouble(stmt, 21, ev.getIntensvar());
		this.trySetDouble(stmt, 22, ev.getIntensskew());
		this.trySetDouble(stmt, 23, ev.getIntenskurt());
		this.trySetDouble(stmt, 24, ev.getIntenstotal());
		stmt.setString(25, ev.getIntensunit());
		this.trySetDouble(stmt, 26, ev.getFrm_versionnumber());

	}

	private String createARString() {
		StringBuilder sb = new StringBuilder();

		sb.append("CREATE TABLE `hekevents_ar` ( ");
		sb.append("`eventID` integer unsigned NOT NULL AUTO_INCREMENT, ");
		sb.append("`eventType` varchar(255), ");
		sb.append("`startTime` timestamp DEFAULT '1970-01-01 00:00:01', ");
		sb.append("`endTime` timestamp DEFAULT '1970-01-01 00:00:01', ");
		sb.append("`observatory` varchar(255), ");
		sb.append("`instrument` varchar(255), ");
		sb.append("`channel` varchar(255), ");
		sb.append("`center` text, ");
		sb.append("`bbox` text, ");
		sb.append("`ccode` text, ");
		sb.append("`area_raw` real, ");
		sb.append("`area_unit` varchar(255), ");
		sb.append("`area_uncert` real, ");
		sb.append("`area_atdiskcenter` real, ");
		sb.append("`area_atdiskcenteruncert` real, ");
		sb.append("`event_npixels` real, ");
		sb.append("`event_pixelunit` varchar(255), ");
		sb.append("`intensmin` real, ");
		sb.append("`intensmax` real, ");
		sb.append("`intensmean` real, ");
		sb.append("`intensmedian` real, ");
		sb.append("`intensvar` real, ");
		sb.append("`intensskew` real, ");
		sb.append("`intenskurt` real, ");
		sb.append("`intenstotal` real, ");
		sb.append("`intensunit` varchar(255), ");
		sb.append("`frm_versionnumber` real, ");
		sb.append("PRIMARY KEY (`eventID`), ");
		sb.append("KEY `startdate_ar_idx` (`startTime`) ");
		sb.append(")ENGINE=MyISAM AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;");
		return sb.toString();
	}

	private String insertARString() {
		StringBuilder sb = new StringBuilder();
		sb.append("INSERT INTO `hekevents_ar` (");
		sb.append("`eventType`, ");
		sb.append("`startTime`, ");
		sb.append("`endTime`, ");
		sb.append("`observatory`, ");
		sb.append("`instrument`, ");
		sb.append("`channel`, ");
		sb.append("`center`, ");
		sb.append("`bbox`, ");
		sb.append("`ccode`, ");
		sb.append("`area_raw`, ");
		sb.append("`area_unit`, ");
		sb.append("`area_uncert`, ");
		sb.append("`area_atdiskcenter`, ");
		sb.append("`area_atdiskcenteruncert`, ");
		sb.append("`event_npixels`, ");
		sb.append("`event_pixelunit`, ");
		sb.append("`intensmin`, ");
		sb.append("`intensmax`, ");
		sb.append("`intensmean`, ");
		sb.append("`intensmedian`, ");
		sb.append("`intensvar`, ");
		sb.append("`intensskew`, ");
		sb.append("`intenskurt`, ");
		sb.append("`intenstotal`, ");
		sb.append("`intensunit`, ");
		sb.append("`frm_versionnumber` ) ");
		sb.append("VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);");
		return sb.toString();
	}

	////////////////////////// CH Events\\\\\\\\\\\\\\\\\\\
	private void setCHParams(PreparedStatement stmt, IEvent event) throws SQLException {
		CHEvent ev = (CHEvent) event;

		stmt.setString(1, "CH");
		stmt.setTimestamp(2, new Timestamp(ev.getTimePeriod().getStartMillis()));
		stmt.setTimestamp(3, new Timestamp(ev.getTimePeriod().getEndMillis()));
		stmt.setString(4, ev.getObservatory());
		stmt.setString(5, ev.getInstrument());
		stmt.setString(6, ev.getChannel());
		stmt.setString(7, ev.getHpcCenterLocation());
		stmt.setString(8, ev.getHpcBBox());
		stmt.setString(9, ev.getHpcShape());
		this.trySetDouble(stmt, 10, ev.getArea_raw());
		stmt.setString(11, ev.getArea_unit());
		this.trySetDouble(stmt, 12, ev.getArea_uncert());
		this.trySetDouble(stmt, 13, ev.getArea_atdiskcenter());
		this.trySetDouble(stmt, 14, ev.getArea_atdiskcenteruncert());
		this.trySetDouble(stmt, 15, ev.getEvent_npixels());
		stmt.setString(16, ev.getEvent_pixelunit());
		this.trySetDouble(stmt, 17, ev.getIntensmin());
		this.trySetDouble(stmt, 18, ev.getIntensmax());
		this.trySetDouble(stmt, 19, ev.getIntensmean());
		this.trySetDouble(stmt, 20, ev.getIntensmedian());
		this.trySetDouble(stmt, 21, ev.getIntensvar());
		this.trySetDouble(stmt, 22, ev.getIntensskew());
		this.trySetDouble(stmt, 23, ev.getIntenskurt());
		this.trySetDouble(stmt, 24, ev.getIntenstotal());
		stmt.setString(25, ev.getIntensunit());
		this.trySetDouble(stmt, 26, ev.getFrm_versionnumber());

	}

	private String createCHString() {
		StringBuilder sb = new StringBuilder();

		sb.append("CREATE TABLE `hekevents_ch` ( ");
		sb.append("`eventID` integer unsigned NOT NULL AUTO_INCREMENT, ");
		sb.append("`eventType` varchar(255), ");
		sb.append("`startTime` timestamp DEFAULT '1970-01-01 00:00:01', ");
		sb.append("`endTime` timestamp DEFAULT '1970-01-01 00:00:01', ");
		sb.append("`observatory` varchar(255), ");
		sb.append("`instrument` varchar(255), ");
		sb.append("`channel` varchar(255), ");
		sb.append("`center` text, ");
		sb.append("`bbox` text, ");
		sb.append("`ccode` text, ");
		sb.append("`area_raw` real, ");
		sb.append("`area_unit` varchar(255), ");
		sb.append("`area_uncert` real, ");
		sb.append("`area_atdiskcenter` real, ");
		sb.append("`area_atdiskcenteruncert` real, ");
		sb.append("`event_npixels` real, ");
		sb.append("`event_pixelunit` varchar(255), ");
		sb.append("`intensmin` real, ");
		sb.append("`intensmax` real, ");
		sb.append("`intensmean` real, ");
		sb.append("`intensmedian` real, ");
		sb.append("`intensvar` real, ");
		sb.append("`intensskew` real, ");
		sb.append("`intenskurt` real, ");
		sb.append("`intenstotal` real, ");
		sb.append("`intensunit` varchar(255), ");
		sb.append("`frm_versionnumber` real, ");
		sb.append("PRIMARY KEY (`eventID`), ");
		sb.append("KEY `startdate_ch_idx` (`startTime`) ");
		sb.append(")ENGINE=MyISAM AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;");
		return sb.toString();
	}

	private String insertCHString() {
		StringBuilder sb = new StringBuilder();
		sb.append("INSERT INTO `hekevents_ch` (");
		sb.append("`eventType`, ");
		sb.append("`startTime`, ");
		sb.append("`endTime`, ");
		sb.append("`observatory`, ");
		sb.append("`instrument`, ");
		sb.append("`channel`, ");
		sb.append("`center`, ");
		sb.append("`bbox`, ");
		sb.append("`ccode`, ");
		sb.append("`area_raw`, ");
		sb.append("`area_unit`, ");
		sb.append("`area_uncert`, ");
		sb.append("`area_atdiskcenter`, ");
		sb.append("`area_atdiskcenteruncert`, ");
		sb.append("`event_npixels`, ");
		sb.append("`event_pixelunit`, ");
		sb.append("`intensmin`, ");
		sb.append("`intensmax`, ");
		sb.append("`intensmean`, ");
		sb.append("`intensmedian`, ");
		sb.append("`intensvar`, ");
		sb.append("`intensskew`, ");
		sb.append("`intenskurt`, ");
		sb.append("`intenstotal`, ");
		sb.append("`intensunit`, ");
		sb.append("`frm_versionnumber` ) ");
		sb.append("VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);");
		return sb.toString();
	}

	/////////////////// EF Events\\\\\\\\\\\\\\\\\\\\\\\\\\\\
	private void setEFParams(PreparedStatement stmt, IEvent event) throws SQLException {
		EFEvent ev = (EFEvent) event;
		stmt.setString(1, "EF");
		stmt.setTimestamp(2, new Timestamp(ev.getTimePeriod().getStartMillis()));
		stmt.setTimestamp(3, new Timestamp(ev.getTimePeriod().getEndMillis()));
		stmt.setString(4, ev.getObservatory());
		stmt.setString(5, ev.getInstrument());
		stmt.setString(6, ev.getChannel());
		stmt.setString(7, ev.getHpcCenterLocation());
		stmt.setString(8, ev.getHpcBBox());
		stmt.setString(9, ev.getHpcShape());
		this.trySetDouble(stmt, 10, ev.getArea_atdiskcenter());
		this.trySetDouble(stmt, 11, ev.getArea_atdiskcenteruncert());
		this.trySetDouble(stmt, 12, ev.getArea_raw());
		this.trySetDouble(stmt, 13, ev.getArea_uncert());
		stmt.setString(14, ev.getArea_unit());
		this.trySetDouble(stmt, 15, ev.getEvent_npixels());
		stmt.setString(16, ev.getEvent_pixelunit());
		this.trySetDouble(stmt, 17, ev.getEf_pospeakfluxonsetrate());
		this.trySetDouble(stmt, 18, ev.getEf_negpeakfluxonsetrate());
		stmt.setString(19, ev.getEf_onsetrateunit());
		this.trySetDouble(stmt, 20, ev.getEf_sumpossignedflux());
		this.trySetDouble(stmt, 21, ev.getEf_sumnegsignedflux());
		stmt.setString(22, ev.getEf_fluxunit());
		this.trySetDouble(stmt, 23, ev.getEf_axisorientation());
		stmt.setString(24, ev.getEf_axisorientationunit());
		this.trySetDouble(stmt, 25, ev.getEf_axislength());
		this.trySetDouble(stmt, 26, ev.getEf_posequivradius());
		this.trySetDouble(stmt, 27, ev.getEf_negequivradius());
		stmt.setString(28, ev.getEf_lengthunit());
		this.trySetDouble(stmt, 29, ev.getEf_aspectratio());
		this.trySetDouble(stmt, 30, ev.getEf_proximityratio());
		this.trySetDouble(stmt, 31, ev.getMaxmagfieldstrength());
		stmt.setString(32, ev.getMaxmagfieldstrengthunit());
		this.trySetDouble(stmt, 33, ev.getFrm_versionnumber());

	}

	private String createEFString() {
		StringBuilder sb = new StringBuilder();

		sb.append("CREATE TABLE `hekevents_ef` ( ");
		sb.append("`eventID` integer unsigned NOT NULL AUTO_INCREMENT, ");
		sb.append("`eventType` varchar(255), ");
		sb.append("`startTime` timestamp DEFAULT '1970-01-01 00:00:01', ");
		sb.append("`endTime` timestamp DEFAULT '1970-01-01 00:00:01', ");
		sb.append("`observatory` varchar(255), ");
		sb.append("`instrument` varchar(255), ");
		sb.append("`channel` varchar(255), ");
		sb.append("`center` text, ");
		sb.append("`bbox` text, ");
		sb.append("`ccode` text, ");
		sb.append("`area_atdiskcenter` real, ");
		sb.append("`area_atdiskcenteruncert` real, ");
		sb.append("`area_raw` real, ");
		sb.append("`area_uncert` real, ");
		sb.append("`area_unit` varchar(255), ");
		sb.append("`event_npixels` real, ");
		sb.append("`event_pixelunit` varchar(255), ");
		sb.append("`ef_pospeakfluxonsetrate` real, ");
		sb.append("`ef_negpeakfluxonsetrate` real, ");
		sb.append("`ef_onsetrateunit` varchar(255), ");
		sb.append("`ef_sumpossignedflux` real, ");
		sb.append("`ef_sumnegsignedflux` real, ");
		sb.append("`ef_fluxunit` varchar(255), ");
		sb.append("`ef_axisorientation` real, ");
		sb.append("`ef_axisorientationunit` varchar(255), ");
		sb.append("`ef_axislength` real, ");
		sb.append("`ef_posequivradius` real, ");
		sb.append("`ef_negequivradius` real, ");
		sb.append("`ef_lengthunit` varchar(255), ");
		sb.append("`ef_aspectratio` real, ");
		sb.append("`ef_proximityratio` real, ");
		sb.append("`maxmagfieldstrength` real, ");
		sb.append("`maxmagfieldstrengthunit` varchar(255), ");
		sb.append("`frm_versionnumber` real, ");
		sb.append("PRIMARY KEY (`eventID`), ");
		sb.append("KEY `startdate_ef_idx` (`startTime`) ");
		sb.append(")ENGINE=MyISAM AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;");
		return sb.toString();
	}

	private String insertEFString() {

		StringBuilder sb = new StringBuilder();
		sb.append("INSERT INTO `hekevents_ef` (");
		sb.append("`eventType`, ");
		sb.append("`startTime`, ");
		sb.append("`endTime`, ");
		sb.append("`observatory`, ");
		sb.append("`instrument`, ");
		sb.append("`channel`, ");
		sb.append("`center`, ");
		sb.append("`bbox`, ");
		sb.append("`ccode`, ");
		sb.append("`area_atdiskcenter`, ");
		sb.append("`area_atdiskcenteruncert`, ");
		sb.append("`area_raw`, ");
		sb.append("`area_uncert`, ");
		sb.append("`area_unit`, ");
		sb.append("`event_npixels`, ");
		sb.append("`event_pixelunit`, ");
		sb.append("`ef_pospeakfluxonsetrate`, ");
		sb.append("`ef_negpeakfluxonsetrate`, ");
		sb.append("`ef_onsetrateunit`, ");
		sb.append("`ef_sumpossignedflux`, ");
		sb.append("`ef_sumnegsignedflux`, ");
		sb.append("`ef_fluxunit`, ");
		sb.append("`ef_axisorientation`, ");
		sb.append("`ef_axisorientationunit`, ");
		sb.append("`ef_axislength`, ");
		sb.append("`ef_posequivradius`, ");
		sb.append("`ef_negequivradius`, ");
		sb.append("`ef_lengthunit`, ");
		sb.append("`ef_aspectratio`, ");
		sb.append("`ef_proximityratio`, ");
		sb.append("`maxmagfieldstrength`, ");
		sb.append("`maxmagfieldstrengthunit`, ");
		sb.append("`frm_versionnumber` ) ");
		sb.append("VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);");
		return sb.toString();
	}

	/////////////////////// FL Events\\\\\\\\\\\\\\\\\\\\
	private void setFLParams(PreparedStatement stmt, IEvent event) throws SQLException {
		FLEvent ev = (FLEvent) event;
		stmt.setString(1, "FL");
		stmt.setTimestamp(2, new Timestamp(ev.getTimePeriod().getStartMillis()));
		stmt.setTimestamp(3, new Timestamp(ev.getTimePeriod().getEndMillis()));
		stmt.setString(4, ev.getObservatory());
		stmt.setString(5, ev.getInstrument());
		stmt.setString(6, ev.getChannel());
		stmt.setString(7, ev.getHpcCenterLocation());
		stmt.setString(8, ev.getHpcBBox());
		stmt.setString(9, ev.getHpcShape());
		this.trySetDouble(stmt, 10, ev.getFl_peakflux());
		this.trySetDouble(stmt, 11, ev.getFrm_versionnumber());

	}

	private String createFLString() {
		StringBuilder sb = new StringBuilder();

		sb.append("CREATE TABLE `hekevents_fl` ( ");
		sb.append("`eventID` integer unsigned NOT NULL AUTO_INCREMENT, ");
		sb.append("`eventType` varchar(255), ");
		sb.append("`startTime` timestamp DEFAULT '1970-01-01 00:00:01', ");
		sb.append("`endTime` timestamp DEFAULT '1970-01-01 00:00:01', ");
		sb.append("`observatory` varchar(255), ");
		sb.append("`instrument` varchar(255), ");
		sb.append("`channel` varchar(255), ");
		sb.append("`center` text, ");
		sb.append("`bbox` text, ");
		sb.append("`ccode` text, ");
		sb.append("`fl_peakflux` real, ");
		sb.append("`frm_versionnumber` real, ");
		sb.append("PRIMARY KEY (`eventID`), ");
		sb.append("KEY `startdate_fl_idx` (`startTime`) ");
		sb.append(")ENGINE=MyISAM AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;");
		return sb.toString();
	}

	private String insertFLString() {

		StringBuilder sb = new StringBuilder();
		sb.append("INSERT INTO `hekevents_fl` (");
		sb.append("`eventType`, ");
		sb.append("`startTime`, ");
		sb.append("`endTime`, ");
		sb.append("`observatory`, ");
		sb.append("`instrument`, ");
		sb.append("`channel`, ");
		sb.append("`center`, ");
		sb.append("`bbox`, ");
		sb.append("`ccode`, ");
		sb.append("`fl_peakflux`, ");
		sb.append("`frm_versionnumber` ) ");
		sb.append("VALUES(?,?,?,?,?,?,?,?,?,?,?);");
		return sb.toString();
	}

	/////////////////////////// SG Events\\\\\\\\\\\\\\\\\\
	private void setSGParams(PreparedStatement stmt, IEvent event) throws SQLException {
		SGEvent ev = (SGEvent) event;
		stmt.setString(1, "SG");
		stmt.setTimestamp(2, new Timestamp(ev.getTimePeriod().getStartMillis()));
		stmt.setTimestamp(3, new Timestamp(ev.getTimePeriod().getEndMillis()));
		stmt.setString(4, ev.getObservatory());
		stmt.setString(5, ev.getInstrument());
		stmt.setString(6, ev.getChannel());
		stmt.setString(7, ev.getHpcCenterLocation());
		stmt.setString(8, ev.getHpcBBox());
		stmt.setString(9, ev.getHpcShape());
		this.trySetInt(stmt, 10, ev.getAr_noaanum());
		stmt.setString(11, ev.getSg_shape());
		this.trySetDouble(stmt, 12, ev.getSg_aspectratio());
		this.trySetDouble(stmt, 13, ev.getFrm_versionnumber());

	}

	private String createSGString() {
		StringBuilder sb = new StringBuilder();
		sb.append("CREATE TABLE `hekevents_sg` ( ");
		sb.append("`eventID` integer unsigned NOT NULL AUTO_INCREMENT, ");
		sb.append("`eventType` varchar(255), ");
		sb.append("`startTime` timestamp DEFAULT '1970-01-01 00:00:01', ");
		sb.append("`endTime` timestamp DEFAULT '1970-01-01 00:00:01', ");
		sb.append("`observatory` varchar(255), ");
		sb.append("`instrument` varchar(255), ");
		sb.append("`channel` varchar(255), ");
		sb.append("`center` text, ");
		sb.append("`bbox` text, ");
		sb.append("`ccode` text, ");
		sb.append("`ar_noaanum` integer, ");
		sb.append("`sg_shape` varchar(255), ");
		sb.append("`sg_aspectratio` real, ");
		sb.append("`frm_versionnumber` real, ");
		sb.append("PRIMARY KEY (`eventID`), ");
		sb.append("KEY `startdate_sg_idx` (`startTime`) ");
		sb.append(")ENGINE=MyISAM AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;");
		return sb.toString();
	}

	private String insertSGString() {
		StringBuilder sb = new StringBuilder();
		sb.append("INSERT INTO `hekevents_sg` (");
		sb.append("`eventType`, ");
		sb.append("`startTime`, ");
		sb.append("`endTime`, ");
		sb.append("`observatory`, ");
		sb.append("`instrument`, ");
		sb.append("`channel`, ");
		sb.append("`center`, ");
		sb.append("`bbox`, ");
		sb.append("`ccode`, ");
		sb.append("`ar_noaanum`, ");
		sb.append("`sg_shape`, ");
		sb.append("`sg_aspectratio`, ");
		sb.append("`frm_versionnumber` ) ");
		sb.append("VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?);");
		return sb.toString();
	}

	////////////////// SS Events\\\\\\\\\\\\\\\\\\\\
	private void setSSParams(PreparedStatement stmt, IEvent event) throws SQLException {
		SSEvent ev = (SSEvent) event;
		stmt.setString(1, "SS");
		stmt.setTimestamp(2, new Timestamp(ev.getTimePeriod().getStartMillis()));
		stmt.setTimestamp(3, new Timestamp(ev.getTimePeriod().getEndMillis()));
		stmt.setString(4, ev.getObservatory());
		stmt.setString(5, ev.getInstrument());
		stmt.setString(6, ev.getChannel());
		stmt.setString(7, ev.getHpcCenterLocation());
		stmt.setString(8, ev.getHpcBBox());
		stmt.setString(9, ev.getHpcShape());
		this.trySetDouble(stmt, 10, ev.getArea_atdiskcenter());
		stmt.setString(11, ev.getArea_unit());
		this.trySetDouble(stmt, 12, ev.getEvent_npixels());
		this.trySetDouble(stmt, 13, ev.getFrm_versionnumber());

	}

	private String createSSString() {
		StringBuilder sb = new StringBuilder();
		sb.append("CREATE TABLE `hekevents_ss` ( ");
		sb.append("`eventID` integer unsigned NOT NULL AUTO_INCREMENT, ");
		sb.append("`eventType` varchar(255), ");
		sb.append("`startTime` timestamp DEFAULT '1970-01-01 00:00:01', ");
		sb.append("`endTime` timestamp DEFAULT '1970-01-01 00:00:01', ");
		sb.append("`observatory` varchar(255), ");
		sb.append("`instrument` varchar(255), ");
		sb.append("`channel` varchar(255), ");
		sb.append("`center` text, ");
		sb.append("`bbox` text, ");
		sb.append("`ccode` MEDIUMTEXT, ");
		sb.append("`area_atdiskcenter` real, ");
		sb.append("`area_unit` varchar(255), ");
		sb.append("`event_npixels` real, ");
		sb.append("`frm_versionnumber` real, ");
		sb.append("PRIMARY KEY (`eventID`), ");
		sb.append("KEY `startdate_ss_idx` (`startTime`) ");
		sb.append(")ENGINE=MyISAM AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;");
		return sb.toString();
	}

	private String insertSSString() {
		StringBuilder sb = new StringBuilder();
		sb.append("INSERT INTO `hekevents_ss` (");
		sb.append("`eventType`, ");
		sb.append("`startTime`, ");
		sb.append("`endTime`, ");
		sb.append("`observatory`, ");
		sb.append("`instrument`, ");
		sb.append("`channel`, ");
		sb.append("`center`, ");
		sb.append("`bbox`, ");
		sb.append("`ccode`, ");
		sb.append("`area_atdiskcenter`, ");
		sb.append("`area_unit`, ");
		sb.append("`event_npixels`, ");
		sb.append("`frm_versionnumber` ) ");
		sb.append("VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?);");
		return sb.toString();
	}
}
