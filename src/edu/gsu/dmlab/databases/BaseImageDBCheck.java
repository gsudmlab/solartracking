/**
 * SolarTracking Copyright (C) 2017 Georgia State University
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.gsu.dmlab.databases;

import java.util.Calendar;

import org.joda.time.Interval;

/**
 * A class used to consolidate several query string generation methods.
 * 
 * @author Dustin Kempton, Data Mining Lab, Georgia State University
 * @version {@value edu.gsu.dmlab.ConstValues#major_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#minor_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#build_ver}
 */
public class BaseImageDBCheck {

	/**
	 * Constructs a query string to see if the file descriptor table exists.
	 * 
	 * @param period
	 *            Used to determine which month to construct the query for.
	 * @return
	 */
	public String queryTableExistsFiles(Interval period) {

		Calendar calStart = Calendar.getInstance();
		calStart.setTimeInMillis(period.getStartMillis());

		StringBuilder sb = new StringBuilder();
		sb.append("SHOW TABLES LIKE 'files_");
		sb.append(calStart.get(Calendar.YEAR));
		if ((calStart.get(Calendar.MONTH) + 1) < 10) {
			sb.append("0");
			sb.append(calStart.get(Calendar.MONTH) + 1);
		} else {
			sb.append(calStart.get(Calendar.MONTH) + 1);
		}
		sb.append("';");
		return sb.toString();
	}

	/**
	 * Constructs a query string to see if the header table exists.
	 * 
	 * @param period
	 *            Used to determine which month to construct the query for.
	 * @return
	 */
	public String queryTableExistsHeader(Interval period) {

		Calendar calStart = Calendar.getInstance();
		calStart.setTimeInMillis(period.getStartMillis());

		StringBuilder sb = new StringBuilder();
		sb.append("SHOW TABLES LIKE 'image_header_");
		sb.append(calStart.get(Calendar.YEAR));
		if ((calStart.get(Calendar.MONTH) + 1) < 10) {
			sb.append("0");
			sb.append(calStart.get(Calendar.MONTH) + 1);
		} else {
			sb.append(calStart.get(Calendar.MONTH) + 1);
		}
		sb.append("';");
		return sb.toString();
	}

	/**
	 * Constructs a query string to see if the image table exists.
	 * 
	 * @param period
	 *            Used to determine which month to construct the query for.
	 * @return
	 */
	public String queryTableExistsImage(Interval period) {

		Calendar calStart = Calendar.getInstance();
		calStart.setTimeInMillis(period.getStartMillis());

		StringBuilder sb = new StringBuilder();
		sb.append("SHOW TABLES LIKE 'image_files_");
		sb.append(calStart.get(Calendar.YEAR));
		if ((calStart.get(Calendar.MONTH) + 1) < 10) {
			sb.append("0");
			sb.append(calStart.get(Calendar.MONTH) + 1);
		} else {
			sb.append(calStart.get(Calendar.MONTH) + 1);
		}
		sb.append("';");
		return sb.toString();
	}

	/**
	 * Constructs a query string to see if the full sized image table exists.
	 * 
	 * @param period
	 *            Used to determine which month to construct the query for.
	 * @return
	 */
	public String queryTableExistsImageFull(Interval period) {

		Calendar calStart = Calendar.getInstance();
		calStart.setTimeInMillis(period.getStartMillis());

		StringBuilder sb = new StringBuilder();
		sb.append("SHOW TABLES LIKE 'image_files_");
		sb.append(calStart.get(Calendar.YEAR));
		if ((calStart.get(Calendar.MONTH) + 1) < 10) {
			sb.append("0");
			sb.append(calStart.get(Calendar.MONTH) + 1);
		} else {
			sb.append(calStart.get(Calendar.MONTH) + 1);
		}
		sb.append("_full';");
		return sb.toString();
	}

	/**
	 * Constructs a query string to see if the image parameter table exists.
	 * 
	 * @param period
	 *            Used to determine which month to construct the query for.
	 * @return
	 */
	public String queryTableExistsParams(Interval period) {

		Calendar calStart = Calendar.getInstance();
		calStart.setTimeInMillis(period.getStartMillis());

		StringBuilder sb = new StringBuilder();
		sb.append("SHOW TABLES LIKE 'image_params_");
		sb.append(calStart.get(Calendar.YEAR));
		if ((calStart.get(Calendar.MONTH) + 1) < 10) {
			sb.append("0");
			sb.append(calStart.get(Calendar.MONTH) + 1);
		} else {
			sb.append(calStart.get(Calendar.MONTH) + 1);
		}
		sb.append("';");
		return sb.toString();
	}

}
