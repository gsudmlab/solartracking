/**
 * SolarTracking Copyright (C) 2017 Georgia State University
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.gsu.dmlab.databases;

import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Locale;
import java.util.stream.IntStream;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.plugins.jpeg.JPEGImageWriteParam;
import javax.imageio.stream.ImageOutputStream;
import javax.imageio.stream.MemoryCacheImageOutputStream;
import javax.sql.DataSource;

import org.joda.time.Interval;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import edu.gsu.dmlab.databases.interfaces.IImageDBCreator;
import edu.gsu.dmlab.datatypes.ImageDBFitsHeaderData;
import edu.gsu.dmlab.datatypes.Waveband;
import edu.gsu.dmlab.util.Utility;

/**
 * Class used to create several tables dealing with image and image parameter
 * storage.
 * 
 * @author Dustin Kempton, Data Mining Lab, Georgia State University
 * @version {@value edu.gsu.dmlab.ConstValues#major_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#minor_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#build_ver}
 */
public class ImageDBCreator implements IImageDBCreator {

	private DataSource dsourc;
	private BaseImageDBCheck dbChk;

	/**
	 * Constructor
	 * 
	 * @param dsourc
	 *            The connection object to the database that we will be creating
	 *            tables in.
	 */
	public ImageDBCreator(DataSource dsourc) {
		if (dsourc == null)
			throw new IllegalArgumentException("DataSource cannot be null in ImageDBCreator constructor.");
		this.dsourc = dsourc;
		this.dbChk = new BaseImageDBCheck();
	}

	@Override
	public boolean insertFileDescriptTables(Interval period) throws SQLException {
		Connection con = null;
		boolean done = false;
		try {
			con = this.dsourc.getConnection();
			con.setAutoCommit(true);

			// Check for file description table and create if not there.
			String query = this.dbChk.queryTableExistsFiles(period);
			PreparedStatement tableExistsPrepStmt = con.prepareStatement(query);
			ResultSet res = tableExistsPrepStmt.executeQuery();
			if (!res.next()) {
				query = this.createFilesTableString(period);
				tableExistsPrepStmt = con.prepareStatement(query);
				tableExistsPrepStmt.execute();
			}

			// Check for Image table and create if not there.
			query = this.dbChk.queryTableExistsImage(period);
			tableExistsPrepStmt = con.prepareStatement(query);
			res = tableExistsPrepStmt.executeQuery();
			if (!res.next()) {
				query = this.createImageTableString(period);
				tableExistsPrepStmt = con.prepareStatement(query);
				tableExistsPrepStmt.execute();
			}

			// Check for FullImage table and create if not there.
			query = this.dbChk.queryTableExistsImageFull(period);
			tableExistsPrepStmt = con.prepareStatement(query);
			res = tableExistsPrepStmt.executeQuery();
			if (!res.next()) {
				query = this.createFullImageTableString(period);
				tableExistsPrepStmt = con.prepareStatement(query);
				tableExistsPrepStmt.execute();
			}

			// Check for Parameter table and create if not there.
			query = this.dbChk.queryTableExistsParams(period);
			tableExistsPrepStmt = con.prepareStatement(query);
			res = tableExistsPrepStmt.executeQuery();
			if (!res.next()) {
				query = this.createParamsTableString(period);
				tableExistsPrepStmt = con.prepareStatement(query);
				tableExistsPrepStmt.execute();
			}

			// check for header table and create if not there
			query = this.dbChk.queryTableExistsHeader(period);
			tableExistsPrepStmt = con.prepareStatement(query);
			res = tableExistsPrepStmt.executeQuery();
			if (!res.next()) {
				query = this.createHeaderTableString(period);
				tableExistsPrepStmt = con.prepareStatement(query);
				tableExistsPrepStmt.execute();
			}

			done = true;

		} finally {
			if (con != null) {
				con.close();
			}
		}

		return done;
	}

	@Override
	public int insertFileDescript(Waveband wavelength, Interval period) throws SQLException {
		Connection con = null;
		int result = -1;
		try {
			con = this.dsourc.getConnection();
			con.setAutoCommit(true);
			String query = this.insertFileDescriptQuery(period);
			PreparedStatement stmt = con.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
			Timestamp startTime = new Timestamp(period.getStartMillis());
			Timestamp endTime = new Timestamp(period.getEndMillis());
			stmt.setTimestamp(1, startTime);
			stmt.setTimestamp(2, endTime);
			stmt.setInt(3, Utility.convertWavebandToInt(wavelength));
			int affectedRows = stmt.executeUpdate();

			if (affectedRows == 0)
				throw new SQLException("Insert file descriptor failed, no rows affected.");

			ResultSet res = stmt.getGeneratedKeys();
			if (res.next()) {
				result = res.getInt(1);
			}

		} finally {
			if (con != null) {
				con.close();
			}
		}

		return result;
	}

	@Override
	public void insertImage(File file, int id, Interval period) throws SQLException, FileNotFoundException {
		Connection con = null;

		try {
			con = this.dsourc.getConnection();
			con.setAutoCommit(true);
			String query = this.insertImageFileQuery(period);
			PreparedStatement stmt = con.prepareStatement(query);
			stmt.setInt(1, id);
			stmt.setBinaryStream(2, new FileInputStream(file), file.length());
			int res = stmt.executeUpdate();

			if (res == 0)
				throw new SQLException("Insert blob failed, no rows affected.");

		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	@Override
	public void insertFullImage(BufferedImage file, int id, Interval period) throws SQLException, IOException {

		// downsample the image
		BufferedImage img = new BufferedImage(2048, 2048, BufferedImage.TYPE_BYTE_GRAY);
		Graphics2D g = img.createGraphics();
		AffineTransform at = AffineTransform.getScaleInstance(.5, .5);
		g.drawRenderedImage(file, at);

		Connection con = null;
		try {
			con = this.dsourc.getConnection();
			con.setAutoCommit(true);
			String query = this.insertFullImageFileQuery(period);
			PreparedStatement stmt = con.prepareStatement(query);

			ByteArrayOutputStream os = new ByteArrayOutputStream();
			ImageOutputStream out = new MemoryCacheImageOutputStream(os);

			// get an image writer for jpg images
			ImageWriter writer = ImageIO.getImageWritersByFormatName("jpg").next();
			writer.setOutput(out);

			// set the parameter for compression quality
			ImageWriteParam iwparam = new JPEGImageWriteParam(Locale.getDefault());
			iwparam.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
			iwparam.setCompressionQuality((float) 0.90);

			// write the image to the output stream
			writer.write(null, new IIOImage(img, null, null), iwparam);

			// get the input stream for inserting into the database.
			InputStream is = new ByteArrayInputStream(os.toByteArray());

			// set the query parameters and execute the image insert.
			stmt.setInt(1, id);
			stmt.setBinaryStream(2, is, os.size());
			int res = stmt.executeUpdate();

			if (res == 0)
				throw new SQLException("Insert blob failed, no rows affected.");

		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	@Override
	public void insertParams(Document file, int id, Interval period) throws SQLException {
		float[][][] params = this.readParam(file);

		Connection con = null;

		try {
			con = this.dsourc.getConnection();
			con.setAutoCommit(true);
			String query = this.insertImageParamQuery(period);
			PreparedStatement stmt = con.prepareStatement(query);
			for (int x = 0; x < params.length; x++) {
				for (int y = 0; y < params[x].length; y++) {
					stmt.setInt(1, id);
					stmt.setInt(2, x + 1);
					stmt.setInt(3, y + 1);
					for (int i = 0; i < params[x][y].length; i++) {
						stmt.setFloat(4 + i, params[x][y][i]);
					}
					stmt.addBatch();
				}
			}

			int[] res = stmt.executeBatch();
			for (int i = 0; i < res.length; i++)
				if (res[i] == 0)
					throw new SQLException("Insert params failed, no rows affected.");

		} finally {
			if (con != null) {
				con.close();
			}
		}

	}

	@Override
	public void insertEmptyParams(int id, Interval period) throws SQLException {
		float[][][] params = new float[64][64][10];

		Connection con = null;

		try {
			con = this.dsourc.getConnection();
			con.setAutoCommit(true);
			String query = this.insertImageParamQuery(period);
			PreparedStatement stmt = con.prepareStatement(query);
			for (int x = 0; x < params.length; x++) {
				for (int y = 0; y < params[x].length; y++) {
					stmt.setInt(1, id);
					stmt.setInt(2, x + 1);
					stmt.setInt(3, y + 1);
					for (int i = 0; i < params[x][y].length; i++) {
						stmt.setFloat(4 + i, params[x][y][i]);
					}
					stmt.addBatch();
				}
			}

			int[] res = stmt.executeBatch();
			for (int i = 0; i < res.length; i++)
				if (res[i] == 0)
					throw new SQLException("Insert params failed, no rows affected.");

		} finally {
			if (con != null) {
				con.close();
			}
		}

	}

	@Override
	public void insertHeader(ImageDBFitsHeaderData header, int id, Interval period) throws SQLException {

		Connection con = null;

		try {
			con = this.dsourc.getConnection();
			con.setAutoCommit(true);
			String query = this.insertHeaderQuery(period);
			PreparedStatement stmt = con.prepareStatement(query);

			stmt.setInt(1, id);
			stmt.setDouble(2, header.X0);
			stmt.setDouble(3, header.Y0);
			stmt.setDouble(4, header.R_SUN);
			stmt.setDouble(5, header.DSUN);
			stmt.setDouble(6, header.CDELT);

			int res = stmt.executeUpdate();

			if (res == 0)
				throw new SQLException("Insert header failed, no rows affected.");

		} finally {
			if (con != null) {
				con.close();
			}
		}

	}

	private float[][][] readParam(Document paramFile) {
		float[][][] results = new float[64][64][];

		Element root = paramFile.getDocumentElement();
		NodeList ndLst = root.getChildNodes();

		IntStream.range(0, ndLst.getLength()).parallel().forEach(i -> {

			Node nde = ndLst.item(i);
			if (nde.getNodeType() == Node.ELEMENT_NODE) {
				String ndName = nde.getNodeName();
				if (ndName.equalsIgnoreCase("cell")) {
					NodeList cellNdLst = nde.getChildNodes();
					int x = 0;
					int y = 0;
					float[] vals = null;
					for (int j = 0; j < cellNdLst.getLength(); j++) {
						Node cellNde = cellNdLst.item(j);
						if (cellNde.getNodeType() == Node.ELEMENT_NODE) {
							String cellNdeName = cellNde.getNodeName();
							switch (cellNdeName) {
							case "x":
								x = Integer.parseInt(cellNde.getTextContent());
								break;
							case "y":
								y = Integer.parseInt(cellNde.getTextContent());
								break;
							case "params":
								vals = this.parseParams(cellNde);
								break;
							}
						}
					}
					results[x][y] = vals;
				}
			}

		});

		return results;
	}

	private float[] parseParams(Node nde) {
		NodeList cellNdLst = nde.getChildNodes();
		float[] vals = new float[10];
		int count = 0;
		for (int j = 0; j < cellNdLst.getLength(); j++) {
			Node cellNde = cellNdLst.item(j);
			if (cellNde.getNodeType() == Node.ELEMENT_NODE) {
				String cellNdeName = cellNde.getNodeName();
				if (cellNdeName.equalsIgnoreCase("param")) {
					vals[count++] = Float.parseFloat(cellNde.getTextContent());
				}
			}
		}
		return vals;
	}

	private String insertFileDescriptQuery(Interval period) {
		Calendar calStart = Calendar.getInstance();
		calStart.setTimeInMillis(period.getStartMillis());

		StringBuilder sb = new StringBuilder();
		sb.append("INSERT INTO files_");
		sb.append(calStart.get(Calendar.YEAR));
		if ((calStart.get(Calendar.MONTH) + 1) < 10) {
			sb.append("0");
			sb.append(calStart.get(Calendar.MONTH) + 1);
		} else {
			sb.append(calStart.get(Calendar.MONTH) + 1);
		}
		sb.append(" (startdate, enddate, wavelength) VALUES(?,?,?);");
		return sb.toString();
	}

	private String insertImageFileQuery(Interval period) {
		Calendar calStart = Calendar.getInstance();
		calStart.setTimeInMillis(period.getStartMillis());

		StringBuilder sb = new StringBuilder();
		sb.append("INSERT INTO image_files_");
		sb.append(calStart.get(Calendar.YEAR));
		if ((calStart.get(Calendar.MONTH) + 1) < 10) {
			sb.append("0");
			sb.append(calStart.get(Calendar.MONTH) + 1);
		} else {
			sb.append(calStart.get(Calendar.MONTH) + 1);
		}
		sb.append(" VALUES( ?, ? );");
		return sb.toString();
	}

	private String insertFullImageFileQuery(Interval period) {
		Calendar calStart = Calendar.getInstance();
		calStart.setTimeInMillis(period.getStartMillis());

		StringBuilder sb = new StringBuilder();
		sb.append("INSERT INTO image_files_");
		sb.append(calStart.get(Calendar.YEAR));
		if ((calStart.get(Calendar.MONTH) + 1) < 10) {
			sb.append("0");
			sb.append(calStart.get(Calendar.MONTH) + 1);
		} else {
			sb.append(calStart.get(Calendar.MONTH) + 1);
		}
		sb.append("_full VALUES( ?, ? );");
		return sb.toString();
	}

	private String insertImageParamQuery(Interval period) {
		Calendar calStart = Calendar.getInstance();
		calStart.setTimeInMillis(period.getStartMillis());

		StringBuilder sb = new StringBuilder();
		sb.append("INSERT INTO image_params_");
		sb.append(calStart.get(Calendar.YEAR));
		if ((calStart.get(Calendar.MONTH) + 1) < 10) {
			sb.append("0");
			sb.append(calStart.get(Calendar.MONTH) + 1);
		} else {
			sb.append(calStart.get(Calendar.MONTH) + 1);
		}
		sb.append(" VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?);");
		return sb.toString();
	}

	private String insertHeaderQuery(Interval period) {
		Calendar calStart = Calendar.getInstance();
		calStart.setTimeInMillis(period.getStartMillis());

		StringBuilder sb = new StringBuilder();
		sb.append("INSERT INTO image_header_");
		sb.append(calStart.get(Calendar.YEAR));
		if ((calStart.get(Calendar.MONTH) + 1) < 10) {
			sb.append("0");
			sb.append(calStart.get(Calendar.MONTH) + 1);
		} else {
			sb.append(calStart.get(Calendar.MONTH) + 1);
		}
		sb.append(" VALUES(?,?,?,?,?,?);");
		return sb.toString();
	}

	private String createFilesTableString(Interval period) {
		Calendar calStart = Calendar.getInstance();
		calStart.setTimeInMillis(period.getStartMillis());

		String name = "" + calStart.get(Calendar.YEAR);
		if ((calStart.get(Calendar.MONTH) + 1) < 10) {
			name += "0";
			name += (calStart.get(Calendar.MONTH) + 1);
		} else {
			name += (calStart.get(Calendar.MONTH) + 1);
		}

		StringBuilder sb = new StringBuilder();
		sb.append("CREATE TABLE `files_");
		sb.append(name);
		sb.append("` (");
		sb.append("`id` int(10) unsigned NOT NULL AUTO_INCREMENT, ");
		sb.append("`startdate` datetime NOT NULL, ");
		sb.append("`enddate` datetime NOT NULL, ");
		sb.append("`wavelength` smallint(5) unsigned NOT NULL, ");
		sb.append("PRIMARY KEY (`id`), ");
		sb.append("KEY `startdate_wave_");
		sb.append(name);
		sb.append("_idx` (`startdate`,`wavelength`), ");
		sb.append("KEY `enddate_wave_");
		sb.append(name);
		sb.append("_idx` (`enddate`,`wavelength`), ");
		sb.append("KEY `idx_files_startdate_enddate_wavelenght_");
		sb.append(name);
		sb.append("` (`startdate`,`enddate`,`wavelength`) ");
		sb.append(") ENGINE=MyISAM AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;");
		return sb.toString();
	}

	private String createHeaderTableString(Interval period) {
		Calendar calStart = Calendar.getInstance();
		calStart.setTimeInMillis(period.getStartMillis());

		String name = "" + calStart.get(Calendar.YEAR);
		if ((calStart.get(Calendar.MONTH) + 1) < 10) {
			name += "0";
			name += (calStart.get(Calendar.MONTH) + 1);
		} else {
			name += (calStart.get(Calendar.MONTH) + 1);
		}
		StringBuilder sb = new StringBuilder();
		sb.append("CREATE TABLE `image_header_");
		sb.append(name);
		sb.append("` (");
		sb.append("`file_id` int(10) unsigned NOT NULL, ");
		sb.append("`x0` real(14,8) NOT NULL, ");
		sb.append("`y0` real(14,8) NOT NULL, ");
		sb.append("`rSun` real(14,8) NOT NULL, ");
		sb.append("`dSun` real(14,1) NOT NULL, ");
		sb.append("`cdelt` real(14,8) NOT NULL, ");
		sb.append("PRIMARY KEY (`file_id`), ");
		sb.append("CONSTRAINT `header_id_fk_");
		sb.append(name);
		sb.append("` FOREIGN KEY (`file_id`) REFERENCES `files_");
		sb.append(name);
		sb.append("` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION ");
		sb.append(") ENGINE=MyISAM DEFAULT CHARSET=utf8;");
		return sb.toString();
	}

	private String createParamsTableString(Interval period) {
		Calendar calStart = Calendar.getInstance();
		calStart.setTimeInMillis(period.getStartMillis());

		String name = "" + calStart.get(Calendar.YEAR);
		if ((calStart.get(Calendar.MONTH) + 1) < 10) {
			name += "0";
			name += (calStart.get(Calendar.MONTH) + 1);
		} else {
			name += (calStart.get(Calendar.MONTH) + 1);
		}

		StringBuilder sb = new StringBuilder();
		sb.append("CREATE TABLE `image_params_");
		sb.append(name);
		sb.append("` (");
		sb.append("`file_id` int(10) unsigned NOT NULL, ");
		sb.append("`x` tinyint(3) unsigned NOT NULL, ");
		sb.append("`y` tinyint(3) unsigned NOT NULL, ");
		sb.append("`p1` float(7,4) NOT NULL, ");
		sb.append("`p2` float(7,4) NOT NULL, ");
		sb.append("`p3` float(7,4) NOT NULL, ");
		sb.append("`p4` float(7,4) NOT NULL, ");
		sb.append("`p5` float(7,4) NOT NULL, ");
		sb.append("`p6` float(7,4) NOT NULL, ");
		sb.append("`p7` float(7,4) NOT NULL, ");
		sb.append("`p8` float(7,4) NOT NULL, ");
		sb.append("`p9` float(7,4) NOT NULL, ");
		sb.append("`p10` float(7,4) NOT NULL, ");
		sb.append("PRIMARY KEY (`file_id`,`x`,`y`), ");
		sb.append("KEY `image_param_id_idx");
		sb.append(name);
		sb.append("` (`file_id`), ");
		sb.append("CONSTRAINT `image_params_id_fk_");
		sb.append(name);
		sb.append("` FOREIGN KEY (`file_id`) REFERENCES `files_");
		sb.append(name);
		sb.append("` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION ");
		sb.append(") ENGINE=MyISAM DEFAULT CHARSET=utf8;");
		return sb.toString();
	}

	private String createImageTableString(Interval period) {
		Calendar calStart = Calendar.getInstance();
		calStart.setTimeInMillis(period.getStartMillis());

		String name = "" + calStart.get(Calendar.YEAR);
		if ((calStart.get(Calendar.MONTH) + 1) < 10) {
			name += "0";
			name += (calStart.get(Calendar.MONTH) + 1);
		} else {
			name += (calStart.get(Calendar.MONTH) + 1);
		}

		StringBuilder sb = new StringBuilder();
		sb.append("CREATE TABLE `image_files_");
		sb.append(name);
		sb.append("` ( ");
		sb.append("`file_id` int(10) unsigned NOT NULL, ");
		sb.append("`image_file` blob NOT NULL, ");
		sb.append("PRIMARY KEY (`file_id`), ");
		sb.append("CONSTRAINT `files_id_fk_");
		sb.append(name);
		sb.append("` FOREIGN KEY (`file_id`) REFERENCES `files_");
		sb.append(name);
		sb.append("` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION ");
		sb.append(") ENGINE=MyISAM DEFAULT CHARSET=utf8;");
		return sb.toString();
	}

	private String createFullImageTableString(Interval period) {
		Calendar calStart = Calendar.getInstance();
		calStart.setTimeInMillis(period.getStartMillis());

		String name = "" + calStart.get(Calendar.YEAR);
		if ((calStart.get(Calendar.MONTH) + 1) < 10) {
			name += "0";
			name += (calStart.get(Calendar.MONTH) + 1);
		} else {
			name += (calStart.get(Calendar.MONTH) + 1);
		}

		StringBuilder sb = new StringBuilder();
		sb.append("CREATE TABLE `image_files_");
		sb.append(name);
		sb.append("_full` ( ");
		sb.append("`file_id` int(10) unsigned NOT NULL, ");
		sb.append("`image_file` mediumblob NOT NULL, ");
		sb.append("PRIMARY KEY (`file_id`), ");
		sb.append("CONSTRAINT `files_id_fk_");
		sb.append(name);
		sb.append("_full` FOREIGN KEY (`file_id`) REFERENCES `files_");
		sb.append(name);
		sb.append("` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION ");
		sb.append(") ENGINE=MyISAM DEFAULT CHARSET=utf8;");
		return sb.toString();
	}

}
