/**
 * SolarTracking Copyright (C) 2017 Georgia State University
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.gsu.dmlab.databases.interfaces;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;

import org.joda.time.Interval;
import org.w3c.dom.Document;

import edu.gsu.dmlab.datatypes.ImageDBFitsHeaderData;
import edu.gsu.dmlab.datatypes.Waveband;

/**
 * @author Dustin Kempton, Data Mining Lab, Georgia State University
 * @version {@value edu.gsu.dmlab.ConstValues#major_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#minor_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#build_ver}
 */
public interface IImageDBCreator {

	/**
	 * Creates the tables for holding images and image parameter data for the
	 * month represented by the start of the passed in interval.
	 * 
	 * @param period
	 *            The period that the table corresponds with
	 * @return True if table was created, else False.
	 * @throws SQLException
	 *             If something went wrong with the connection.
	 */
	public boolean insertFileDescriptTables(Interval period) throws SQLException;

	/**
	 * Inserts the descriptor for a particular image.
	 * 
	 * @param wavelength
	 *            The wavelength of the image to be stored
	 * @param period
	 *            The period of time that the image corresponds to.
	 * @return The id for the image description
	 * @throws SQLException
	 *             If the operation fails.
	 */
	public int insertFileDescript(Waveband wavelength, Interval period) throws SQLException;

	/**
	 * Inserts the image for a given descriptor.
	 * 
	 * @param file
	 *            The file containing the image.
	 * @param id
	 *            The id of the image descriptor.
	 * @param period
	 *            The period that the image corresponds to. This is used to
	 *            determine which table to insert into, so it only needs to be
	 *            in the same month.
	 * @throws SQLException
	 *             If the operation files.
	 * @throws FileNotFoundException
	 *             If the file doesn't exist.
	 */
	public void insertImage(File file, int id, Interval period) throws SQLException, FileNotFoundException;

	/**
	 * Inserts the image for a given descriptor.
	 * 
	 * @param file
	 *            The file containing the image.
	 * @param id
	 *            The id of the image descriptor.
	 * @param period
	 *            The period that the image corresponds to. This is used to
	 *            determine which table to insert into, so it only needs to be
	 *            in the same month.
	 * @throws SQLException
	 *             If the operation files.
	 * @throws IOException
	 */
	public void insertFullImage(BufferedImage file, int id, Interval period) throws SQLException, IOException;

	/**
	 * Inserts the image parameters for the image matching the description id.
	 * 
	 * @param file
	 *            The file containing the image parameters.
	 * @param id
	 *            The id of the image descriptor.
	 * @param period
	 *            The period that the image corresponds to. This is used to
	 *            determine which table to insert into, so it only needs to be
	 *            in the same month.
	 * @throws SQLException
	 *             If the operation fails.
	 */
	public void insertParams(Document file, int id, Interval period) throws SQLException;

	/**
	 * Inserts the empty image parameters because the dataset does not have them
	 * for the image matching the description id.
	 * 
	 * @param file
	 *            The file containing the image parameters.
	 * @param id
	 *            The id of the image descriptor.
	 * @param period
	 *            The period that the image corresponds to. This is used to
	 *            determine which table to insert into, so it only needs to be
	 *            in the same month.
	 * @throws SQLException
	 *             If the operation fails.
	 */
	public void insertEmptyParams(int id, Interval period) throws SQLException;

	/**
	 * Inserts the fits header information for the image matching the
	 * description id.
	 * 
	 * @param header
	 *            The fits header data to insert.
	 * @param id
	 *            The id of the image descriptor.
	 * @param period
	 *            The period that the image corresponds to. This is used to
	 *            determine which table to insert into, so it only needs to be
	 *            in the same month.
	 * @throws SQLException
	 *             If the operation fails.
	 */
	public void insertHeader(ImageDBFitsHeaderData header, int id, Interval period) throws SQLException;
}
