/**
 * SolarTracking Copyright (C) 2017 Georgia State University
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.gsu.dmlab.databases.interfaces;

import java.sql.SQLException;

import edu.gsu.dmlab.datatypes.EventType;
import edu.gsu.dmlab.datatypes.ImageDBWaveParamPair;

/**
 * Connection to the database of feature feature values representing the fitness
 * of a parameter for representing an event type.
 * 
 * @author Dustin Kempton, Data Mining Lab, Georgia State University
 * @version {@value edu.gsu.dmlab.ConstValues#major_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#minor_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#build_ver}
 */
public interface IFeatureDBConnection {

	/**
	 * Selects the top features in the database and returns the array of their
	 * descriptors
	 * 
	 * @param type
	 *            The event type to select features for
	 * @param num
	 *            The number of features to return
	 * @return The array of the top features in the database
	 * @throws SQLException
	 *             If an error occurs
	 */
	public ImageDBWaveParamPair[] getBestFeatures(EventType type, int num) throws SQLException;

	/**
	 * Checks to see if the feature table for the particular event type exists
	 * and has the data set.
	 * 
	 * @param type
	 *            The event type to check for.
	 * @return True if exists, else false.
	 * @throws SQLException
	 *             If an error occurs
	 */
	public boolean featureTableSet(EventType type) throws SQLException;
}
