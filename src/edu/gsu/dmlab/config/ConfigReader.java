/**
 * SolarTracking Copyright (C) 2017 Georgia State University
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.gsu.dmlab.config;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import javax.sql.DataSource;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.dbcp2.BasicDataSource;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import edu.gsu.dmlab.datatypes.EventType;
import edu.gsu.dmlab.datatypes.ImageDBWaveParamPair;
import edu.gsu.dmlab.datatypes.Waveband;
import edu.gsu.dmlab.exceptions.InvalidConfigException;
import edu.gsu.dmlab.util.Utility;

/**
 * Class used to read the configuration file for this project.
 * 
 * @author Dustin Kempton, Data Mining Lab, Georgia State University
 * @version {@value edu.gsu.dmlab.ConstValues#major_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#minor_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#build_ver}
 */
public class ConfigReader {

	private BasicDataSource trackingDBPoolSourc;
	private int expId = 1;
	private int imageCacheSize;
	private int regionDim;
	private int regionDiv;
	private int numFeatures = 20;
	private int stepSize = 1;
	private int patchSize = 4;
	private ArrayList<Waveband> wavelengths;
	private ArrayList<Integer> params;
	private String startDatetime = "2012-01-01 00:00:00";
	private String endDatetime = "2014-12-31 23:59:59";
	private String dataLocation;
	private HashMap<Integer, double[]> histoBoundaryMap;
	private HashMap<EventType, EventConfig> map;

	private double entExitMult = 5.0;
	private double obsMult = 65.0;
	private double assocMult = 85.0;
	private double appearWeight = 0.5;
	private double skipWeight = 0.8;
	private double motionWeight = 0.1;

	private double dictErrorLim = 10e-2;
	private double dictFracktion = 0.15;
	private double lambda = 1.55;

	/**
	 * Constructor that takes in the name and location of the config file.
	 * 
	 * @param configFile
	 *            The name and location of the config file
	 * @throws InvalidConfigException
	 */
	public ConfigReader(String configFile) throws InvalidConfigException {
		this.map = new HashMap<EventType, EventConfig>();
		this.histoBoundaryMap = new HashMap<Integer, double[]>();

		this.wavelengths = new ArrayList<Waveband>();
		this.wavelengths.add(Utility.getWavebandFromInt(94));
		this.wavelengths.add(Utility.getWavebandFromInt(131));
		this.wavelengths.add(Utility.getWavebandFromInt(171));
		this.wavelengths.add(Utility.getWavebandFromInt(193));
		this.wavelengths.add(Utility.getWavebandFromInt(211));
		this.wavelengths.add(Utility.getWavebandFromInt(304));
		this.wavelengths.add(Utility.getWavebandFromInt(335));
		this.wavelengths.add(Utility.getWavebandFromInt(1600));
		this.wavelengths.add(Utility.getWavebandFromInt(1700));

		this.params = new ArrayList<Integer>();
		this.params.add(1);
		this.params.add(2);
		this.params.add(3);
		this.params.add(4);
		this.params.add(5);
		this.params.add(6);
		this.params.add(7);
		this.params.add(8);
		this.params.add(9);
		this.params.add(10);

		this.config(configFile);
	}

	/**
	 * Gets the datasource object that is the connection to the database.
	 * 
	 * @return The datasource object used to connect to a database.
	 */
	public DataSource getTrackDBSource() {
		return this.trackingDBPoolSourc;
	}

	/**
	 * Gets the number of objects we should store image parameter information
	 * for.
	 * 
	 * @return The size of the cache of object image parameters.
	 */
	public int getCacheSize() {
		return this.imageCacheSize;
	}

	/**
	 * The region dimension for indexing. Range of 1 to 4096, where 1
	 * corresponds to everything being indexed by time and 4096 means there is
	 * no down sizing of objects prior to indexing.
	 * 
	 * @return Region Dimension
	 */
	public int getRegionDim() {
		return this.regionDim;
	}

	/**
	 * The divisor used to get objects small enough to fit in the index range.
	 * The objects are originally in a 4096 by 4096 coordinate system. So, if
	 * the region dimension is set to 4096 then this is 1, if region dimension
	 * is 128 this is 32.
	 * 
	 * @return The region divisor
	 */
	public int getRegionDiv() {
		return this.regionDiv;
	}

	/**
	 * Gets the Map of image parameter numbers to their min/max values seen in
	 * the dataset.
	 * 
	 * @return Map of image parameter min/max values.
	 */
	public HashMap<Integer, double[]> getHistoMap() {
		return this.histoBoundaryMap;
	}

	/**
	 * Gets the event configuration for a specific event type.
	 * 
	 * @param key
	 *            The event type to get the configuration information for.
	 * @return
	 */
	public EventConfig getEventConfig(EventType key) {
		return this.map.get(key);
	}

	/**
	 * The lambda value used in the lasso methods.
	 * 
	 * @return
	 */
	public double getLambda() {
		return this.lambda;
	}

	/**
	 * Gets the dictionary error that is the cutoff for stopping the dictionary
	 * learing process.
	 * 
	 * @return
	 */
	public double getDictErrorLim() {
		return this.dictErrorLim;
	}

	/**
	 * Gets the fraction of the input size that we want the dictionary to be.
	 * 
	 * @return
	 */
	public double getDictFraction() {
		return this.dictFracktion;
	}

	/**
	 * The step size when extracting patches inside the ROI of an object as
	 * vectors from the image parameter set.
	 * 
	 * @return
	 */
	public int getVectorizorStepSize() {
		return this.stepSize;
	}

	/**
	 * The patch size when extracting patches inside the ROI of an object.
	 * 
	 * @return
	 */
	public int getVecorizorPatchSize() {
		return this.patchSize;
	}

	/**
	 * The start date time string of the data we are going to process. In
	 * yyyy-MM-dd hh:mm:ss format.
	 * 
	 * @return
	 */
	public String getStartDatetime() {
		return this.startDatetime;
	}

	/**
	 * The end date time string of the data we are going to process. In
	 * yyyy-MM-dd hh:mm:ss format.
	 * 
	 * @return
	 */
	public String getEndDatetime() {
		return this.endDatetime;
	}

	/**
	 * The list of wavelengths that we will be using.
	 * 
	 * @return
	 */
	public ArrayList<Waveband> getWavelengths() {
		return this.wavelengths;
	}

	/**
	 * The list of image parameter numbers we will be using.
	 * 
	 * @return
	 */
	public ArrayList<Integer> getParams() {
		return this.params;
	}

	/**
	 * The directory that contains the input dataset of detected events.
	 * 
	 * @return
	 */
	public String getDataLocation() {
		return this.dataLocation;
	}

	/**
	 * An indicator used to number different runs of the tracking algorithm as
	 * different result tables in the database.
	 * 
	 * @return
	 */
	public int getExpId() {
		return this.expId;
	}

	/**
	 * The number of features to use when doing visual comparison of objects.
	 * Value can be 1 to Wavelengths * Params.
	 * 
	 * @return
	 */
	public int getNumFeatures() {
		return this.numFeatures;
	}

	/**
	 * A multiplier constant for enter and exit edge weights in the track graph
	 * portion of the algorithm.
	 * 
	 * @return
	 */
	public double getEntExitMult() {
		return this.entExitMult;
	}

	/**
	 * A multiplier constant for the observation edge weights in the track graph
	 * portion of the algorithm.
	 * 
	 * @return
	 */
	public double getObsMult() {
		return this.obsMult;
	}

	/**
	 * A multiplier constant for the association edge weights in the track graph
	 * portion of the algorithm.
	 * 
	 * @return
	 */
	public double getAssocMult() {
		return this.assocMult;
	}

	/**
	 * A weight multiplier for how much we want the appearance to affect the
	 * association edge.
	 * 
	 * @return
	 */
	public double getAppearWeight() {
		return this.appearWeight;
	}

	/**
	 * A weight multiplier for how much we want the skip model to affect the
	 * association edge.
	 * 
	 * @return
	 */
	public double getSkipWeight() {
		return this.skipWeight;
	}

	/**
	 * A weight multiplier for how much we want the motion model to affect the
	 * association edge.
	 * 
	 * @return
	 */
	public double getMotionWeight() {
		return this.motionWeight;
	}

	// /////////////////////////////////////////////////////////////////////////////////
	// Start of private methods
	// ////////////////////////////////////////////////////////////////////////////////
	private void config(String fileLoc) throws InvalidConfigException {
		try {

			DocumentBuilderFactory fctry = DocumentBuilderFactory.newInstance();
			Document doc;
			DocumentBuilder bldr = fctry.newDocumentBuilder();
			doc = bldr.parse(new File(fileLoc));
			doc.getDocumentElement().normalize();

			Element root = doc.getDocumentElement();
			NodeList ndLst = root.getChildNodes();
			for (int i = 0; i < ndLst.getLength(); i++) {
				Node nde = ndLst.item(i);
				if (nde.getNodeType() == Node.ELEMENT_NODE) {
					String ndName = nde.getNodeName();
					switch (ndName) {
					case "trackingpool":
						this.trackingDBPoolSourc = this.getPoolSourc(nde.getChildNodes());
						break;
					case "imagedbcache":
						this.imageCacheSize = Integer.parseInt(this.getAttrib(nde, "max"));
						break;
					case "datalocation":
						this.dataLocation = this.getAttrib(nde, "value");
						if (this.dataLocation.startsWith("~")) {
							this.dataLocation = System.getProperty("user.home") + this.dataLocation.substring(1);
						}
						break;
					case "expid":
						this.expId = Integer.parseInt(this.getAttrib(nde, "value"));
						break;
					case "numfeatures":
						this.numFeatures = Integer.parseInt(this.getAttrib(nde, "value"));
						break;
					case "entexitmult":
						this.entExitMult = Double.parseDouble(this.getAttrib(nde, "value"));
						break;
					case "obsmult":
						this.obsMult = Double.parseDouble(this.getAttrib(nde, "value"));
						break;
					case "assocmult":
						this.assocMult = Double.parseDouble(this.getAttrib(nde, "value"));
						break;
					case "appearweight":
						this.appearWeight = Double.parseDouble(this.getAttrib(nde, "value"));
						break;
					case "skipweight":
						this.skipWeight = Double.parseDouble(this.getAttrib(nde, "value"));
						break;
					case "motionweight":
						this.motionWeight = Double.parseDouble(this.getAttrib(nde, "value"));
						break;
					case "startdatetime":
						this.startDatetime = this.getAttrib(nde, "value");
						break;
					case "enddatetime":
						this.endDatetime = this.getAttrib(nde, "value");
					case "indexing":
						this.setIndexingConf(nde.getChildNodes());
						break;
					case "eventtypes":
						this.setEventTypConf(nde.getChildNodes());
						break;
					case "histograms":
						this.setHistoBoundaries(nde.getChildNodes());
						break;
					}

				}
			}

		} catch (Exception e) {
			throw new InvalidConfigException("Config failed with: " + e.getMessage());
		}

	}

	private void setEventTypConf(NodeList ndList) {
		for (int i = 0; i < ndList.getLength(); i++) {
			Node nde = ndList.item(i);
			if (nde.getNodeType() == Node.ELEMENT_NODE) {
				String ndName = nde.getNodeName();
				switch (ndName) {
				case "activeregion":
					this.map.put(EventType.ACTIVE_REGION, this.getEventConfig(nde.getChildNodes()));
					break;
				case "coronalhole":
					this.map.put(EventType.CORONAL_HOLE, this.getEventConfig(nde.getChildNodes()));
					break;
				case "emergingflux":
					this.map.put(EventType.EMERGING_FLUX, this.getEventConfig(nde.getChildNodes()));
					break;
				case "filiment":
					this.map.put(EventType.FILAMENT, this.getEventConfig(nde.getChildNodes()));
					break;
				case "sigmoid":
					this.map.put(EventType.SIGMOID, this.getEventConfig(nde.getChildNodes()));
					break;
				case "sunspot":
					this.map.put(EventType.SUNSPOT, this.getEventConfig(nde.getChildNodes()));
					break;
				case "flare":
					this.map.put(EventType.FLARE, this.getEventConfig(nde.getChildNodes()));
				}
			}
		}
	}

	private EventConfig getEventConfig(NodeList ndList) {

		int timeSpan = 0;

		for (int i = 0; i < ndList.getLength(); i++) {
			Node nde = ndList.item(i);
			if (nde.getNodeType() == Node.ELEMENT_NODE) {
				String ndName = nde.getNodeName();
				switch (ndName) {
				case "timespan":
					timeSpan = Integer.parseInt(this.getAttrib(nde, "value"));
					break;
				}
			}
		}

		return new EventConfig(timeSpan);
	}

	public ImageDBWaveParamPair getParam(NodeList ndList) {
		int wavelength = 0;
		int param = 0;

		for (int i = 0; i < ndList.getLength(); i++) {
			Node nde = ndList.item(i);
			if (nde.getNodeType() == Node.ELEMENT_NODE) {
				String ndName = nde.getNodeName();
				switch (ndName) {
				case "wavelength":
					wavelength = Integer.parseInt(this.getAttrib(nde, "value"));
					break;
				case "id":
					param = Integer.parseInt(this.getAttrib(nde, "value"));
					break;
				default:
					System.out.println("ndeName");
				}
			}
		}
		ImageDBWaveParamPair paramPair = new ImageDBWaveParamPair();
		paramPair.wavelength = Utility.getWavebandFromInt(wavelength);
		paramPair.parameter = param;
		return paramPair;
	}

	public double[] getMeanDev(NodeList ndList) {
		double[] vals = new double[2];
		for (int i = 0; i < ndList.getLength(); i++) {
			Node nde = ndList.item(i);
			if (nde.getNodeType() == Node.ELEMENT_NODE) {
				String ndName = nde.getNodeName();
				switch (ndName) {
				case "mean":
					vals[0] = Double.parseDouble(this.getAttrib(nde, "value"));
					break;
				case "standardDev":
					vals[1] = Double.parseDouble(this.getAttrib(nde, "value"));
					break;
				}
			}
		}
		return vals;
	}

	private void setIndexingConf(NodeList ndList) {
		for (int i = 0; i < ndList.getLength(); i++) {
			Node nde = ndList.item(i);
			if (nde.getNodeType() == Node.ELEMENT_NODE) {
				String ndName = nde.getNodeName();
				switch (ndName) {
				case "regiondim":
					this.regionDim = Integer.parseInt(this.getAttrib(nde, "value"));
					break;
				case "regiondiv":
					this.regionDiv = Integer.parseInt(this.getAttrib(nde, "value"));
					break;
				}
			}
		}
	}

	private BasicDataSource getPoolSourc(NodeList ndLst) {
		BasicDataSource dbPoolSourc = null;
		dbPoolSourc = new BasicDataSource();
		dbPoolSourc.setPoolPreparedStatements(false);
		dbPoolSourc.setDefaultAutoCommit(true);

		for (int i = 0; i < ndLst.getLength(); i++) {
			Node nde = ndLst.item(i);
			if (nde.getNodeType() == Node.ELEMENT_NODE) {
				String ndName = nde.getNodeName();
				switch (ndName) {

				case "timeout":
					String idlStr = this.getAttrib(nde, "value");
					dbPoolSourc.setDefaultQueryTimeout(Integer.parseInt(idlStr));
					dbPoolSourc.setSoftMinEvictableIdleTimeMillis(Integer.parseInt(idlStr));
					break;
				case "minpool":
					String minStr = this.getAttrib(nde, "value");
					dbPoolSourc.setMinIdle(Integer.parseInt(minStr));
					break;
				case "maxpool":
					String maxStr = this.getAttrib(nde, "value");
					dbPoolSourc.setMaxIdle(Integer.parseInt(maxStr));
					break;
				case "maxsize":
					String maxszStr = this.getAttrib(nde, "value");
					dbPoolSourc.setMaxTotal(Integer.parseInt(maxszStr));
					break;
				case "username":
					dbPoolSourc.setUsername(this.getAttrib(nde, "value"));
					break;
				case "password":
					dbPoolSourc.setPassword(this.getAttrib(nde, "value"));
					break;
				case "validationquery":
					dbPoolSourc.setValidationQuery(this.getAttrib(nde, "value"));
					break;
				case "driverclass":
					dbPoolSourc.setDriverClassName(this.getAttrib(nde, "value"));
					break;
				case "url":
					dbPoolSourc.setUrl(this.getAttrib(nde, "value"));
					break;
				default:
					System.out.print("Unknown Element: ");
					System.out.println(ndName);
				}
			}
		}
		return dbPoolSourc;

	}

	private String getAttrib(Node prntNde, String attName) {
		StringBuffer buf = new StringBuffer("");
		boolean isSet = false;
		if (prntNde.hasAttributes()) {
			NamedNodeMap ndeMp = prntNde.getAttributes();
			for (int i = 0; i < ndeMp.getLength(); i++) {
				Node nde = ndeMp.item(i);
				if (nde.getNodeName().compareTo(attName) == 0) {
					buf.append(nde.getNodeValue());
					isSet = true;
					break;
				}
			}
		}

		if (!isSet) {
			return "";
		} else {
			return buf.toString();
		}
	}

	private void setHistoBoundaries(NodeList ndList) {
		for (int i = 0; i < ndList.getLength(); i++) {
			Node nde = ndList.item(i);
			if (nde.getNodeType() == Node.ELEMENT_NODE) {
				String ndName = nde.getNodeName();
				switch (ndName) {
				case "parameter":
					int paramNumber = Integer.parseInt(this.getAttrib(nde, "value"));
					double[] paramLimits = this.getUpperLowerLimit(nde.getChildNodes());
					this.histoBoundaryMap.put(paramNumber, paramLimits);
					break;
				}
			}
		}
	}

	private double[] getUpperLowerLimit(NodeList ndList) {
		double[] vals = new double[2];
		for (int i = 0; i < ndList.getLength(); i++) {
			Node nde = ndList.item(i);
			if (nde.getNodeType() == Node.ELEMENT_NODE) {
				String ndName = nde.getNodeName();
				switch (ndName) {
				case "lowerlimit":
					vals[0] = Double.parseDouble(this.getAttrib(nde, "value"));
					break;
				case "upperlimit":
					vals[1] = Double.parseDouble(this.getAttrib(nde, "value"));
					break;
				}
			}
		}
		return vals;
	}

}
