/**
 * SolarTracking Copyright (C) 2017 Georgia State University
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.gsu.dmlab.datatypes;

import java.awt.Polygon;
import java.awt.Rectangle;
import java.util.UUID;

import org.joda.time.Interval;

import edu.gsu.dmlab.datatypes.interfaces.IBaseDataType;
import edu.gsu.dmlab.datatypes.interfaces.IEvent;
import edu.gsu.dmlab.geometry.Point2D;

/**
 * Class that is used for the input data of a flare event type. This data file
 * comes from [Supporting Data: A large-scale dataset of solar event reports
 * from automated feature recognition
 * modules](https://doi.org/10.5281/zenodo.48187)
 * 
 * @author Dustin Kempton, Data Mining Lab, Georgia State University
 * @version {@value edu.gsu.dmlab.ConstValues#major_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#minor_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#build_ver}
 */
public class FLEvent extends BaseHekEvent implements IEvent {

	private double fl_peakflux;
	private double frm_versionnumber;

	private IEvent next = null;
	private IEvent prev = null;
	private UUID uniqueId = null;

	public FLEvent(int eventID, Interval period, String observatory, String instrument, String channel, String hpcBBox,
			String hpcCenterLocation, String hpcShape, double fl_peakflux, double frm_versionnumber) {
		super(eventID, period, observatory, instrument, channel, hpcBBox, hpcCenterLocation, hpcShape);

		this.fl_peakflux = fl_peakflux;
		this.frm_versionnumber = frm_versionnumber;

		this.uniqueId = UUID.randomUUID();
	}

	@Override
	public int compareTime(IBaseDataType baseDataType) {
		return this.getTimePeriod().getStart().compareTo(baseDataType.getTimePeriod().getStart());
	}

	@Override
	public Interval getTimePeriod() {
		return super.getPeriod();
	}

	@Override
	public Rectangle getBBox() {
		return null;
	}

	@Override
	public int getId() {
		return super.eventID;
	}

	@Override
	public Point2D getLocation() {
		return null;
	}

	@Override
	public IEvent getNext() {
		return this.next;
	}

	@Override
	public IEvent getPrevious() {
		return this.prev;
	}

	@Override
	public Polygon getShape() {
		return null;
	}

	@Override
	public EventType getType() {
		return EventType.FLARE;
	}

	@Override
	public UUID getUUID() {
		return this.uniqueId;
	}

	@Override
	public void setNext(IEvent event) {
		if (this.next == null) {
			this.next = event;
		}
	}

	@Override
	public void setPrevious(IEvent event) {
		if (this.prev == null) {
			this.prev = event;
		}
	}

	@Override
	public void updateTimePeriod(Interval period) {
		super.period = period;
	}

	public double getFl_peakflux() {
		return fl_peakflux;
	}

	public void setFl_peakflux(double fl_peakflux) {
		this.fl_peakflux = fl_peakflux;
	}

	public double getFrm_versionnumber() {
		return frm_versionnumber;
	}

	public void setFrm_versionnumber(double frm_versionnumber) {
		this.frm_versionnumber = frm_versionnumber;
	}

}
