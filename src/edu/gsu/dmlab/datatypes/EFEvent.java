/**
 * SolarTracking Copyright (C) 2017 Georgia State University
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.gsu.dmlab.datatypes;

import java.awt.Polygon;
import java.awt.Rectangle;
import java.util.UUID;

import org.joda.time.Interval;

import edu.gsu.dmlab.datatypes.interfaces.IBaseDataType;
import edu.gsu.dmlab.datatypes.interfaces.IEvent;
import edu.gsu.dmlab.geometry.Point2D;

/**
 * Class that is used for the input data of a emerging flux event type. This
 * data file comes from [Supporting Data: A large-scale dataset of solar event
 * reports from automated feature recognition
 * modules](https://doi.org/10.5281/zenodo.48187)
 * 
 * @author Dustin Kempton, Data Mining Lab, Georgia State University
 * @version {@value edu.gsu.dmlab.ConstValues#major_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#minor_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#build_ver}
 */
public class EFEvent extends BaseHekEvent implements IEvent {

	private double area_atdiskcenter;
	private double area_atdiskcenteruncert;
	private double area_raw;
	private double area_uncert;
	private String area_unit;
	private double event_npixels;
	private String event_pixelunit;
	private double ef_pospeakfluxonsetrate;
	private double ef_negpeakfluxonsetrate;
	private String ef_onsetrateunit;
	private double ef_sumpossignedflux;
	private double ef_sumnegsignedflux;
	private String ef_fluxunit;
	private double ef_axisorientation;
	private String ef_axisorientationunit;
	private double ef_axislength;
	private double ef_posequivradius;
	private double ef_negequivradius;
	private String ef_lengthunit;
	private double ef_aspectratio;
	private double ef_proximityratio;
	private double maxmagfieldstrength;
	private String maxmagfieldstrengthunit;
	private double frm_versionnumber;

	private IEvent next = null;
	private IEvent prev = null;
	private UUID uniqueId = null;

	public EFEvent(int eventID, Interval period, String observatory, String instrument, String channel, String hpcBBox,
			String hpcCenterLocation, String hpcShape, double area_atdiskcenter, double area_atdiskcenteruncert,
			double area_raw, double area_uncert, String area_unit, double event_npixels, String event_pixelunit,
			double ef_pospeakfluxonsetrate, double ef_negpeakfluxonsetrate, String ef_onsetrateunit,
			double ef_sumpossignedflux, double ef_sumnegsignedflux, String ef_fluxunit, double ef_axisorientation,
			String ef_axisorientationunit, double ef_axislength, double ef_posequivradius, double ef_negequivradius,
			String ef_lengthunit, double ef_aspectratio, double ef_proximityratio, double maxmagfieldstrength,
			String maxmagfieldstrengthunit, double frm_versionnumber) {
		super(eventID, period, observatory, instrument, channel, hpcBBox, hpcCenterLocation, hpcShape);

		this.area_atdiskcenter = area_atdiskcenter;
		this.area_atdiskcenteruncert = area_atdiskcenteruncert;
		this.area_raw = area_raw;
		this.area_uncert = area_uncert;
		this.area_unit = area_unit;
		this.event_npixels = event_npixels;
		this.event_pixelunit = event_pixelunit;
		this.ef_pospeakfluxonsetrate = ef_pospeakfluxonsetrate;
		this.ef_negpeakfluxonsetrate = ef_negpeakfluxonsetrate;
		this.ef_onsetrateunit = ef_onsetrateunit;
		this.ef_sumpossignedflux = ef_sumpossignedflux;
		this.ef_sumnegsignedflux = ef_sumnegsignedflux;
		this.ef_fluxunit = ef_fluxunit;
		this.ef_axisorientation = ef_axisorientation;
		this.ef_axisorientationunit = ef_axisorientationunit;
		this.ef_axislength = ef_axislength;
		this.ef_posequivradius = ef_posequivradius;
		this.ef_negequivradius = ef_negequivradius;
		this.ef_lengthunit = ef_lengthunit;
		this.ef_aspectratio = ef_aspectratio;
		this.ef_proximityratio = ef_proximityratio;
		this.maxmagfieldstrength = maxmagfieldstrength;
		this.maxmagfieldstrengthunit = maxmagfieldstrengthunit;
		this.frm_versionnumber = frm_versionnumber;

		this.uniqueId = UUID.randomUUID();
	}

	@Override
	public int compareTime(IBaseDataType baseDataType) {
		return this.getTimePeriod().getStart().compareTo(baseDataType.getTimePeriod().getStart());
	}

	@Override
	public Interval getTimePeriod() {
		return super.getPeriod();
	}

	@Override
	public Rectangle getBBox() {
		return null;
	}

	@Override
	public int getId() {
		return super.eventID;
	}

	@Override
	public Point2D getLocation() {
		return null;
	}

	@Override
	public IEvent getNext() {
		return this.next;
	}

	@Override
	public IEvent getPrevious() {
		return this.prev;
	}

	@Override
	public Polygon getShape() {
		return null;
	}

	@Override
	public EventType getType() {
		return EventType.EMERGING_FLUX;
	}

	@Override
	public UUID getUUID() {
		return this.uniqueId;
	}

	@Override
	public void setNext(IEvent event) {
		if (this.next == null) {
			this.next = event;
		}
	}

	@Override
	public void setPrevious(IEvent event) {
		if (this.prev == null) {
			this.prev = event;
		}
	}

	@Override
	public void updateTimePeriod(Interval period) {
		super.period = period;
	}

	public double getArea_atdiskcenter() {
		return area_atdiskcenter;
	}

	public void setArea_atdiskcenter(double area_atdiskcenter) {
		this.area_atdiskcenter = area_atdiskcenter;
	}

	public double getArea_atdiskcenteruncert() {
		return area_atdiskcenteruncert;
	}

	public void setArea_atdiskcenteruncert(double area_atdiskcenteruncert) {
		this.area_atdiskcenteruncert = area_atdiskcenteruncert;
	}

	public double getArea_raw() {
		return area_raw;
	}

	public void setArea_raw(double area_raw) {
		this.area_raw = area_raw;
	}

	public double getArea_uncert() {
		return area_uncert;
	}

	public void setArea_uncert(double area_uncert) {
		this.area_uncert = area_uncert;
	}

	public String getArea_unit() {
		return area_unit;
	}

	public void setArea_unit(String area_unit) {
		this.area_unit = area_unit;
	}

	public double getEvent_npixels() {
		return event_npixels;
	}

	public void setEvent_npixels(double event_npixels) {
		this.event_npixels = event_npixels;
	}

	public String getEvent_pixelunit() {
		return event_pixelunit;
	}

	public void setEvent_pixelunit(String event_pixelunit) {
		this.event_pixelunit = event_pixelunit;
	}

	public double getEf_pospeakfluxonsetrate() {
		return ef_pospeakfluxonsetrate;
	}

	public void setEf_pospeakfluxonsetrate(double ef_pospeakfluxonsetrate) {
		this.ef_pospeakfluxonsetrate = ef_pospeakfluxonsetrate;
	}

	public double getEf_negpeakfluxonsetrate() {
		return ef_negpeakfluxonsetrate;
	}

	public void setEf_negpeakfluxonsetrate(double ef_negpeakfluxonsetrate) {
		this.ef_negpeakfluxonsetrate = ef_negpeakfluxonsetrate;
	}

	public String getEf_onsetrateunit() {
		return ef_onsetrateunit;
	}

	public void setEf_onsetrateunit(String ef_onsetrateunit) {
		this.ef_onsetrateunit = ef_onsetrateunit;
	}

	public double getEf_sumpossignedflux() {
		return ef_sumpossignedflux;
	}

	public void setEf_sumpossignedflux(double ef_sumpossignedflux) {
		this.ef_sumpossignedflux = ef_sumpossignedflux;
	}

	public double getEf_sumnegsignedflux() {
		return ef_sumnegsignedflux;
	}

	public void setEf_sumnegsignedflux(double ef_sumnegsignedflux) {
		this.ef_sumnegsignedflux = ef_sumnegsignedflux;
	}

	public String getEf_fluxunit() {
		return ef_fluxunit;
	}

	public void setEf_fluxunit(String ef_fluxunit) {
		this.ef_fluxunit = ef_fluxunit;
	}

	public double getEf_axisorientation() {
		return ef_axisorientation;
	}

	public void setEf_axisorientation(double ef_axisorientation) {
		this.ef_axisorientation = ef_axisorientation;
	}

	public String getEf_axisorientationunit() {
		return ef_axisorientationunit;
	}

	public void setEf_axisorientationunit(String ef_axisorientationunit) {
		this.ef_axisorientationunit = ef_axisorientationunit;
	}

	public double getEf_axislength() {
		return ef_axislength;
	}

	public void setEf_axislength(double ef_axislength) {
		this.ef_axislength = ef_axislength;
	}

	public double getEf_posequivradius() {
		return ef_posequivradius;
	}

	public void setEf_posequivradius(double ef_posequivradius) {
		this.ef_posequivradius = ef_posequivradius;
	}

	public double getEf_negequivradius() {
		return ef_negequivradius;
	}

	public void setEf_negequivradius(double ef_negequivradius) {
		this.ef_negequivradius = ef_negequivradius;
	}

	public String getEf_lengthunit() {
		return ef_lengthunit;
	}

	public void setEf_lengthunit(String ef_lengthunit) {
		this.ef_lengthunit = ef_lengthunit;
	}

	public double getEf_aspectratio() {
		return ef_aspectratio;
	}

	public void setEf_aspectratio(double ef_aspectratio) {
		this.ef_aspectratio = ef_aspectratio;
	}

	public double getEf_proximityratio() {
		return ef_proximityratio;
	}

	public void setEf_proximityratio(double ef_proximityratio) {
		this.ef_proximityratio = ef_proximityratio;
	}

	public double getMaxmagfieldstrength() {
		return maxmagfieldstrength;
	}

	public void setMaxmagfieldstrength(double maxmagfieldstrength) {
		this.maxmagfieldstrength = maxmagfieldstrength;
	}

	public String getMaxmagfieldstrengthunit() {
		return maxmagfieldstrengthunit;
	}

	public void setMaxmagfieldstrengthunit(String maxmagfieldstrengthunit) {
		this.maxmagfieldstrengthunit = maxmagfieldstrengthunit;
	}

	public double getFrm_versionnumber() {
		return frm_versionnumber;
	}

	public void setFrm_versionnumber(double frm_versionnumber) {
		this.frm_versionnumber = frm_versionnumber;
	}

}
