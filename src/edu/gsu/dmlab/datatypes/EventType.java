/**
 * SolarTracking Copyright (C) 2017 Georgia State University
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.gsu.dmlab.datatypes;

/**
 * Simply an enumeration used to distinguish which type of solar activity an
 * event object is an instance of.
 * 
 * @author Thaddeus Gholston, Data Mining Lab, Georgia State University
 * @version {@value edu.gsu.dmlab.ConstValues#major_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#minor_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#build_ver}
 */
public enum EventType {
	ACTIVE_REGION(0), CORONAL_HOLE(1), FILAMENT(2), SIGMOID(3), SUNSPOT(4), EMERGING_FLUX(5), FLARE(6), QUIET_SUN(7);
	private final int id;

	EventType(int id) {
		this.id = id;
	}

	public int getValue() {
		return id;
	}
}
