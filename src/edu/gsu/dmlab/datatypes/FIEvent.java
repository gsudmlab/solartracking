/**
 * SolarTracking Copyright (C) 2017 Georgia State University
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.gsu.dmlab.datatypes;

import java.awt.Polygon;
import java.awt.Rectangle;
import java.util.UUID;

import org.joda.time.Interval;

import edu.gsu.dmlab.datatypes.interfaces.IBaseDataType;
import edu.gsu.dmlab.datatypes.interfaces.IEvent;
import edu.gsu.dmlab.geometry.Point2D;

/**
 * Class that is used for the input data of a filament event type. This data
 * file comes from [Supporting Data: A large-scale dataset of solar event
 * reports from automated feature recognition
 * modules](https://doi.org/10.5281/zenodo.48187)
 * 
 * @author Dustin Kempton, Data Mining Lab, Georgia State University
 * @version {@value edu.gsu.dmlab.ConstValues#major_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#minor_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#build_ver}
 */
public class FIEvent extends BaseHekEvent implements IEvent {

	private double area_raw;
	private String area_unit;
	private double event_npixels;
	private String event_pixelunit;
	private double fi_length;
	private String fi_lengthunit;
	private double fi_tilt;
	private double fi_barbstot;
	private double fi_barbsr;
	private double fi_barbsl;
	private double fi_chirality;
	private String fi_barbsstartc1;
	private String fi_barbsstartc2;
	private String fi_barbsendc1;
	private String fi_barbsendc2;
	private double frm_versionnumber;

	private IEvent next = null;
	private IEvent prev = null;
	private UUID uniqueId = null;

	public FIEvent(int eventID, Interval period, String observatory, String instrument, String channel, String hpcBBox,
			String hpcCenterLocation, String hpcShape, double area_raw, String area_unit, double event_npixels,
			String event_pixelunit, double fi_length, String fi_lengthunit, double fi_tilt, double fi_barbstot,
			double fi_barbsr, double fi_barbsl, double fi_chirality, String fi_barbsstartc1, String fi_barbsstartc2,
			String fi_barbsendc1, String fi_barbsendc2, double frm_versionnumber) {
		super(eventID, period, observatory, instrument, channel, hpcBBox, hpcCenterLocation, hpcShape);

		this.area_raw = area_raw;
		this.area_unit = area_unit;
		this.event_npixels = event_npixels;
		this.event_pixelunit = event_pixelunit;
		this.fi_length = fi_length;
		this.fi_lengthunit = fi_lengthunit;
		this.fi_tilt = fi_tilt;
		this.fi_barbstot = fi_barbstot;
		this.fi_barbsr = fi_barbsr;
		this.fi_barbsl = fi_barbsl;
		this.fi_chirality = fi_chirality;
		this.fi_barbsstartc1 = fi_barbsstartc1;
		this.fi_barbsstartc2 = fi_barbsstartc2;
		this.fi_barbsendc1 = fi_barbsendc1;
		this.fi_barbsendc2 = fi_barbsendc2;
		this.frm_versionnumber = frm_versionnumber;

		this.uniqueId = UUID.randomUUID();
	}

	@Override
	public int compareTime(IBaseDataType baseDataType) {
		return this.getTimePeriod().getStart().compareTo(baseDataType.getTimePeriod().getStart());
	}

	@Override
	public Interval getTimePeriod() {
		return super.getPeriod();
	}

	@Override
	public Rectangle getBBox() {
		return null;
	}

	@Override
	public int getId() {
		return super.eventID;
	}

	@Override
	public Point2D getLocation() {

		return null;
	}

	@Override
	public IEvent getNext() {
		return this.next;
	}

	@Override
	public IEvent getPrevious() {
		return this.prev;
	}

	@Override
	public Polygon getShape() {
		// TODO: Fix this shape
		return null;
	}

	@Override
	public EventType getType() {
		return EventType.FILAMENT;
	}

	@Override
	public UUID getUUID() {
		return this.uniqueId;
	}

	@Override
	public void setNext(IEvent event) {
		if (this.next == null) {
			this.next = event;
		}
	}

	@Override
	public void setPrevious(IEvent event) {
		if (this.prev == null) {
			this.prev = event;
		}
	}

	@Override
	public void updateTimePeriod(Interval period) {
		super.period = period;
	}

	public double getArea_raw() {
		return area_raw;
	}

	public void setArea_raw(double area_raw) {
		this.area_raw = area_raw;
	}

	public String getArea_unit() {
		return area_unit;
	}

	public void setArea_unit(String area_unit) {
		this.area_unit = area_unit;
	}

	public double getEvent_npixels() {
		return event_npixels;
	}

	public void setEvent_npixels(double event_npixels) {
		this.event_npixels = event_npixels;
	}

	public String getEvent_pixelunit() {
		return event_pixelunit;
	}

	public void setEvent_pixelunit(String event_pixelunit) {
		this.event_pixelunit = event_pixelunit;
	}

	public double getFi_length() {
		return fi_length;
	}

	public void setFi_length(double fi_length) {
		this.fi_length = fi_length;
	}

	public String getFi_lengthunit() {
		return fi_lengthunit;
	}

	public void setFi_lengthunit(String fi_lengthunit) {
		this.fi_lengthunit = fi_lengthunit;
	}

	public double getFi_tilt() {
		return fi_tilt;
	}

	public void setFi_tilt(double fi_tilt) {
		this.fi_tilt = fi_tilt;
	}

	public double getFi_barbstot() {
		return fi_barbstot;
	}

	public void setFi_barbstot(double fi_barbstot) {
		this.fi_barbstot = fi_barbstot;
	}

	public double getFi_barbsr() {
		return fi_barbsr;
	}

	public void setFi_barbsr(double fi_barbsr) {
		this.fi_barbsr = fi_barbsr;
	}

	public double getFi_barbsl() {
		return fi_barbsl;
	}

	public void setFi_barbsl(double fi_barbsl) {
		this.fi_barbsl = fi_barbsl;
	}

	public double getFi_chirality() {
		return fi_chirality;
	}

	public void setFi_chirality(double fi_chirality) {
		this.fi_chirality = fi_chirality;
	}

	public String getFi_barbsstartc1() {
		return fi_barbsstartc1;
	}

	public void setFi_barbsstartc1(String fi_barbsstartc1) {
		this.fi_barbsstartc1 = fi_barbsstartc1;
	}

	public String getFi_barbsstartc2() {
		return fi_barbsstartc2;
	}

	public void setFi_barbsstartc2(String fi_barbsstartc2) {
		this.fi_barbsstartc2 = fi_barbsstartc2;
	}

	public String getFi_barbsendc1() {
		return fi_barbsendc1;
	}

	public void setFi_barbsendc1(String fi_barbsendc1) {
		this.fi_barbsendc1 = fi_barbsendc1;
	}

	public String getFi_barbsendc2() {
		return fi_barbsendc2;
	}

	public void setFi_barbsendc2(String fi_barbsendc2) {
		this.fi_barbsendc2 = fi_barbsendc2;
	}

	public double getFrm_versionnumber() {
		return frm_versionnumber;
	}

	public void setFrm_versionnumber(double frm_versionnumber) {
		this.frm_versionnumber = frm_versionnumber;
	}

}
