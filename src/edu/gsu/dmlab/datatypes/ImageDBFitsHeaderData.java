/**
 * dmLabLib, a Library created for use in various projects at the Data Mining Lab 
 * (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).  
 *  
 * Copyright (C) 2017 Georgia State University
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.gsu.dmlab.datatypes;

/**
 * The header data associated with a particular image in the database. It
 * contains the x,y center of the sun location within that image. The radius of
 * the sun in the image. The observed distance to the sun when the image was
 * taken. And the number of arc/sec per pixel in the image.
 * 
 * @author Dustin Kempton, Data Mining Lab, Georgia State University
 * @version {@value edu.gsu.dmlab.ConstValues#major_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#minor_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#build_ver}
 */
public class ImageDBFitsHeaderData {
	/**
	 * The x pixel location where the center of the solar radius is on the image
	 * this header data comes from.
	 */
	public double X0;

	/**
	 * The y pixel location where the center of the solar radius is on the image
	 * this header data comes from.
	 */
	public double Y0;

	/**
	 * The solar radius observed in the image that this header data comes from.
	 */
	public double R_SUN;

	/**
	 * The distance to the sun observed at the time of the image that this
	 * header came from.
	 */
	public double DSUN;

	/**
	 * I believe the number of degrees per pixel on the observed image.
	 */
	public double CDELT;
}
