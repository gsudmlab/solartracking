/**
 * SolarTracking Copyright (C) 2017 Georgia State University
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.gsu.dmlab.datatypes;

import java.awt.Polygon;
import java.awt.Rectangle;
import java.util.UUID;

import org.joda.time.Interval;

import edu.gsu.dmlab.datatypes.interfaces.IBaseDataType;
import edu.gsu.dmlab.datatypes.interfaces.IEvent;
import edu.gsu.dmlab.geometry.Point2D;

/**
 * Class that is used for the input data of an active region event type. This
 * data file comes from [Supporting Data: A large-scale dataset of solar event
 * reports from automated feature recognition
 * modules](https://doi.org/10.5281/zenodo.48187)
 * 
 * @author Dustin Kempton, Data Mining Lab, Georgia State University
 * @version {@value edu.gsu.dmlab.ConstValues#major_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#minor_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#build_ver}
 */
public class AREvent extends BaseHekEvent implements IEvent {

	private double area_raw;
	private String area_unit;
	private double area_uncert;
	private double area_atdiskcenter;
	private double area_atdiskcenteruncert;
	private double event_npixels;
	private String event_pixelunit;
	private double intensmin;
	private double intensmax;
	private double intensmean;
	private double intensmedian;
	private double intensvar;
	private double intensskew;
	private double intenskurt;
	private double intenstotal;
	private String intensunit;
	private double frm_versionnumber;

	private IEvent next = null;
	private IEvent prev = null;
	private UUID uniqueId = null;

	public AREvent(int eventID, Interval period, String observatory, String instrument, String channel, String hpcBBox,
			String hpcCenterLocation, String hpcShape, double intensmin, double intensmax, double intensmean,
			double intensmedian, double intensvar, double intensskew, double intenskurt, double intenstotal,
			String intensunit, double area_atdiskcenter, double area_atdiskcenteruncert, double area_raw,
			double area_uncert, String area_unit, double event_npixels, String event_pixelunit,
			double frm_versionnumber) {
		super(eventID, period, observatory, instrument, channel, hpcBBox, hpcCenterLocation, hpcShape);

		this.intensmin = intensmin;
		this.intensmax = intensmax;
		this.intensmean = intensmean;
		this.intensmedian = intensmedian;
		this.intensvar = intensvar;
		this.intensskew = intensskew;
		this.intenskurt = intenskurt;
		this.intenstotal = intenstotal;
		this.intensunit = intensunit;
		this.area_atdiskcenter = area_atdiskcenter;
		this.area_atdiskcenteruncert = area_atdiskcenteruncert;
		this.area_raw = area_raw;
		this.area_uncert = area_uncert;
		this.area_unit = area_unit;
		this.event_npixels = event_npixels;
		this.event_pixelunit = event_pixelunit;
		this.frm_versionnumber = frm_versionnumber;
		this.uniqueId = UUID.randomUUID();
	}

	@Override
	public int compareTime(IBaseDataType baseDataType) {
		return this.getTimePeriod().getStart().compareTo(baseDataType.getTimePeriod().getStart());
	}

	@Override
	public Interval getTimePeriod() {
		return super.getPeriod();
	}

	@Override
	public Rectangle getBBox() {
		return null;
	}

	@Override
	public int getId() {
		return super.eventID;
	}

	@Override
	public Point2D getLocation() {
		return null;
	}

	@Override
	public IEvent getNext() {
		return this.next;
	}

	@Override
	public IEvent getPrevious() {
		return this.prev;
	}

	@Override
	public Polygon getShape() {
		return null;
	}

	@Override
	public EventType getType() {
		return EventType.ACTIVE_REGION;
	}

	@Override
	public UUID getUUID() {
		return this.uniqueId;
	}

	@Override
	public void setNext(IEvent event) {
		if (this.next == null) {
			this.next = event;
		}
	}

	@Override
	public void setPrevious(IEvent event) {
		if (this.prev == null) {
			this.prev = event;
		}
	}

	@Override
	public void updateTimePeriod(Interval period) {
		super.period = period;
	}

	public double getIntensmin() {
		return intensmin;
	}

	public void setIntensmin(double intensmin) {
		this.intensmin = intensmin;
	}

	public double getIntensmax() {
		return intensmax;
	}

	public void setIntensmax(double intensmax) {
		this.intensmax = intensmax;
	}

	public double getIntensmean() {
		return intensmean;
	}

	public void setIntensmean(double intensmean) {
		this.intensmean = intensmean;
	}

	public double getIntensmedian() {
		return intensmedian;
	}

	public void setIntensmedian(double intensmedian) {
		this.intensmedian = intensmedian;
	}

	public double getIntensvar() {
		return intensvar;
	}

	public void setIntensvar(double intensvar) {
		this.intensvar = intensvar;
	}

	public double getIntensskew() {
		return intensskew;
	}

	public void setIntensskew(double intensskew) {
		this.intensskew = intensskew;
	}

	public double getIntenskurt() {
		return intenskurt;
	}

	public void setIntenskurt(double intenskurt) {
		this.intenskurt = intenskurt;
	}

	public double getIntenstotal() {
		return intenstotal;
	}

	public void setIntenstotal(double intenstotal) {
		this.intenstotal = intenstotal;
	}

	public String getIntensunit() {
		return intensunit;
	}

	public void setIntensunit(String intensunit) {
		this.intensunit = intensunit;
	}

	public double getArea_atdiskcenter() {
		return area_atdiskcenter;
	}

	public void setArea_atdiskcenter(double area_atdiskcenter) {
		this.area_atdiskcenter = area_atdiskcenter;
	}

	public double getArea_atdiskcenteruncert() {
		return area_atdiskcenteruncert;
	}

	public void setArea_atdiskcenteruncert(double area_atdiskcenteruncert) {
		this.area_atdiskcenteruncert = area_atdiskcenteruncert;
	}

	public double getArea_raw() {
		return area_raw;
	}

	public void setArea_raw(double area_raw) {
		this.area_raw = area_raw;
	}

	public double getArea_uncert() {
		return area_uncert;
	}

	public void setArea_uncert(double area_uncert) {
		this.area_uncert = area_uncert;
	}

	public String getArea_unit() {
		return area_unit;
	}

	public void setArea_unit(String area_unit) {
		this.area_unit = area_unit;
	}

	public double getEvent_npixels() {
		return event_npixels;
	}

	public void setEvent_npixels(double event_npixels) {
		this.event_npixels = event_npixels;
	}

	public String getEvent_pixelunit() {
		return event_pixelunit;
	}

	public void setEvent_pixelunit(String event_pixelunit) {
		this.event_pixelunit = event_pixelunit;
	}

	public double getFrm_versionnumber() {
		return frm_versionnumber;
	}

	public void setFrm_versionnumber(double frm_versionnumber) {
		this.frm_versionnumber = frm_versionnumber;
	}

}
