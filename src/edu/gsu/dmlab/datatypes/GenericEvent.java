/**
 * SolarTracking Copyright (C) 2017 Georgia State University
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.gsu.dmlab.datatypes;

import java.util.UUID;

import edu.gsu.dmlab.datatypes.interfaces.IBaseDataType;
import edu.gsu.dmlab.geometry.Point2D;

import org.joda.time.Interval;

import edu.gsu.dmlab.datatypes.interfaces.IEvent;

import java.awt.Polygon;
import java.awt.Rectangle;

/**
 * Is a generic event type used to represent a single detection of a solar event
 * taken from HEK.
 * 
 * @author Dustin Kempton, Data Mining Lab, Georgia State University
 * @version {@value edu.gsu.dmlab.ConstValues#major_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#minor_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#build_ver}
 */
public class GenericEvent implements IEvent {

	private int id;
	private Polygon poly = null;
	private Rectangle bBox = null;
	private Point2D location = null;
	private Interval timePeriod = null;
	private EventType type = null;

	IEvent next = null;
	IEvent previous = null;
	UUID uniqueId = null;
	private boolean interpolated;

	public GenericEvent(int id, Interval timePeriod, Point2D location, Rectangle bbox, Polygon poly, EventType type) {
		if (timePeriod == null)
			throw new IllegalArgumentException("Interval cannot be null in GenaricEvet constructor.");
		if (location == null)
			throw new IllegalArgumentException("Point2D cannot be null in GenaricEvet constructor.");
		if (bbox == null)
			throw new IllegalArgumentException("Rectangle cannot be null in GenaricEvet constructor.");
		if (poly == null)
			throw new IllegalArgumentException("Polygon cannot be null in GenaricEvet constructor.");
		if (type == null)
			throw new IllegalArgumentException("Type cannot be null in GenaricEvet constructor.");

		this.id = id;
		this.poly = poly;
		this.bBox = bbox;
		this.location = location;
		this.timePeriod = timePeriod;
		this.type = type;
		this.uniqueId = UUID.randomUUID();
	}

	public GenericEvent(Interval timePeriod) {
		this.timePeriod = timePeriod;
	}

	public GenericEvent(int id2, Interval range, Rectangle bounds, Polygon poly2, EventType type2) {
		if (range == null)
			throw new IllegalArgumentException("Interval cannot be null in GenaricEvet constructor.");
		if (poly2 == null)
			throw new IllegalArgumentException("Polygon cannot be null in GenaricEvet constructor.");
		if (type2 == null)
			throw new IllegalArgumentException("Type cannot be null in GenaricEvet constructor.");

		this.id = id2;
		this.poly = poly2;
		this.bBox = bounds;
		this.timePeriod = range;
		this.type = type2;
		this.uniqueId = UUID.randomUUID();
	}

	public GenericEvent(int id2, Interval range, Rectangle bounds, Polygon poly2, boolean interpolated,
			EventType type2) {
		if (range == null)
			throw new IllegalArgumentException("Interval cannot be null in GenaricEvet constructor.");
		if (poly2 == null)
			throw new IllegalArgumentException("Polygon cannot be null in GenaricEvet constructor.");
		if (type2 == null)
			throw new IllegalArgumentException("Type cannot be null in GenaricEvet constructor.");

		this.id = id2;
		this.poly = poly2;
		this.timePeriod = range;
		this.type = type2;
		this.bBox = bounds;
		this.uniqueId = UUID.randomUUID();
		this.interpolated = interpolated;
	}

	public boolean isInterpolated() {
		return interpolated;
	}

	public void setInterpolated(boolean interpolated) {
		this.interpolated = interpolated;
	}

	@Override
	public int getId() {
		return this.id;
	}

	@Override
	public Point2D getLocation() {
		return this.location;
	}

	@Override
	public Rectangle getBBox() {
		return this.bBox;
	}

	@Override
	public Polygon getShape() {
		return this.poly;
	}

	@Override
	public Interval getTimePeriod() {
		return this.timePeriod;
	}

	@Override
	public int compareTime(IBaseDataType baseDataType) {
		return this.getTimePeriod().getStart().compareTo(baseDataType.getTimePeriod().getStart());
	}

	@Override
	public void updateTimePeriod(Interval period) {
		this.timePeriod = period;
	}

	@Override
	public EventType getType() {
		return this.type;
	}

	@Override
	public IEvent getPrevious() {
		return this.previous;
	}

	@Override
	public void setPrevious(IEvent event) {
		if (this.previous == null) {
			this.previous = event;
		} else if (event == null && this.previous != null) {
			IEvent ev = this.previous;
			this.previous = null;
			ev.setNext(null);
		}
	}

	@Override
	public IEvent getNext() {
		return this.next;
	}

	@Override
	public void setNext(IEvent event) {
		if (this.next == null) {
			this.next = event;
		} else if (event == null && this.next != null) {
			IEvent ev = this.next;
			this.next = null;
			ev.setPrevious(null);
		}
	}

	@Override
	public UUID getUUID() {
		return this.uniqueId;
	}

}