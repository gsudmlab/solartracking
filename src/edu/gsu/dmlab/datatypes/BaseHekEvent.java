package edu.gsu.dmlab.datatypes;

import org.joda.time.Interval;

public class BaseHekEvent {
	int eventID;
	Interval period;
	String observatory;
	String instrument;
	String channel;
	String hpcBBox;
	String hpcCenterLocation;
	String hpcShape;

	public BaseHekEvent(int eventID, Interval period, String observatory, String instrument, String channel,
			String hpcBBox, String hpcCenterLocation, String hpcShape) {
		this.eventID = eventID;
		this.period = period;
		this.observatory = observatory;
		this.instrument = instrument;
		this.channel = channel;
		this.hpcBBox = hpcBBox;
		this.hpcCenterLocation = hpcCenterLocation;
		this.hpcShape = hpcShape;
	}

	public int getEventID() {
		return eventID;
	}

	public void setEventID(int eventID) {
		this.eventID = eventID;
	}

	public Interval getPeriod() {
		return period;
	}

	public void setPeriod(Interval period) {
		this.period = period;
	}

	public String getObservatory() {
		return observatory;
	}

	public void setObservatory(String observatory) {
		this.observatory = observatory;
	}

	public String getInstrument() {
		return instrument;
	}

	public void setInstrument(String instrument) {
		this.instrument = instrument;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getHpcBBox() {
		return hpcBBox;
	}

	public void setHpcBBox(String hpcBBox) {
		this.hpcBBox = hpcBBox;
	}

	public String getHpcCenterLocation() {
		return hpcCenterLocation;
	}

	public void setHpcCenterLocation(String hpcCenterLocation) {
		this.hpcCenterLocation = hpcCenterLocation;
	}

	public String getHpcShape() {
		return hpcShape;
	}

	public void setHpcShape(String hpcShape) {
		this.hpcShape = hpcShape;
	}

}
