/**
 * SolarTracking Copyright (C) 2017 Georgia State University
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.gsu.dmlab.datatypes.interfaces;

import edu.gsu.dmlab.datatypes.EventType;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * The public interface for event tracks processed by tracking algorithms
 * implemented by the Data Mining Lab at Georgia State University
 * 
 * @author Dustin Kempton, Data Mining Lab, Georgia State University
 * @version {@value edu.gsu.dmlab.ConstValues#major_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#minor_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#build_ver}
 */
public interface ITrack extends IBaseDataType, Serializable {

	/**
	 * Returns the start time in milliseconds of the first event in the track
	 * 
	 * @return The number of milliseconds from epoch that the track starts.
	 */
	long getStartTimeMillis();

	/**
	 * Returns the end time in milliseconds of the last event in the track
	 * 
	 * @return The number of milliseconds from epoch that the track ends.
	 */
	long getEndTimeMillis();

	/**
	 * Returns an array that contains all of the events currently in the track
	 * 
	 * @return array of all the events currently in the track
	 */
	ArrayList<IEvent> getEvents();

	int size();

	/**
	 * Returns the first event in the track
	 * 
	 * @return the first event in the track
	 */
	IEvent getFirst();

	/**
	 * Returns the last event in the track
	 * 
	 * @return the last event in the track
	 */
	IEvent getLast();

	/**
	 * Returns the event type of the track
	 * 
	 * @return the event type of the track
	 */
	public EventType getType();

}