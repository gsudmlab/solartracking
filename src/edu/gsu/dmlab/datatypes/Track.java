/**
 * SolarTracking Copyright (C) 2017 Georgia State University
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.gsu.dmlab.datatypes;

import edu.gsu.dmlab.datatypes.interfaces.IBaseDataType;
import edu.gsu.dmlab.datatypes.interfaces.IEvent;
import edu.gsu.dmlab.datatypes.interfaces.ITrack;
import org.joda.time.Interval;

import java.util.ArrayList;
import java.util.Collection;
import java.util.UUID;

/**
 * Track is the public object for event tracks processed by tracking algorithms
 * 
 * @author Dustin Kempton, Data Mining Lab, Georgia State University implemented
 *         by the Data Mining Lab at Georgia State University
 * @version {@value edu.gsu.dmlab.ConstValues#major_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#minor_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#build_ver}
 */
@SuppressWarnings("serial")
public class Track implements ITrack {

	private UUID uniqueId = null;
	private IEvent headEvent;
	private IEvent tailEvent;
	private ArrayList<IEvent> eventsList = null;

	private boolean outDated = true;

	/**
	 * Constructor for when there is only one detection known to be in this
	 * track object.
	 * 
	 * @param event
	 *            The single event detection known to be in this track object.
	 */
	public Track(IEvent event) {
		this();
		this.headEvent = event;
		this.tailEvent = event;
	}

	/**
	 * Constructor for when the entire collection of detection objects in the
	 * track are known. The collection must be sorted and the objects must be
	 * linked together via their previous and next indicator variables.
	 * 
	 * @param events
	 *            The collection of objects in this track.
	 */
	public Track(Collection<IEvent> events) {
		this();
		this.eventsList = new ArrayList<IEvent>();
		events.forEach(ev -> {
			this.eventsList.add(ev);
		});
		this.headEvent = this.eventsList.get(0);
		this.tailEvent = this.eventsList.get(this.eventsList.size() - 1);
	}

	/**
	 * Constructor for when the head and tail detections of a linked list of
	 * detections representing a single track object are known. It is ok to pass
	 * in the same detection for both the head and tail, the track object will
	 * follow the doubly linked list to the head and/or tail when methods that
	 * need to know them are called.
	 * 
	 * @param headEvent
	 *            The detection object that is the start of the track
	 *            represented by this object.
	 * @param tailEvent
	 *            The detection object that is the end of the track represented
	 *            by this object.
	 */
	public Track(IEvent headEvent, IEvent tailEvent) {
		this();
		this.headEvent = headEvent;
		this.tailEvent = tailEvent;
	}

	/**
	 * Private default constructor used to generate a uuid for this object.
	 */
	private Track() {
		this.uniqueId = UUID.randomUUID();
	}

	@Override
	public long getStartTimeMillis() {
		return this.getFirst().getTimePeriod().getStartMillis();
	}

	@Override
	public long getEndTimeMillis() {
		return this.getLast().getTimePeriod().getEndMillis();
	}

	@SuppressWarnings("unchecked")
	@Override
	public ArrayList<IEvent> getEvents() {
		this.getFirst();
		this.getLast();
		if (this.outDated) {
			this.update();
		}
		return (ArrayList<IEvent>) this.eventsList.clone();
	}

	@Override
	public IEvent getFirst() {
		while (this.headEvent.getPrevious() != null) {
			this.outDated = true;
			this.headEvent = this.headEvent.getPrevious();
		}
		return this.headEvent;
	}

	@Override
	public IEvent getLast() {
		while (this.tailEvent.getNext() != null) {
			this.outDated = true;
			this.tailEvent = this.tailEvent.getNext();
		}
		return this.tailEvent;
	}

	@Override
	public Interval getTimePeriod() {
		return new Interval(this.getStartTimeMillis(), this.getEndTimeMillis());
	}

	@Override
	public int compareTime(IBaseDataType baseDataType) {
		ITrack track = (ITrack) baseDataType;
		return this.getFirst().getTimePeriod().getStart().compareTo(track.getTimePeriod().getStart());
	}

	@Override
	public EventType getType() {
		return this.getFirst().getType();
	}

	@Override
	public UUID getUUID() {
		return this.uniqueId;
	}

	@Override
	public int size() {
		this.getFirst();
		this.getLast();
		if (this.outDated) {
			this.update();
		}
		return this.eventsList.size();
	}

	private void update() {
		IEvent first = this.getFirst();

		ArrayList<IEvent> evList = new ArrayList<IEvent>();
		evList.add(first);

		while (first.getNext() != null) {
			first = first.getNext();
			evList.add(first);
		}
		this.eventsList = evList;
		this.outDated = false;
	}
}