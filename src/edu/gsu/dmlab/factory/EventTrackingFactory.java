/**
 * SolarTracking Copyright (C) 2017 Georgia State University
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.gsu.dmlab.factory;

import java.awt.Polygon;
import java.awt.Rectangle;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.joda.time.Interval;

import edu.gsu.dmlab.config.ConfigReader;
import edu.gsu.dmlab.databases.DMLabPullingImageDBConnection;
import edu.gsu.dmlab.databases.ImageDBCreator;
import edu.gsu.dmlab.databases.SingleFeatureDBConnection;
import edu.gsu.dmlab.databases.SingleFeatureDBCreator;
import edu.gsu.dmlab.databases.interfaces.IFeatureDBConnection;
import edu.gsu.dmlab.databases.interfaces.IFeatureDBCreator;
import edu.gsu.dmlab.databases.interfaces.IImageDBConnection;
import edu.gsu.dmlab.databases.interfaces.IImageDBCreator;
import edu.gsu.dmlab.datatypes.EventType;
import edu.gsu.dmlab.datatypes.GenericEvent;
import edu.gsu.dmlab.datatypes.Track;
import edu.gsu.dmlab.datatypes.Waveband;
import edu.gsu.dmlab.datatypes.interfaces.IEvent;
import edu.gsu.dmlab.datatypes.interfaces.ITrack;
import edu.gsu.dmlab.factory.interfaces.IEventTrackingFactory;
import edu.gsu.dmlab.factory.interfaces.IGraphProblemFactory;
import edu.gsu.dmlab.features.FStatProducer;
import edu.gsu.dmlab.features.FeatureSetSelector;
import edu.gsu.dmlab.features.interfaces.IFeatureSetSelector;
import edu.gsu.dmlab.features.interfaces.IStatProducer;
import edu.gsu.dmlab.geometry.Point2D;
import edu.gsu.dmlab.imageproc.HistogramProducer;
import edu.gsu.dmlab.imageproc.ImgParamNormalizer;
import edu.gsu.dmlab.imageproc.ImgPatchVectorizer;
import edu.gsu.dmlab.imageproc.histocomp.IntersectionHistoComparator;
import edu.gsu.dmlab.imageproc.interfaces.IHistoComparator;
import edu.gsu.dmlab.imageproc.interfaces.IHistogramProducer;
import edu.gsu.dmlab.imageproc.interfaces.IImgParamNormalizer;
import edu.gsu.dmlab.imageproc.interfaces.IImgPatchVectorizer;
import edu.gsu.dmlab.indexes.interfaces.IEventIndexer;
import edu.gsu.dmlab.indexes.interfaces.ITrackIndexer;
import edu.gsu.dmlab.sparse.approximation.LARS_LassoCoeffVectorApproximator;
import edu.gsu.dmlab.sparse.approximation.LARS_LassoSparseMatrixApproximator;
import edu.gsu.dmlab.sparse.approximation.LassoMode;
import edu.gsu.dmlab.sparse.approximation.interfaces.ISparseMatrixApproximator;
import edu.gsu.dmlab.sparse.approximation.interfaces.ISparseVectorApproximator;
import edu.gsu.dmlab.sparse.dictionary.BlockCoordinateDescentDictionaryUpdater;
import edu.gsu.dmlab.sparse.dictionary.CorrelationDictionaryCleaner;
import edu.gsu.dmlab.sparse.dictionary.OnlineDictionaryLearner;
import edu.gsu.dmlab.sparse.dictionary.interfaces.IDictionaryCleaner;
import edu.gsu.dmlab.sparse.dictionary.interfaces.IDictionaryUpdater;
import edu.gsu.dmlab.sparse.dictionary.interfaces.ISparseDictionaryLearner;
import edu.gsu.dmlab.tracking.BinomialFrameSkipModel;
import edu.gsu.dmlab.tracking.GraphAssociationProblem;
import edu.gsu.dmlab.tracking.LocationProbCalc;
import edu.gsu.dmlab.tracking.MotionModel;
import edu.gsu.dmlab.tracking.PoissonObsModel;
import edu.gsu.dmlab.tracking.appearance.SparseGenLikeliModel;
import edu.gsu.dmlab.tracking.appearance.SparseHistoAppearanceModel;
import edu.gsu.dmlab.tracking.appearance.SparseHistoCreator;
import edu.gsu.dmlab.tracking.appearance.interfaces.IAppearanceModel;
import edu.gsu.dmlab.tracking.appearance.interfaces.ISparseCandidateModel;
import edu.gsu.dmlab.tracking.appearance.interfaces.ISparseHistoCreator;
import edu.gsu.dmlab.tracking.interfaces.IAssociationProblem;
import edu.gsu.dmlab.tracking.interfaces.IFrameSkipModel;
import edu.gsu.dmlab.tracking.interfaces.ILocationProbCal;
import edu.gsu.dmlab.tracking.interfaces.IMotionModel;
import edu.gsu.dmlab.tracking.interfaces.IObsModel;
import edu.gsu.dmlab.tracking.stages.EdgeWeightCalculatorStageThree;
import edu.gsu.dmlab.tracking.stages.EdgeWeightCalculatorStageTwo;
import edu.gsu.dmlab.tracking.stages.StageOne;
import edu.gsu.dmlab.tracking.stages.StageThree;
import edu.gsu.dmlab.tracking.stages.StageTwo;
import edu.gsu.dmlab.tracking.stages.interfaces.IEdgeWeightCalculator;
import edu.gsu.dmlab.tracking.stages.interfaces.IProcessingStage;
import edu.gsu.dmlab.util.TrapezoidPositionPredictor;
import edu.gsu.dmlab.util.interfaces.ISearchAreaProducer;

/**
 * EventTrackingFactory is the main class where almost everything that is used
 * in the tracking portion of this project is constructed.
 * 
 * @author Dustin Kempton, Data Mining Lab, Georgia State University
 * @version {@value edu.gsu.dmlab.ConstValues#major_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#minor_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#build_ver}
 */
public class EventTrackingFactory implements IEventTrackingFactory {

	private ISearchAreaProducer predictor;

	private IGraphProblemFactory graphFactory;
	private ISparseDictionaryLearner dictionaryLearner;
	private ISparseMatrixApproximator coefExtractor;
	private IImgPatchVectorizer patchVectorizer;
	private IImageDBConnection imageDB;
	private IFeatureDBCreator featdbCreator;
	private IFeatureDBConnection featDbConnect;

	private ILocationProbCal exitProbCalculator = null;
	private ILocationProbCal enterProbCalculator = null;
	private IFrameSkipModel skipModel = null;
	private IMotionModel motionModel = null;
	private IAppearanceModel appearanceModel = null;

	private int cacheSize;
	private int regionDiv;

	private int patchSize;
	private int frameSpan;
	private int numFeatures;
	private String enterProbFileLoc;
	private String exitProbLileLoc;
	private EventType type;

	private double entExitMult;
	private double obsMult;
	private double assocMult;
	private double appearWeight;
	private double skipWeight;
	private double motionWeight;

	private double fractionOfInput = 0.15;
	private int batchSize = 512;

	private ArrayList<Waveband> wavelengths;
	private ArrayList<Integer> params;

	public EventTrackingFactory(ConfigReader config, EventType type) {

		this.type = type;

		String probFolder = "prob_files";

		this.enterProbFileLoc = System.getProperty("user.dir") + File.separator + probFolder + File.separator
				+ "eventsStartProb.csv";
		this.exitProbLileLoc = System.getProperty("user.dir") + File.separator + probFolder + File.separator
				+ "eventsEndProb.csv";

		this.regionDiv = config.getRegionDiv();
		this.patchSize = config.getVecorizorPatchSize();

		this.frameSpan = config.getEventConfig(this.type).getTimeSpan();

		this.numFeatures = config.getNumFeatures();
		this.entExitMult = config.getEntExitMult();
		this.obsMult = config.getObsMult();
		this.assocMult = config.getAssocMult();
		this.appearWeight = config.getAppearWeight();
		this.skipWeight = config.getSkipWeight();
		this.motionWeight = config.getSkipWeight();
		this.cacheSize = config.getCacheSize();

		this.wavelengths = config.getWavelengths();
		this.params = config.getParams();

		this.predictor = new TrapezoidPositionPredictor();

		this.graphFactory = new GraphProblemFactory();
		ISparseVectorApproximator solver = new LARS_LassoCoeffVectorApproximator(config.getLambda(), LassoMode.PENALTY);

		IDictionaryUpdater updater = new BlockCoordinateDescentDictionaryUpdater(config.getDictErrorLim());
		IDictionaryCleaner cleaner = new CorrelationDictionaryCleaner();
		this.dictionaryLearner = new OnlineDictionaryLearner(solver, updater, cleaner, this.fractionOfInput,
				this.batchSize);
		this.coefExtractor = new LARS_LassoSparseMatrixApproximator(config.getLambda(), LassoMode.L1COEFF);
		this.patchVectorizer = new ImgPatchVectorizer(config.getVectorizorStepSize(), config.getVecorizorPatchSize());

		IImgParamNormalizer normalizer = new ImgParamNormalizer(config.getHistoMap());
		IImageDBCreator creator = new ImageDBCreator(config.getTrackDBSource());
		this.imageDB = new DMLabPullingImageDBConnection(config.getTrackDBSource(), creator, normalizer,
				config.getCacheSize());
		this.featdbCreator = new SingleFeatureDBCreator(config.getTrackDBSource());
		this.featDbConnect = new SingleFeatureDBConnection(config.getTrackDBSource(),
				this.wavelengths.size() * this.params.size());
	}

	@Override
	public ITrack getTrack(IEvent event) {
		ITrack trk = new Track(event);
		return trk;
	}

	@Override
	public IEvent getEvent(int id, Interval validInterval, Point2D center, Rectangle mbr, Polygon poly,
			EventType eventType) {
		IEvent ev = new GenericEvent(id, validInterval, center, mbr, poly, eventType);
		return ev;
	}

	@Override
	public IFrameSkipModel getSkipModel() {
		if (skipModel == null) {
			ILocationProbCal exitProbCalculator = this.getExitModel();
			this.skipModel = new BinomialFrameSkipModel(exitProbCalculator);
		}
		return this.skipModel;
	}

	@Override
	public ILocationProbCal getEnterModel() {
		if (this.enterProbCalculator == null) {
			this.enterProbCalculator = new LocationProbCalc(this.enterProbFileLoc, 64);
		}

		return this.enterProbCalculator;
	}

	@Override
	public ILocationProbCal getExitModel() {
		if (this.exitProbCalculator == null) {
			this.exitProbCalculator = new LocationProbCalc(this.exitProbLileLoc, 64);
		}
		return this.exitProbCalculator;
	}

	@Override
	public IObsModel getObservationModel(IEventIndexer evntsIdxr) {
		IObsModel obs = new PoissonObsModel(evntsIdxr, this.frameSpan);
		return obs;
	}

	@Override
	public IAppearanceModel getAppearanceModel(List<ITrack> tracks) {
		if (this.appearanceModel == null) {
			ISparseHistoCreator histoCreator = new SparseHistoCreator(this.patchSize, this.regionDiv);
			ISparseCandidateModel candidateModel = new SparseGenLikeliModel(this.patchSize, this.regionDiv);
			IHistogramProducer histoProducer = new HistogramProducer(this.imageDB);
			IHistoComparator comparator = new IntersectionHistoComparator();
			IStatProducer statProducer = new FStatProducer(tracks, histoProducer, comparator);
			IFeatureSetSelector featureSelector = new FeatureSetSelector(statProducer, this.featdbCreator,
					this.featDbConnect, this.wavelengths, this.params, this.type);
			this.appearanceModel = new SparseHistoAppearanceModel(this.dictionaryLearner, this.coefExtractor,
					this.patchVectorizer, histoCreator, candidateModel, this.imageDB,
					featureSelector.getBestFeatures(this.numFeatures), this.cacheSize);
		}
		return this.appearanceModel;
	}

	@Override
	public IProcessingStage getStage1(IEventIndexer eventIndexer) {
		IProcessingStage s1 = new StageOne(this.predictor, eventIndexer, this, -1);
		return s1;
	}

	@Override
	public IProcessingStage getStage2(IEventIndexer eventIndexer, ITrackIndexer tracksIdxr, int maxFrameSkip) {
		IProcessingStage s2 = new StageTwo(this.predictor, this, tracksIdxr, eventIndexer, maxFrameSkip, -1);
		return s2;
	}

	@Override
	public IProcessingStage getStage3(IEventIndexer eventIndexer, ITrackIndexer tracksIdxr, int maxFrameSkip) {
		IProcessingStage s3 = new StageThree(this.predictor, this, tracksIdxr, eventIndexer, maxFrameSkip, -1);
		return s3;
	}

	@Override
	public IMotionModel getMotionModel() {
		if (this.motionModel == null) {
			this.motionModel = new MotionModel();
		}
		return this.motionModel;
	}

	@Override
	public IAssociationProblem getAssociationProblem(List<ITrack> tracks, IEventIndexer evntsIdxr, int stage) {
		if (stage == 2) {
			IEdgeWeightCalculator weightCalculator = new EdgeWeightCalculatorStageTwo(this.getEnterModel(),
					this.getExitModel(), this.getObservationModel(evntsIdxr), this.getAppearanceModel(tracks),
					this.getSkipModel(), this.entExitMult, this.obsMult, this.assocMult, this.appearWeight,
					this.skipWeight);
			IAssociationProblem assocProblem = new GraphAssociationProblem(tracks, this.graphFactory, weightCalculator);
			return assocProblem;
		} else {
			IEdgeWeightCalculator weightCalculator = new EdgeWeightCalculatorStageThree(this.getEnterModel(),
					this.getExitModel(), this.getObservationModel(evntsIdxr), this.getAppearanceModel(tracks),
					this.getSkipModel(), this.getMotionModel(), this.entExitMult, this.obsMult, this.assocMult,
					this.appearWeight, this.skipWeight, this.motionWeight);
			IAssociationProblem assocProblem = new GraphAssociationProblem(tracks, this.graphFactory, weightCalculator);
			return assocProblem;
		}
	}

}
