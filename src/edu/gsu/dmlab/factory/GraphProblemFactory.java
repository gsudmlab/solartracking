/**
 * SolarTracking Copyright (C) 2017 Georgia State University
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.gsu.dmlab.factory;

import edu.gsu.dmlab.factory.interfaces.IGraphProblemFactory;
import edu.gsu.dmlab.graph.Edge;
import edu.gsu.dmlab.graph.algo.SuccessiveShortestPaths;
import edu.gsu.dmlab.graph.algo.interfaces.IGraphProblemSolver;
import edu.gsu.dmlab.graph.interfaces.IGraph;
import edu.gsu.dmlab.tracking.stages.interfaces.IEdgeWeightCalculator;
import edu.gsu.dmlab.graph.LockGraph;


/**
 * This class is used create objects for solving the underlying minimum cost
 * multi-commodity flow problem for ITrack association.
 * 
 * @author Dustin Kempton, Data Mining Lab, Georgia State University
 * 
 * @version {@value edu.gsu.dmlab.ConstValues#major_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#minor_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#build_ver}
 */
public class GraphProblemFactory implements IGraphProblemFactory {

	@Override
	public IGraphProblemSolver getGraphSolver() {
		IGraphProblemSolver solver = new SuccessiveShortestPaths();
		return solver;
	}

	@Override
	public Edge getEdge(double weight) {
		Edge e = new Edge(weight);
		return e;
	}

	@Override
	public IGraph getGraph(IEdgeWeightCalculator weightCalculator) {
		LockGraph graph = new LockGraph(Edge.class, this, weightCalculator);
		return graph;
	}

}
