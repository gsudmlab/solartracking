/**
 * SolarTracking Copyright (C) 2017 Georgia State University
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.gsu.dmlab.factory.interfaces;

import java.awt.Polygon;
import java.awt.Rectangle;
import java.util.List;

import org.joda.time.Interval;

import edu.gsu.dmlab.datatypes.EventType;
import edu.gsu.dmlab.datatypes.interfaces.IEvent;
import edu.gsu.dmlab.datatypes.interfaces.ITrack;
import edu.gsu.dmlab.geometry.Point2D;
import edu.gsu.dmlab.indexes.interfaces.IEventIndexer;
import edu.gsu.dmlab.indexes.interfaces.ITrackIndexer;
import edu.gsu.dmlab.tracking.appearance.interfaces.IAppearanceModel;
import edu.gsu.dmlab.tracking.interfaces.IAssociationProblem;
import edu.gsu.dmlab.tracking.interfaces.IFrameSkipModel;
import edu.gsu.dmlab.tracking.interfaces.ILocationProbCal;
import edu.gsu.dmlab.tracking.interfaces.IMotionModel;
import edu.gsu.dmlab.tracking.interfaces.IObsModel;
import edu.gsu.dmlab.tracking.stages.interfaces.IProcessingStage;

/**
 * @author Dustin Kempton, Data Mining Lab, Georgia State University
 * @version {@value edu.gsu.dmlab.ConstValues#major_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#minor_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#build_ver}
 */
public interface IEventTrackingFactory {

	/**
	 * Creates a new track containing the passed in IEvent object.
	 * 
	 * @param event
	 *            The IEvent for the track to contain.
	 * @return The track containing the passed in IEvent.
	 */
	public ITrack getTrack(IEvent event);

	/**
	 * Creates a new IEvent from the data passed in.
	 * 
	 * @param id
	 *            The id of the IEvent from the data source.
	 * @param validInterval
	 *            The time period that the IEvent is valid.
	 * @param center
	 *            The Center Point of the IEvent.
	 * @param mbr
	 *            The Minimum Bounding Rectangle of the IEvent.
	 * @param poly
	 *            The Polygon shape of the IEvent.
	 * @param eventType
	 *            The event type of the IEvent.
	 * @return A new IEvent from the passed in data.
	 */
	public IEvent getEvent(int id, Interval validInterval, Point2D center, Rectangle mbr, Polygon poly,
			EventType eventType);

	/**
	 * Produces a new Graph that depicts the possible paths tracks can take
	 * through multiple shorter track fragments.
	 * 
	 * @param tracks
	 *            List of track fragments to build the association graph from.
	 * @param evntsIdxr
	 *            The indexer of the events that are contained in all the tracks
	 *            passed in. Which is used to calculate the expected change in
	 *            the number of detections over a period of time.
	 * @param stage
	 *            The stage which the association problem shall be used. This
	 *            matters because we calculate the weights differently based on
	 *            what stage we are in.
	 * @return The graph of possible association paths.
	 */
	public IAssociationProblem getAssociationProblem(List<ITrack> tracks, IEventIndexer evntsIdxr, int stage);

	/**
	 * Gets a model for producing probability values based on the number of
	 * skipped frames after a particular track another track starts.
	 * 
	 * @return The fame skip model.
	 */
	public IFrameSkipModel getSkipModel();

	public ILocationProbCal getEnterModel();

	public ILocationProbCal getExitModel();

	public IMotionModel getMotionModel();

	public IObsModel getObservationModel(IEventIndexer evntsIdxr);

	public IAppearanceModel getAppearanceModel(List<ITrack> tracks);

	public IProcessingStage getStage1(IEventIndexer eventIndexer);

	public IProcessingStage getStage2(IEventIndexer eventIndexer, ITrackIndexer tracksIdxr, int maxFrameSkip);

	public IProcessingStage getStage3(IEventIndexer eventIndexer, ITrackIndexer tracksIdxr, int maxFrameSkip);
}
