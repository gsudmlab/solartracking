/**
 * SolarTracking Copyright (C) 2017 Georgia State University
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.gsu.dmlab.factory.interfaces;

import edu.gsu.dmlab.graph.Edge;
import edu.gsu.dmlab.graph.algo.interfaces.IGraphProblemSolver;
import edu.gsu.dmlab.graph.interfaces.IGraph;
import edu.gsu.dmlab.tracking.stages.interfaces.IEdgeWeightCalculator;

/**
 * This interface is for classes that are used to solve the underlying minimum
 * cost multi-commodity flow problem for
 * {@link edu.gsu.dmlab.datatypes.interfaces.ITrack ITrack} association.
 * 
 * @author Dustin Kempton, Data Mining Lab, Georgia State University
 * @version {@value edu.gsu.dmlab.ConstValues#major_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#minor_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#build_ver}
 */
public interface IGraphProblemFactory {

	/**
	 * Produces a new IGraphProblemSolver for solving the ITrack association
	 * problem.
	 * 
	 * @return The solver for the graph produced for the ITrack association
	 *         problem.
	 */
	public IGraphProblemSolver getGraphSolver();

	/**
	 * Produces an edge for use in the Graph.
	 * 
	 * @param weight
	 *            Weight for the edge.
	 * @return An edge with the passed in weight.
	 */
	public Edge getEdge(double weight);

	/**
	 * Produces a new WeightedGraph that is thread safe.
	 * 
	 * @param weightCalculator
	 *            The calculator for the weight of the edges that will be added
	 *            to the graph.
	 * @return A new WeightedGraph
	 */
	public IGraph getGraph(IEdgeWeightCalculator weightCalculator);
}
