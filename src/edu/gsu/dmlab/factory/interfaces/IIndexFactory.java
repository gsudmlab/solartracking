/**
 * SolarTracking Copyright (C) 2017 Georgia State University
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.gsu.dmlab.factory.interfaces;

import java.util.List;

import edu.gsu.dmlab.datatypes.interfaces.IEvent;
import edu.gsu.dmlab.datatypes.interfaces.ITrack;
import edu.gsu.dmlab.indexes.interfaces.IEventIndexer;
import edu.gsu.dmlab.indexes.interfaces.ITrackIndexer;

/**
 * This interface is for classes that produce Indexes of both
 * {@link edu.gsu.dmlab.indexes.interfaces.IEventIndexer IEventIndexer} and
 * {@link edu.gsu.dmlab.indexes.interfaces.ITrackIndexer ITrackIndexer} type. As
 * well as some of the objects that are needed in such indexes.
 * 
 * @author Dustin Kempton, Data Mining Lab, Georgia State University
 * @version {@value edu.gsu.dmlab.ConstValues#major_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#minor_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#build_ver}
 */
public interface IIndexFactory {

	/**
	 * Produces a new IEventIndexer that indexes the IEvent objects passed in.
	 * 
	 * @param regionalList
	 *            The IEvent objects to be indexed.
	 * @return A new IEventIndexer containing the passed in IEvnet objects.
	 */
	public IEventIndexer getEventIndexer(List<IEvent> regionalList);

	/**
	 * Produces a new ITrackIndexer that indexes the ITrack objects passed in.
	 * 
	 * @param trackList
	 *            The ITrack objects to be indexed.
	 * @return A new ITrackIndexer containing the passed in ITrack objects.
	 */
	public ITrackIndexer getTrackIndexer(List<ITrack> trackList);

}
