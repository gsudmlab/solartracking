package edu.gsu.dmlab;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.sql.DataSource;

import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import edu.gsu.dmlab.config.ConfigReader;
import edu.gsu.dmlab.databases.BaseImageDBCheck;
import edu.gsu.dmlab.databases.ImageDBCreator;
import edu.gsu.dmlab.databases.TrackDBCreator;
import edu.gsu.dmlab.datatypes.EventType;
import edu.gsu.dmlab.datatypes.interfaces.IEvent;
import edu.gsu.dmlab.exceptions.InvalidConfigException;
import edu.gsu.dmlab.exceptions.UnknownEventTypeException;
import edu.gsu.dmlab.input.ARFileReader;
import edu.gsu.dmlab.input.CHFileReader;
import edu.gsu.dmlab.input.EFFileReader;
import edu.gsu.dmlab.input.FIFileReader;
import edu.gsu.dmlab.input.FLFileReader;
import edu.gsu.dmlab.input.SGFileReader;
import edu.gsu.dmlab.input.SSFileReader;
import edu.gsu.dmlab.input.interfaces.IInputFileReader;

public class DatabaseCheckAndSetup {

	String dataPath;
	DataSource dsourc;
	BaseImageDBCheck imgChk;
	ArrayList<EventType> types;
	DateTime startDateTime;
	DateTime endDateTime;

	public DatabaseCheckAndSetup(ConfigReader config) {
		this.dataPath = config.getDataLocation();
		this.dsourc = config.getTrackDBSource();

		DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
		String startDateTime = config.getStartDatetime();
		this.startDateTime = formatter.parseDateTime(startDateTime);
		String endDateTime = config.getEndDatetime();
		this.endDateTime = formatter.parseDateTime(endDateTime);
		this.imgChk = new BaseImageDBCheck();

		this.types = new ArrayList<EventType>();
		this.types.add(EventType.ACTIVE_REGION);
		this.types.add(EventType.CORONAL_HOLE);
		this.types.add(EventType.EMERGING_FLUX);
		this.types.add(EventType.FILAMENT);
		this.types.add(EventType.FLARE);
		this.types.add(EventType.SIGMOID);
		this.types.add(EventType.SUNSPOT);
	}

	public boolean isSetup() {
		Connection con = null;

		try {
			con = this.dsourc.getConnection();
			con.setAutoCommit(true);
			DateTime current = new DateTime(this.startDateTime);
			DateTime currentPlusMonth = current.plusMonths(1);
			Interval period = new Interval(current, currentPlusMonth);

			while (!period.isAfter(this.endDateTime)) {
				// Check for file description table and create if not there.
				String query = this.imgChk.queryTableExistsFiles(period);
				PreparedStatement tableExistsPrepStmt = con.prepareStatement(query);
				ResultSet res = tableExistsPrepStmt.executeQuery();
				if (!res.next()) {
					return false;
				}

				// Check for Image table and create if not there.
				query = this.imgChk.queryTableExistsImage(period);
				tableExistsPrepStmt = con.prepareStatement(query);
				res = tableExistsPrepStmt.executeQuery();
				if (!res.next()) {
					return false;
				}

				// Check for FullImage table and create if not there.
				query = this.imgChk.queryTableExistsImageFull(period);
				tableExistsPrepStmt = con.prepareStatement(query);
				res = tableExistsPrepStmt.executeQuery();
				if (!res.next()) {
					return false;
				}

				// Check for Parameter table and create if not there.
				query = this.imgChk.queryTableExistsParams(period);
				tableExistsPrepStmt = con.prepareStatement(query);
				res = tableExistsPrepStmt.executeQuery();
				if (!res.next()) {
					return false;
				}

				// check for header table and create if not there
				query = this.imgChk.queryTableExistsHeader(period);
				tableExistsPrepStmt = con.prepareStatement(query);
				res = tableExistsPrepStmt.executeQuery();
				if (!res.next()) {
					return false;
				}

				current = currentPlusMonth;
				currentPlusMonth = current.plusMonths(1);
				period = new Interval(current, currentPlusMonth);
			}

			for (EventType type : this.types) {
				// Check for the event table and create if not there.
				String query = this.queryTableExistsFilesString(type);
				PreparedStatement tableExistsPrepStmt = con.prepareStatement(query);
				ResultSet res = tableExistsPrepStmt.executeQuery();
				if (!res.next()) {
					return false;
				}
			}
		} catch (SQLException | UnknownEventTypeException e) {
			e.printStackTrace();
			return false;
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

		return true;
	}

	public boolean setupDatabase() throws InvalidConfigException {
		Path path = Paths.get(this.dataPath);
		System.out.println(path.toString());
		if (!Files.exists(path))
			throw new InvalidConfigException("Cannot find the data path.");

		if (!this.isSetup()) {

			TrackDBCreator trkCreator = new TrackDBCreator(this.dsourc);
			for (EventType type : this.types) {
				ArrayList<IEvent> eventList;
				IInputFileReader fileReader;
				switch (type) {
				case ACTIVE_REGION:
					fileReader = new ARFileReader(dataPath);
					break;
				case CORONAL_HOLE:
					fileReader = new CHFileReader(dataPath);
					break;
				case EMERGING_FLUX:
					fileReader = new EFFileReader(dataPath);
					break;
				case FILAMENT:
					fileReader = new FIFileReader(dataPath);
					break;
				case FLARE:
					fileReader = new FLFileReader(dataPath);
					break;
				case SIGMOID:
					fileReader = new SGFileReader(dataPath);
					break;
				case SUNSPOT:
					fileReader = new SSFileReader(dataPath);
					break;
				default:
					throw new InvalidConfigException("Unrecognized event type: " + type);
				}
				try {
					eventList = fileReader.readFile();
					trkCreator.insertEvents(eventList);
				} catch (IOException | SQLException | UnknownEventTypeException e) {
					e.printStackTrace();
					return false;
				}
			}

			ImageDBCreator imgCreator = new ImageDBCreator(this.dsourc);
			DateTime current = new DateTime(this.startDateTime);
			DateTime currentPlusMonth = current.plusMonths(1);
			Interval period = new Interval(current, currentPlusMonth);

			while (!period.isAfter(this.endDateTime)) {
				try {
					imgCreator.insertFileDescriptTables(period);
				} catch (SQLException e) {
					e.printStackTrace();
					return false;
				}
				current = currentPlusMonth;
				currentPlusMonth = current.plusMonths(1);
				period = new Interval(current, currentPlusMonth);
			}

		}
		return true;
	}

	private String queryTableExistsFilesString(EventType type) throws UnknownEventTypeException {
		switch (type) {
		case ACTIVE_REGION:
			return "SHOW TABLES LIKE 'hekevents_ar';";
		case CORONAL_HOLE:
			return "SHOW TABLES LIKE 'hekevents_ch';";
		case EMERGING_FLUX:
			return "SHOW TABLES LIKE 'hekevents_ef';";
		case FILAMENT:
			return "SHOW TABLES LIKE 'hekevents_fi';";
		case FLARE:
			return "SHOW TABLES LIKE 'hekevents_fl';";
		case SIGMOID:
			return "SHOW TABLES LIKE 'hekevents_sg';";
		case SUNSPOT:
			return "SHOW TABLES LIKE 'hekevents_ss';";
		default:
			throw new UnknownEventTypeException("Unrecognized event type: " + type);
		}
	}

}
