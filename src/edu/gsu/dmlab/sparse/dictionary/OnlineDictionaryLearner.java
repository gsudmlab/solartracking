/**
 * SolarTracking Copyright (C) 2017 Georgia State University
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.gsu.dmlab.sparse.dictionary;

import smile.math.Math;

import java.util.Random;

import edu.gsu.dmlab.exceptions.MatrixDimensionMismatch;
import edu.gsu.dmlab.exceptions.VectorDimensionMismatch;
import edu.gsu.dmlab.sparse.approximation.interfaces.ISparseVectorApproximator;
import edu.gsu.dmlab.sparse.dictionary.interfaces.IDictionaryCleaner;
import edu.gsu.dmlab.sparse.dictionary.interfaces.IDictionaryUpdater;
import edu.gsu.dmlab.sparse.dictionary.interfaces.ISparseDictionaryLearner;
import smile.math.matrix.DenseMatrix;
import smile.math.matrix.JMatrix;

/**
 * Implements the online dictionary learning algorithm of Mairal et al., 2010.
 * The algorithm can be found in "Sparse Modeling, Theory, Algorithms, and
 * Applications" by Irina Rish and Genady Ya. Grabarnik. Published by CRC Press
 * 2015, on page 172.
 * 
 * @author Dustin Kempton, Data Mining Lab, Georgia State University
 * @version {@value edu.gsu.dmlab.ConstValues#major_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#minor_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#build_ver}
 */
public class OnlineDictionaryLearner implements ISparseDictionaryLearner {

	private Random rand = new Random(0);

	private IDictionaryCleaner cleaner;
	private IDictionaryUpdater dictionaryUpdater;
	private ISparseVectorApproximator leqSolver;
	private boolean constantDictionarySize;
	private double fractionOfInputForDictionaryElements;
	private int constantNumber;
	private int batchSize;
	private double t0 = 1e-5;

	/**
	 * Constructor that specifies the number of dictionary elements to contain
	 * in the returned dictionary as a fraction of the input matrix size.
	 * 
	 * @param leqSolver
	 *            System of Linear Equations Solver used by this object.
	 * @param dictionaryUpdater
	 *            Dictionary updated used by this object.
	 * @param cleaner
	 *            Dictionary cleaner used by this object.
	 * @param fractionOfInputSize
	 *            The fraction of the input size that is wished as an output
	 *            number of dictionary elements. Must be 0.025 (2.5%) or
	 *            greater.
	 * @param batchSize
	 *            The number of elements to select from the input between
	 *            dictionary updates.
	 */
	public OnlineDictionaryLearner(ISparseVectorApproximator leqSolver, IDictionaryUpdater dictionaryUpdater,
			IDictionaryCleaner cleaner, double fractionOfInputSize, int batchSize) {
		if (leqSolver == null)
			throw new IllegalArgumentException(
					"System of Linear Equations Solver cannot be null in Online Dictionary Learner.");
		if (dictionaryUpdater == null)
			throw new IllegalArgumentException("The dictionary updater cannot be null in Dictionary Learner.");
		if (cleaner == null)
			throw new IllegalArgumentException("The dictionary cleaner cannot be null in Dictionary Learner.");
		if (batchSize < 1)
			throw new IllegalArgumentException(
					"Batch size must be at least 1  in OnlineDictionaryLearner constructor.");
		if (fractionOfInputSize < 0.025)
			throw new IllegalArgumentException(
					"fractionOfInputSize must be at least 2.5% in OnlineDictionaryLearner constructor.");

		this.dictionaryUpdater = dictionaryUpdater;
		this.fractionOfInputForDictionaryElements = fractionOfInputSize;
		this.constantDictionarySize = false;
		this.leqSolver = leqSolver;
		this.batchSize = batchSize;
		this.cleaner = cleaner;

	}

	/**
	 * Constructor that specifies the number of dictionary elements to contain
	 * in the returned dictionary.
	 * 
	 * @param leqSolver
	 *            System of Linear Equations Solver used by this object.
	 * @param dictionaryUpdater
	 *            Dictionary updated used by this object.
	 * @param cleaner
	 *            Dictionary cleaner used by this object.
	 * @param dictionaryElements
	 *            The number of dictionary elements to include in the returned
	 *            dictionary.
	 * @param batchSize
	 *            The number of elements to select from the input between
	 *            dictionary updates.
	 */
	public OnlineDictionaryLearner(ISparseVectorApproximator leqSolver, IDictionaryUpdater dictionaryUpdater,
			IDictionaryCleaner cleaner, int dictionaryElements, int batchSize) {
		if (leqSolver == null)
			throw new IllegalArgumentException(
					"System of Linear Equations Solver cannot be null in Online Dictionary Learner.");
		if (dictionaryUpdater == null)
			throw new IllegalArgumentException("The dictionary updater cannot be null in Dictionary Learner.");
		if (cleaner == null)
			throw new IllegalArgumentException("The dictionary cleaner cannot be null in Dictionary Learner.");
		if (batchSize < 1)
			throw new IllegalArgumentException(
					"Batch size must be at least 1  in OnlineDictionaryLearner constructor.");
		if (dictionaryElements < 1)
			throw new IllegalArgumentException(
					"dictionaryElements must be at least 1 in OnlineDictionaryLearner constructor.");

		this.dictionaryUpdater = dictionaryUpdater;
		this.constantDictionarySize = true;
		this.constantNumber = dictionaryElements;
		this.leqSolver = leqSolver;
		this.batchSize = batchSize;
		this.cleaner = cleaner;

	}

	@Override
	public void finalize() throws Throwable {
		try {
			this.cleaner = null;
			this.dictionaryUpdater = null;
			this.leqSolver = null;
		} finally {
			super.finalize();
		}
	}

	@Override
	public DenseMatrix train(DenseMatrix inputSamples) throws MatrixDimensionMismatch, VectorDimensionMismatch {
		int numSamples = inputSamples.ncols();

		int numColsInDict;
		if (constantDictionarySize) {
			numColsInDict = constantNumber;
		} else {
			numColsInDict = (int) Math.ceil((inputSamples.nrows() * this.fractionOfInputForDictionaryElements));
		}
		final int inputRows = inputSamples.nrows();
		final int numberIterations;

		numberIterations = inputSamples.ncols();

		// D_0 initial dictionary
		double[][] tmpDict = new double[inputRows][numColsInDict];

		// Initialize the dictionary with random values from the input data.
		for (int i = 0; i < numColsInDict; i++) {
			int idx = this.rand.nextInt(numSamples);
			for (int j = 0; j < inputRows; j++) {
				tmpDict[j][i] = inputSamples.get(j, idx);
			}
		}

		// center the columns in the dictionary to have zero mean and unit
		// variance.
		Math.normalize(tmpDict, true);
		JMatrix dictionary = new JMatrix(tmpDict);

		this.sequentialLearn(numberIterations, inputSamples, dictionary);

		return dictionary;
	}

	private void sequentialLearn(final int numberIterations, DenseMatrix inputSamples, JMatrix dictionary)
			throws MatrixDimensionMismatch, VectorDimensionMismatch {
		int numColsInDict = dictionary.ncols();
		int inputRows = inputSamples.nrows();

		JMatrix auxMatrixUOdd = new JMatrix(numColsInDict, numColsInDict, 0.0);
		JMatrix auxMatrixUEven = new JMatrix(numColsInDict, numColsInDict, 0.0);
		JMatrix auxMatrixUtmp = new JMatrix(numColsInDict, numColsInDict, 0.0);

		JMatrix auxMatrixVOdd = new JMatrix(inputRows, numColsInDict, 0.0);
		JMatrix auxMatrixVEven = new JMatrix(inputRows, numColsInDict, 0.0);
		JMatrix auxMatrixVtmp = new JMatrix(inputRows, numColsInDict, 0.0);

		JMatrix auxMatrixUOrig = new JMatrix(numColsInDict, numColsInDict, 0.0);
		for (int i = 0; i < auxMatrixUOrig.nrows(); i++)
			auxMatrixUOrig.set(i, i, this.t0);
		JMatrix auxMatrixVOrig = dictionary.copy();
		auxMatrixVOrig.mul(this.t0);

		boolean even = false;
		int[] permutationOfIdx = Math.permutate(inputSamples.ncols());

		for (int j = 0; j < numberIterations; j++) {

			// Compute the Gram Matrix D'*D on the dictionary.
			JMatrix Gm = dictionary.ata();
			this.cleaner.cleanDictionary(dictionary, inputSamples, Gm);

			// Add to the diagonal to avoid divide by zero
			for (int i = 0; i < Gm.nrows(); i++)
				Gm.add(i, i, 10e-10);

			// Process the batch with n parallel threads
			for (int i = 0; i < this.batchSize; i++) {
				int idx = permutationOfIdx[(i + (j * this.batchSize)) % permutationOfIdx.length];

				// y_i
				double[] dataCol = new double[inputRows];
				this.getCol(inputSamples, dataCol, idx);
				double dataColMean = Math.mean(dataCol);

				for (int k = 0; k < dataCol.length; k++) {
					dataCol[k] = (dataCol[k] - dataColMean);
				}

				// x_i = sparse coding computation using
				double[] sparseRep = new double[dictionary.ncols()];
				try {
					this.leqSolver.solve(dataCol, dictionary, Gm, sparseRep);
				} catch (RuntimeException | VectorDimensionMismatch | MatrixDimensionMismatch ex) {
					System.out.println(ex.getMessage());
				}

				// U_i <- U_i-1 + x_i*x_i^T
				this.rank1Update(sparseRep, auxMatrixUtmp);
				// V_i <- V_i-1 + y_i*x_i^T
				this.rank1Update(auxMatrixVtmp, dataCol, sparseRep);

				dataCol = null;
				sparseRep = null;
			}

			int epoch = (((j + 1) % inputRows) * this.batchSize) / inputRows;
			if ((even && ((epoch % 2) == 1)) || (!even && ((epoch % 2) == 0))) {
				// System.out.println("epoch");
				auxMatrixUOdd = auxMatrixUEven;
				auxMatrixUEven = new JMatrix(numColsInDict, numColsInDict, 0.0);
				auxMatrixVOdd = auxMatrixVEven;
				auxMatrixVEven = new JMatrix(inputRows, numColsInDict, 0.0);
				even = !even;
			}

			double scale = Math.max(0.95, Math.pow(j / (j + 1), -1.0));

			auxMatrixUOdd.mul(scale);
			auxMatrixUEven.mul(scale);
			auxMatrixVOdd.mul(scale);
			auxMatrixVEven.mul(scale);

			auxMatrixUEven.add(auxMatrixUtmp);
			auxMatrixUtmp = new JMatrix(numColsInDict, numColsInDict, 0.0);

			auxMatrixVEven.add(auxMatrixVtmp);
			auxMatrixVtmp = new JMatrix(inputRows, numColsInDict, 0.0);

			JMatrix auxMatrixU;
			JMatrix auxMatrixV;
			if (j * this.batchSize < 10000) {
				auxMatrixUOrig.mul(scale);
				auxMatrixVOrig.mul(scale);
				auxMatrixU = auxMatrixUOrig.copy();
				auxMatrixV = auxMatrixVOrig.copy();
			} else {
				auxMatrixU = new JMatrix(numColsInDict, numColsInDict, 0.0);
				auxMatrixV = new JMatrix(inputRows, numColsInDict, 0.0);
			}

			auxMatrixU.add(auxMatrixUOdd);
			auxMatrixU.add(auxMatrixUEven);
			auxMatrixV.add(auxMatrixVOdd);
			auxMatrixV.add(auxMatrixVEven);

			this.dictionaryUpdater.updateDictionary(dictionary, auxMatrixU, auxMatrixV);
		}

	}

	public void rank1Update(JMatrix mat, double[] vec1, double[] vec2) {
		for (int i = 0; i < mat.ncols(); i++) {
			if (vec2[i] != 0) {
				for (int j = 0; j < mat.nrows(); j++) {
					mat.add(j, i, (vec1[j] * vec2[i]));
				}
			}
		}

	}

	private void rank1Update(double[] x, JMatrix mat) {
		for (int i = 0; i < mat.ncols(); i++) {
			if (x[i] != 0) {
				for (int j = 0; j < mat.nrows(); j++) {
					mat.add(j, i, (x[i] * x[j]));
				}
			}
		}

	}

	private void getCol(DenseMatrix A, double[] vect, int j) {
		for (int i = 0; i < A.nrows(); i++) {
			vect[i] = A.get(i, j);
		}
	}

}
