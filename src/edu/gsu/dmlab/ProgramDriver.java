package edu.gsu.dmlab;

import java.io.File;
import java.util.Scanner;

import edu.gsu.dmlab.config.ConfigReader;
import edu.gsu.dmlab.datatypes.EventType;
import edu.gsu.dmlab.exceptions.InvalidConfigException;
import edu.gsu.dmlab.tracking.TrackingAll;

public class ProgramDriver {

	private static ConfigReader config;
	private static String configFile = "config" + File.separator + "tracking.cfg.xml";

	public static void main(String[] args) {
		setConfig();
		eulaPrint();
		start();
	}

	private static void setConfig() {
		try {
			config = new ConfigReader(configFile);

		} catch (InvalidConfigException e) {
			System.out.println("======================================================");
			System.out.println("Cannot configure project.");
			System.out.println("Is the config file located in the ./config directory?");
			System.out.println("\n\n");
			System.out.println("==================Stack Trace=========================");
			e.printStackTrace();
			System.out.println("======================================================");
			System.exit(1);
		}

	}

	private static int testDBSetup(Scanner sc) {
		int returnVal = 0;
		DatabaseCheckAndSetup checkAndConfig = new DatabaseCheckAndSetup(config);
		if (!checkAndConfig.isSetup()) {
			boolean done = false;
			while (!done) {
				System.out.println("Database Is Not Setup.");
				System.out.println("Do you want to setup now? (y/n)");
				if (sc.hasNext()) {
					String input = sc.nextLine();
					switch (input) {
					case "q":
						System.out.println("All done.");
						done = true;
						returnVal = -1;
						break;
					case "Y":
					case "y":
						done = true;
						returnVal = 1;
						break;
					case "N":
					case "n":
						done = true;
						returnVal = 2;
						break;
					default:
						System.out.println("------------------------------------------------------");
						System.out.println("Input was incorrect.");
						System.out.println("The options are:");
						System.out.println("y for Yes, Please setup the database.");
						System.out.println("n for No, I don't want to setup the database.");
						System.out.println("or q to quit.");
						System.out.println("------------------------------------------------------");
					}
				}
			}

			if (returnVal == 1) {
				try {
					done = checkAndConfig.setupDatabase();
				} catch (InvalidConfigException e) {
					System.out.println("------------------------------------------------------");
					System.out.println("Could not find the directory for input data.");
					System.out.println("Pleas make sure it is configured correctly in the config file.");
					System.out.println("------------------------------------------------------");
				}
				if (!done)
					returnVal = -1;
			}

		}
		return returnVal;
	}

	private static void start() {
		Scanner sc = new Scanner(System.in);
		int initVal = testDBSetup(sc);
		if (initVal == -1)
			System.exit(1);

		TrackingAll tracking = new TrackingAll(config);
		int input = 0;
		while (input > -1) {
			input = startMenu(sc);
			switch (input) {
			case 1:
				tracking.trackEvents(EventType.ACTIVE_REGION);
				break;
			case 2:
				tracking.trackEvents(EventType.CORONAL_HOLE);
				break;
			case 3:
				tracking.trackEvents(EventType.EMERGING_FLUX);
				break;
			case 4:
				tracking.trackEvents(EventType.FILAMENT);
				break;
			case 5:
				tracking.trackEvents(EventType.FLARE);
				break;
			case 6:
				tracking.trackEvents(EventType.SIGMOID);
				break;
			case 7:
				tracking.trackEvents(EventType.SUNSPOT);
				break;
			case 8:
				tracking.trackEvents(EventType.ACTIVE_REGION);
				tracking.trackEvents(EventType.CORONAL_HOLE);
				tracking.trackEvents(EventType.EMERGING_FLUX);
				tracking.trackEvents(EventType.FILAMENT);
				tracking.trackEvents(EventType.FLARE);
				tracking.trackEvents(EventType.SIGMOID);
				tracking.trackEvents(EventType.SUNSPOT);
			}
		}
		System.exit(0);

	}

	static int startMenu(Scanner sc) {
		int returnVal = 0;
		boolean done = false;
		while (!done) {
			System.out.println("======================================================");
			System.out.println("Enter an event type to track or q to quit:");
			System.out.println("------------------------------------------------------");
			System.out.println("1 for Active Region");
			System.out.println("2 for Coronal Hole");
			System.out.println("3 for Emerging Flux");
			System.out.println("4 for Filament");
			System.out.println("5 for Flare");
			System.out.println("6 for Sigmoid");
			System.out.println("7 for Sun Spot");
			System.out.println("8 for All");
			System.out.println("======================================================");

			if (sc.hasNext()) {
				String input = sc.nextLine();
				switch (input) {
				case "q":
					System.out.println("All done.");
					done = true;
					returnVal = -1;
					break;
				case "1":
				case "ar":
				case "AR":
					done = true;
					returnVal = 1;
					break;
				case "2":
				case "CH":
				case "ch":
					done = true;
					returnVal = 2;
					break;
				case "3":
				case "EF":
				case "ef":
					done = true;
					returnVal = 3;
					break;
				case "4":
				case "FI":
				case "fi":
					done = true;
					returnVal = 4;
					break;
				case "5":
				case "FL":
				case "fl":
					done = true;
					returnVal = 5;
					break;
				case "6":
				case "SG":
				case "sg":
					done = true;
					returnVal = 6;
					break;
				case "7":
				case "SS":
				case "ss":
					done = true;
					returnVal = 7;
					break;
				case "8":
					done = true;
					returnVal = 8;
					break;
				default:
					System.out.println("======================================================");
					System.out.println("Input was incorrect.");
				}
			}
		}
		return returnVal;
	}

	private static void eulaPrint() {
		System.out.println("SolarTracking Copyright (C) 2017 Georgia State University");
		System.out.println("This program comes with ABSOLUTELY NO WARRANTY.");
		System.out.println("This is free software, and you are welcome to redistribute it");
		System.out.println("under certain conditions. For details on warranty and redistibution");
		System.out.println("conditions see the LICENSE.txt that accompanies this program.");
	}
}
