/**
 * SolarTracking Copyright (C) 2017 Georgia State University
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.gsu.dmlab.util;

import edu.gsu.dmlab.geometry.Point2D;
import edu.gsu.dmlab.datatypes.Waveband;
import edu.gsu.dmlab.datatypes.interfaces.IEvent;
import edu.gsu.dmlab.datatypes.interfaces.ITrack;

import org.joda.time.DateTime;

/**
 * @author Dustin Kempton, Data Mining Lab, Georgia State University
 * @version {@value edu.gsu.dmlab.ConstValues#major_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#minor_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#build_ver}
 */
public class Utility {

	/**
	 * Constant: number of seconds in a day.
	 */
	public static final double SECONDS_TO_DAYS = 60.0 * 60.0 * 24.0;

	/**
	 * Utility function to calculate the mean movement of a track.
	 * 
	 * @param track
	 *            The track to find the mean movement of
	 * @return The mean movement vector of the input track.
	 */
	public static float[] trackMovement(ITrack track) {
		double xMovement = 0.0;
		double yMovement = 0.0;
		double totalTime = 0.0;
		int count = 0;

		IEvent event = track.getFirst();

		float[] motionNormMean = new float[2];

		for (IEvent currentEvent : track.getEvents()) {
			Point2D locationTwo = currentEvent.getLocation();
			Point2D locationOne = event.getLocation();
			xMovement += locationOne.getX() - locationTwo.getX();
			yMovement += locationOne.getY() - locationTwo.getY();

			double span;
			DateTime startSearch;
			DateTime endSearch;

			// Interval timePeriod = event.getTimePeriod();
			startSearch = currentEvent.getTimePeriod().getEnd();
			endSearch = startSearch
					.plus(currentEvent.getTimePeriod().getStartMillis() - event.getTimePeriod().getStartMillis());
			span = ((endSearch.minus(startSearch.getMillis())).getMillis() / 1000) / SECONDS_TO_DAYS;

			totalTime += span;
			event = currentEvent;
		}

		if (track.size() > 0) {
			double xMean = xMovement / count;
			double yMean = yMovement / count;
			double tMean = totalTime / count;
			float xMeanPerTime = (float) (xMean / tMean);
			float yMeanPerTime = (float) (yMean / tMean);

			motionNormMean[0] = xMeanPerTime;
			motionNormMean[1] = yMeanPerTime;
		} else {
			motionNormMean[0] = 0;
			motionNormMean[1] = 0;
		}
		return motionNormMean;
	}

	public static int convertWavebandToInt(Waveband wavelength) {
		switch (wavelength) {
		case AIA94:
			return 94;
		case AIA131:
			return 131;
		case AIA171:
			return 171;
		case AIA193:
			return 193;
		case AIA211:
			return 211;
		case AIA304:
			return 304;
		case AIA335:
			return 335;
		case AIA1600:
			return 1600;
		case AIA1700:
			return 1700;
		default:
			return 94;
		}
	}

	public static Waveband getWavebandFromInt(int wavelength) {
		switch (wavelength) {
		case 94:
			return Waveband.AIA94;
		case 131:
			return Waveband.AIA131;
		case 171:
			return Waveband.AIA171;
		case 193:
			return Waveband.AIA193;
		case 211:
			return Waveband.AIA211;
		case 304:
			return Waveband.AIA304;
		case 335:
			return Waveband.AIA335;
		case 1600:
			return Waveband.AIA1600;
		case 1700:
			return Waveband.AIA1700;
		default:
			return null;
		}
	}

}
