/**
 * SolarTracking Copyright (C) 2017 Georgia State University
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.gsu.dmlab.features;

import java.sql.SQLException;
import java.util.ArrayList;

import edu.gsu.dmlab.databases.interfaces.IFeatureDBConnection;
import edu.gsu.dmlab.databases.interfaces.IFeatureDBCreator;

import edu.gsu.dmlab.datatypes.EventType;
import edu.gsu.dmlab.datatypes.ImageDBWaveParamPair;
import edu.gsu.dmlab.datatypes.Waveband;
import edu.gsu.dmlab.features.interfaces.IFeatureSetSelector;
import edu.gsu.dmlab.features.interfaces.IStatProducer;

/**
 * FeatureSetSelector is a class that either pulls the best N features
 * (wavelength/image parameter pairs) from a database table or calculates and
 * stores the scores and then pulls the best N from the database if the
 * calculation has not already been done.
 * 
 * @author Dustin Kempton, Data Mining Lab, Georgia State University
 * @version {@value edu.gsu.dmlab.ConstValues#major_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#minor_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#build_ver}
 */
public class FeatureSetSelector implements IFeatureSetSelector {

	private IStatProducer statProducer;
	private IFeatureDBCreator dbCreator;
	private IFeatureDBConnection featDbConnect;
	private ArrayList<Waveband> wavelengths;
	private ArrayList<Integer> params;// = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
	private EventType type;

	/**
	 * Constructor for the feature selection class. A new object needs to be
	 * constructed for each event type as the data is stored separately for each
	 * event type.
	 * 
	 * @param statProducer
	 *            The object that produces the evaluation statistic for each set
	 *            of wavelength and parameter to be tested.
	 * @param dbCreator
	 *            The object that creates the table used to store the
	 *            statistics, also stores the statistics.
	 * @param featDbConnect
	 *            The object that is used to access the statistics in the
	 *            database.
	 * @param wavelengths
	 *            The list of wavelengths to evaluate.
	 * @param params
	 *            The list of parameter calculations to evaluate.
	 * @param type
	 *            The event type we are doing this for.
	 */
	public FeatureSetSelector(IStatProducer statProducer, IFeatureDBCreator dbCreator,
			IFeatureDBConnection featDbConnect, ArrayList<Waveband> wavelengths, ArrayList<Integer> params,
			EventType type) {
		if (statProducer == null)
			throw new IllegalArgumentException("IStatProducer cannot be null in FeatureSetSelector constructor.");
		if (dbCreator == null)
			throw new IllegalArgumentException("IFeatureDBCreator cannot be null in FeatureSetSelector constructor.");
		if (featDbConnect == null)
			throw new IllegalArgumentException(
					"IFeatureDBConnection cannot be null in FeatureSetSelector constructor.");
		if (wavelengths == null)
			throw new IllegalArgumentException("Wavelengths cannot be null in FeatureSetSelector constructor.");
		if (params == null)
			throw new IllegalArgumentException("Params cannot be null in FeatureSetSelector constructor.");
		if (type == null)
			throw new IllegalArgumentException("EventType cannot be null in FeatureSetSelector constructor.");

		this.statProducer = statProducer;
		this.dbCreator = dbCreator;
		this.featDbConnect = featDbConnect;
		this.wavelengths = wavelengths;
		this.params = params;
		this.type = type;
	}

	@Override
	public ImageDBWaveParamPair[] getBestFeatures(int numFeatures) {

		try {
			if (this.featDbConnect.featureTableSet(this.type)) {
				return this.featDbConnect.getBestFeatures(this.type, numFeatures);
			} else {
				ArrayList<ImageDBWaveParamPair> paramList = new ArrayList<ImageDBWaveParamPair>();

				for (Waveband wave : this.wavelengths) {
					for (int param : this.params) {
						ImageDBWaveParamPair pair = new ImageDBWaveParamPair();
						pair.parameter = param;
						pair.wavelength = wave;
						paramList.add(pair);
					}
				}

				this.dbCreator.createFeatureScoreTable(this.type);
				paramList.forEach(pair -> {
					float stat = statProducer.computeStat(new ImageDBWaveParamPair[] { pair });
					try {
						dbCreator.insertParamFeatureStatVal(type, pair, stat);
					} catch (Exception e) {
						e.printStackTrace();
					}
				});

				return this.featDbConnect.getBestFeatures(this.type, numFeatures);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
}
