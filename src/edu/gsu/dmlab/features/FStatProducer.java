/**
 * SolarTracking Copyright (C) 2017 Georgia State University
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.gsu.dmlab.features;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.IntStream;

import org.apache.commons.math3.stat.inference.OneWayAnova;

import edu.gsu.dmlab.datatypes.ImageDBWaveParamPair;
import edu.gsu.dmlab.datatypes.interfaces.IEvent;
import edu.gsu.dmlab.datatypes.interfaces.ITrack;
import edu.gsu.dmlab.features.interfaces.IStatProducer;
import edu.gsu.dmlab.imageproc.interfaces.IHistoComparator;
import edu.gsu.dmlab.imageproc.interfaces.IHistogramProducer;

/**
 * Calculates the F-Statistic for a parameter when representing a given event
 * type.
 * 
 * @author Dustin Kempton, Data Mining Lab, Georgia State University
 * @version {@value edu.gsu.dmlab.ConstValues#major_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#minor_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#build_ver}
 */
public class FStatProducer implements IStatProducer {

	private List<ITrack> tracks;
	private IHistogramProducer histoProducer;
	private IHistoComparator comparator;

	/**
	 * Constructor
	 * 
	 * @param tracks
	 *            Tracks to compute on
	 * @param histoProducer
	 *            A histogram producer to use.
	 * @param comparator
	 *            A histogram comparator to use on the histograms.
	 */
	public FStatProducer(List<ITrack> tracks, IHistogramProducer histoProducer, IHistoComparator comparator) {

		if (tracks == null)
			throw new IllegalArgumentException("Tracks cannot be null in FStat Producer constructor.");
		if (histoProducer == null)
			throw new IllegalArgumentException("HistoProducer cannot be null in FStat Producer constructor.");
		if (comparator == null)
			throw new IllegalArgumentException("Comparator cannot be null in FStat Producer constructor.");

		this.tracks = tracks;
		this.histoProducer = histoProducer;
		this.comparator = comparator;
	}

	@Override
	public float computeStat(ImageDBWaveParamPair[] dims) {
		ArrayList<double[]> comparValues = new ArrayList<double[]>();
		Random rnd = new Random(10);

		Lock loc = new ReentrantLock();
		// loop over all the tracks and process for this paramCombo
		IntStream.range(0, this.tracks.size()).parallel().forEach(i -> {
			// for (int i = 0; i < this.tracks.size(); i++) {

			// process one track
			ITrack trk = tracks.get(i);

			ArrayList<IEvent> tmpSameEvents = trk.getEvents();

			// if we have enough IEvents in the track to calculate the same
			// and
			// difference historams then we can proceed
			if (tmpSameEvents.size() >= 2) {

				// loop through each of the IEvents in the track
				for (int sameStartIdx = 0; sameStartIdx < tmpSameEvents.size() - 2; sameStartIdx++) {

					// find some random track to compare this set of IEvents
					// to
					int idx = rnd.nextInt(tracks.size());
					ArrayList<IEvent> tmpDiffEvents = tracks.get(idx).getEvents();

					// make sure there are enough IEvents in the different
					// track
					// to do the processing
					while (tmpDiffEvents.size() < 2) {
						idx = rnd.nextInt(tracks.size());
						tmpDiffEvents = tracks.get(idx).getEvents();
					}

					// get the same and different IEvents for processing
					IEvent[] sameEvents = new IEvent[2];

					for (int k = 0; k < 2; k++) {
						sameEvents[k] = tmpSameEvents.get(sameStartIdx + k);
					}
					IEvent diffEvent = tmpDiffEvents.get(rnd.nextInt(tmpDiffEvents.size() - 1));

					int[][] sameHist1 = this.histoProducer.getHist(sameEvents[0], dims, true);
					int[][] sameHist2 = this.histoProducer.getHist(sameEvents[1], dims, false);

					int[][] diffHist1 = this.histoProducer.getHist(diffEvent, dims, false);
					double sameCompVal = this.comparator.compareHists(sameHist1, sameHist2);
					double diffCompVal = this.comparator.compareHists(sameHist1, diffHist1);
					double[] tmpCompVals = new double[2];
					tmpCompVals[0] = sameCompVal;
					tmpCompVals[1] = diffCompVal;
					loc.lock();
					comparValues.add(tmpCompVals);
					loc.unlock();
				}
			}
		});

		return Float.valueOf(this.calcFstatValue(comparValues));
	}

	private float calcFstatValue(ArrayList<double[]> values) {
		OneWayAnova ow = new OneWayAnova();
		double[] sameCat = new double[values.size()];
		double[] diffCat = new double[values.size()];

		for (int i = 0; i < values.size(); i++) {
			double[] tmp = values.get(i);
			sameCat[i] = tmp[0];
			diffCat[i] = tmp[1];
		}

		ArrayList<double[]> catList = new ArrayList<double[]>();
		catList.add(sameCat);
		catList.add(diffCat);

		double fval = ow.anovaFValue(catList);

		if (Double.isNaN(fval))
			return 0;

		return (float) fval;
	}

}
