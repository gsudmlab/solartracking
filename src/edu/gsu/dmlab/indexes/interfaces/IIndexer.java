/**
 * SolarTracking Copyright (C) 2017 Georgia State University
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.gsu.dmlab.indexes.interfaces;

import edu.gsu.dmlab.datatypes.interfaces.IBaseDataType;

import org.joda.time.DateTime;
import org.joda.time.Interval;

import java.awt.Polygon;
import java.util.List;

/**
 * The base interface for indexes that store objects of IBaseDatType or derived
 * from objects that implement that data type.
 * 
 * @author Thaddeus Gholston, Data Mining Lab, Georgia State University
 * @version {@value edu.gsu.dmlab.ConstValues#major_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#minor_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#build_ver}
 */
public interface IIndexer<T extends IBaseDataType> {

	/**
	 * Gets the earliest start time in the index. That way you can check before
	 * you waste time querying the index for objects that don't exist.
	 * 
	 * @return The earliest start time in the index.
	 */
	public DateTime getFirstTime();

	/**
	 * Gets the latest end time in the index. That way you can check before you
	 * waste time querying the index for objects that don't exist.
	 * 
	 * @return The latest end time in the index.
	 */
	public DateTime getLastTime();

	/**
	 * Searches the index for any objects that intersect the query time and the
	 * query search area. This method is intended to look forward in time for
	 * these objects as is done to find the next possible detection for a given
	 * detection.
	 * 
	 * @param timePeriod
	 *            The time period to query the index with.
	 * @param searchArea
	 *            The search area to search for intersections with.
	 * @return A list of the objects in the index that intersect the query time
	 *         and the query search area.
	 */
	public List<T> search(Interval timePeriod, Polygon searchArea);

	/**
	 * Returns the list of all objects in the index.
	 * 
	 * @return A list of all the objects in the index.
	 */
	public List<T> getAll();
}