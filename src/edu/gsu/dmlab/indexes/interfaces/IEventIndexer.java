/**
 * SolarTracking Copyright (C) 2017 Georgia State University
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.gsu.dmlab.indexes.interfaces;

import edu.gsu.dmlab.datatypes.EventType;
import edu.gsu.dmlab.datatypes.interfaces.IEvent;

import org.joda.time.Interval;

/**
 * This interface is for indexes that will index IEvents based on the
 * {@link edu.gsu.dmlab.indexes.interfaces.IIndexer IIndexer} generic interface.
 * It has the added function of requiring the ability to get the expected change
 * in the number of event detections per frame for a period in the index.
 * 
 * @author Dustin Kempton, Data Mining Lab, Georgia State University
 * @version {@value edu.gsu.dmlab.ConstValues#major_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#minor_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#build_ver}
 */
public interface IEventIndexer extends IIndexer<IEvent> {
	/**
	 * Computes the expected change in the number of detections in each frame.
	 * The length of the frame is set internally but the time that this expected
	 * change is to be computed over is passed in as a parameter.
	 * 
	 * @param timePeriod
	 *            The period over which the expected change is to be computed.
	 * @return The expected change in the number of detections per frame based
	 *         on the passed in period of time.
	 */
	int getExpectedChangePerFrame(Interval timePeriod);

	/**
	 * Returns the event type that is indexed in this event indexer.
	 * 
	 * @return Event type of the events in the indexer.
	 */
	public EventType getTypeIndexed();
}