/**
 * SolarTracking Copyright (C) 2017 Georgia State University
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.gsu.dmlab.indexes;

import edu.gsu.dmlab.datatypes.EventType;
import edu.gsu.dmlab.geometry.GeometryUtilities;
import edu.gsu.dmlab.datatypes.interfaces.IBaseDataType;
import edu.gsu.dmlab.datatypes.interfaces.ITrack;
import edu.gsu.dmlab.indexes.interfaces.AbsMatIndexer;
import edu.gsu.dmlab.indexes.interfaces.ITrackIndexer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;
import java.util.stream.IntStream;

import java.awt.Polygon;
import java.awt.Rectangle;

/**
 * This class provides event indexing for ITrack objects. The indexing is based
 * on a grid of ArrayLists, where the grid represents the space over which the
 * index is valid. The ArrayLists in each location of the grid are used to sort
 * the ITrack objects that intersect each spatial coordinate based on time.
 * 
 * @author Thaddeus Gholston, Data Mining Lab, Georgia State University
 * @version {@value edu.gsu.dmlab.ConstValues#major_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#minor_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#build_ver}
 */
public class BasicTrackIndexer extends AbsMatIndexer<ITrack> implements ITrackIndexer {

	private ForkJoinPool forkJoinPool = null;

	/**
	 * Constructor, constructs a new BasicTrackindexer.
	 * 
	 * @param list
	 *            The list of tracks to index.
	 * @param regionDimension
	 *            The length for both the x and y spatial domain. For example x
	 *            will be valid for [0, regionDimension].
	 * @param regionDiv
	 *            The divisor used to down size each of the ITrack objects to
	 *            fit inside the spatial domain specified by the regionDimension
	 *            parameter.
	 * @param numThreads
	 *            The number of threads to use when processing batch. -1 for all
	 *            available > 0 for a specific number. JavaEE doesn't seem to
	 *            like more than one thread, so only use 1 for applications
	 *            built for web application servers.
	 * @throws IllegalArgumentException
	 *             When any of the passed in arguments are null, or if the
	 *             regionDimension is less than 1 same with divisor.
	 */
	public BasicTrackIndexer(List<ITrack> list, int regionDimension, int regionDiv, int numThreads)
			throws IllegalArgumentException {
		super(list, regionDimension, regionDiv);

		if (numThreads < -1 || numThreads == 0)
			throw new IllegalArgumentException("numThreads must be -1 or > 0 in Indexer constructor.");

		if (numThreads == -1) {
			this.forkJoinPool = new ForkJoinPool();
		} else {
			this.forkJoinPool = new ForkJoinPool(numThreads);
		}

		this.buildIndex();
	}

	@Override
	public void finalize() throws Throwable {
		try {
			this.forkJoinPool.shutdownNow();
			this.forkJoinPool = null;
		} finally {
			super.finalize();
		}
	}

	protected void buildIndex() {
		// add all the objects to the index
		try {
			this.forkJoinPool.submit(() -> {
				// Add each object to index
				this.objectList.parallelStream().forEach(track -> {
					this.indexTrack(track);
				});

				// Sort objects in each box in the index by the time of the
				// objects in those regions.
				IntStream.range(0, this.regionDimension * this.regionDimension).parallel().forEach(i -> {
					int x = i / this.regionDimension;
					int y = i % this.regionDimension;
					Collections.sort(this.searchSpace[x][y], IBaseDataType.baseComparator);
				});
			}).get();
		} catch (InterruptedException | ExecutionException e) {
			e.printStackTrace();
		}

	}

	private void indexTrack(ITrack track) {
		if (track.getType() == EventType.SIGMOID) {
			this.pushSGTrackIntoMatrix(track);
		} else {
			this.pushTrackIntoMatrix(track);
		}
	}

	private void pushSGTrackIntoMatrix(ITrack track) {
		Rectangle scaledBoundingBox = GeometryUtilities.scaleBoundingBox(track.getFirst().getBBox(),
				this.regionDivisor);

		IntStream.rangeClosed((int) scaledBoundingBox.getMinX(), (int) scaledBoundingBox.getMaxX()).forEach(x -> {
			IntStream.rangeClosed((int) scaledBoundingBox.getMinY(), (int) scaledBoundingBox.getMaxY()).forEach(y -> {
				if (x > -1 && x < this.regionDimension && y > -1 && y < this.regionDimension) {
					searchSpace[x][y].add(track);
				}
			});
		});
	}

	private void pushTrackIntoMatrix(ITrack track) {
		// add for forward indexing
		{
			Polygon scaledPoly = GeometryUtilities.scalePolygon(track.getFirst().getShape(), this.regionDivisor);
			Rectangle scaledBoundingBox = scaledPoly.getBounds();

			IntStream.rangeClosed((int) scaledBoundingBox.getMinX(), (int) scaledBoundingBox.getMaxX()).forEach(x -> {
				IntStream.rangeClosed((int) scaledBoundingBox.getMinY(), (int) scaledBoundingBox.getMaxY())
						.forEach(y -> {
							if (scaledPoly.intersects(x, y, 1, 1)) {
								if (x > -1 && x < this.regionDimension && y > -1 && y < this.regionDimension) {
									searchSpace[x][y].add(track);
								}
							}
						});
			});
		}
	}

	@Override
	public ArrayList<ITrack> getAll() {
		HashMap<UUID, ITrack> uniqueTracks = new HashMap<UUID, ITrack>();
		for (int i = 0; i < this.objectList.size(); i++) {
			ITrack tmpTrk = this.objectList.get(i);
			if (!uniqueTracks.containsKey(tmpTrk.getFirst().getUUID())) {
				uniqueTracks.put(tmpTrk.getFirst().getUUID(), tmpTrk);
			}
		}

		ArrayList<ITrack> returnVals = new ArrayList<ITrack>(uniqueTracks.values());
		return returnVals;
	}
}