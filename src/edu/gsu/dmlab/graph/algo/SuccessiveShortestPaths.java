/**
 * SolarTracking Copyright (C) 2017 Georgia State University
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.gsu.dmlab.graph.algo;

import java.util.ArrayList;
import java.util.List;

import edu.gsu.dmlab.datatypes.interfaces.ITrack;
import edu.gsu.dmlab.graph.Edge;
import edu.gsu.dmlab.graph.algo.interfaces.IGraphProblemSolver;
import edu.gsu.dmlab.graph.interfaces.IGraph;

import org.jgrapht.alg.BellmanFordShortestPath;

/**
 * This class is used to find the optimal flow through the passed in
 * SimpleDirectedWeightedGraph where some of the edges are negative and the
 * graph is a DAG.
 * 
 * @author Dustin Kempton, Data Mining Lab, Georgia State University
 * @version {@value edu.gsu.dmlab.ConstValues#major_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#minor_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#build_ver}
 */
public class SuccessiveShortestPaths implements IGraphProblemSolver {

	@Override
	public ArrayList<ITrack[]> solve(IGraph graph) {
		if (graph == null)
			throw new IllegalArgumentException("Graph Cannot be null.");

		boolean done = false;
		ArrayList<ITrack[]> edgesInSolution = new ArrayList<ITrack[]>();
		while (!done) {

			// find the path from source to sink
			BellmanFordShortestPath<String, Edge> bellman = new BellmanFordShortestPath<String, Edge>(graph,
					graph.getSourceName());
			List<Edge> edges = bellman.getPathEdgeList(graph.getSinkName());
			// if path exists and costs is negative process otherwise we are
			// done
			if (edges != null) {
				double cost = 0.0;
				for (Edge edge : edges)
					cost += edge.getWeight();
				if (cost <= 0) {
					for (Edge edge : edges) {
						String edgSource = graph.getEdgeSource(edge);
						String edgTarget = graph.getEdgeTarget(edge);

						// if the edge is not from source or to the sink add to
						// returned set
						if (!(edgSource.equalsIgnoreCase(graph.getSourceName())
								|| edgTarget.equalsIgnoreCase(graph.getSinkName()))) {
							ITrack[] tmpVerts = { graph.getTrackForVertex(edgSource),
									graph.getTrackForVertex(edgTarget) };
							edgesInSolution.add(tmpVerts);
						}
						graph.removeEdge(edge);
					}
				} else {
					done = true;
				}
			} else {
				done = true;
			}
		}

		return edgesInSolution;
	}

}
