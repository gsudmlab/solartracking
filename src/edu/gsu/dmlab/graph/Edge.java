/**
 * SolarTracking Copyright (C) 2017 Georgia State University
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.gsu.dmlab.graph;

import org.jgrapht.graph.DefaultWeightedEdge;

/**
 * This class is just a wrapper class so we can add weights to the edge with a
 * constructor.
 * 
 * @author Dustin Kempton, Data Mining Lab, Georgia State University
 * @version {@value edu.gsu.dmlab.ConstValues#major_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#minor_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#build_ver}
 */
public class Edge extends DefaultWeightedEdge {

	private static final long serialVersionUID = 7185657615982000843L;
	double weight;

	/**
	 * Constructor
	 * 
	 * @param weight
	 *            the weight the edge should have.
	 */
	public Edge(double weight) {
		super();
		this.weight = weight;
	}

	/**
	 * @return Returns the weight of this edge.
	 */
	@Override
	public double getWeight() {
		return this.weight;
	}

	/**
	 * @return A copy of this edge.
	 */
	@Override
	public Object clone() {
		return new Edge(this.getWeight());
	}

	/**
	 * Sets the weight of the edge.
	 * 
	 * @param weight
	 *            The weight to set on the edge.
	 */
	public void setWeight(double weight) {
		this.weight = weight;
	}
}
