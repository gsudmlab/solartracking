/**
 * SolarTracking Copyright (C) 2017 Georgia State University
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.gsu.dmlab.imageproc;

import java.util.ArrayList;

import edu.gsu.dmlab.imageproc.interfaces.IImgPatchVectorizer;
import smile.math.matrix.DenseMatrix;
import smile.math.matrix.JMatrix;
import smile.math.matrix.Matrix;

/**
 * This class extracts a set of column vectors from the input matrices. It is
 * assumed that each new matrix in the array of matrices is another parameter at
 * the same position. Therefore, each position 1,1 will be concatenated first,
 * then 2,2 and so forth until the first patch is done. Then the patch is moved
 * from having its upper left hand corner at 1,1 to having it at 1+step,1,
 * repeating this until the bottom of the are is reached. Then the patches
 * starts at the top again at 1,1+step, and so forth.
 * 
 * @author Dustin Kempton, Data Mining Lab, Georgia State University
 * @version {@value edu.gsu.dmlab.ConstValues#major_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#minor_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#build_ver}
 */
public class ImgPatchVectorizer implements IImgPatchVectorizer {

	private int step;
	private int patchSize;

	/**
	 * Constructor constructs a new ImgPatchVectorizer with the given step size.
	 * 
	 * @param step
	 *            The step size of the image patches when vectorizing.
	 * @param patchSize
	 *            The size of the patches to extract from the image.
	 */
	public ImgPatchVectorizer(int step, int patchSize) {
		if (patchSize <= 0)
			throw new IllegalArgumentException("The patch size cannot be less than 1.");
		if (step <= 0)
			throw new IllegalArgumentException("The step cannot be less than 1.");
		this.step = step;
		this.patchSize = patchSize;
	}

	@Override
	public DenseMatrix vectorize(DenseMatrix[] imageDims) {

		int numRows = imageDims[0].nrows();
		int numCols = imageDims[0].ncols();

		ArrayList<double[]> colVects = new ArrayList<double[]>();
		for (int colIdx = 0; colIdx < numCols - (this.patchSize - 1); colIdx += this.step) {
			for (int rowIdx = 0; rowIdx < numRows - (this.patchSize - 1); rowIdx += this.step) {
				double[] vect = this.extract(colIdx, rowIdx, this.patchSize, imageDims);
				colVects.add(vect);
			}
		}

		if (!colVects.isEmpty()) {
			int returnRows = colVects.get(0).length;
			int returnCols = colVects.size();
			double[][] rawStorageReturn = new double[returnRows][returnCols];
			for (int i = 0; i < returnCols; i++) {
				double[] colData = colVects.get(i);
				for (int j = 0; j < returnRows; j++) {
					rawStorageReturn[j][i] = colData[j];
				}
			}
			DenseMatrix returnMat = new JMatrix(rawStorageReturn);

			return returnMat;
		} else {
			return new JMatrix(0, 0);
		}

	}

	private double[] extract(int colIdx, int rowIdx, int patchSize, DenseMatrix[] imageDims) {
		double[] data = new double[patchSize * patchSize * imageDims.length];
		for (int col = 0; col < patchSize; col++) {
			for (int row = 0; row < patchSize; row++) {
				for (int dim = 0; dim < imageDims.length; dim++) {
					int idx = (col * patchSize * imageDims.length + (row * imageDims.length)) + dim;
					data[idx] = imageDims[dim].get(row + rowIdx, col + colIdx);
				}
			}
		}
		return data;
	}
}
