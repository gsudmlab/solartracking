/**
 * SolarTracking Copyright (C) 2017 Georgia State University
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.gsu.dmlab.imageproc;

import java.util.Arrays;

import edu.gsu.dmlab.databases.interfaces.IImageDBConnection;
import edu.gsu.dmlab.datatypes.ImageDBWaveParamPair;
import edu.gsu.dmlab.datatypes.interfaces.IEvent;
import edu.gsu.dmlab.imageproc.interfaces.IHistogramProducer;
import smile.math.matrix.DenseMatrix;
import smile.math.matrix.Matrix;

/**
 * HistogramProducer produces a 15 bin histogram for a passed in object
 * detection for each of the passed in image parameter/wavelength pairs.
 * 
 * @author Dustin Kempton, Data Mining Lab, Georgia State University
 * @version {@value edu.gsu.dmlab.ConstValues#major_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#minor_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#build_ver}
 */
public class HistogramProducer implements IHistogramProducer {

	private int histSize = 15;
	private IImageDBConnection imageDB;
	private double[] dimRange = { 0.0, 1.0 };
	private double binSize = (dimRange[1] - dimRange[0]) / (this.histSize);

	/**
	 * Constructor that takes in a connection to the image database.
	 * 
	 * @param imageDB
	 *            The image database connection used to pull the image data from
	 *            the database.
	 */
	public HistogramProducer(IImageDBConnection imageDB) {
		if (imageDB == null)
			throw new IllegalArgumentException("IImageDBConnection cannot be null in HistogramProducer constructor.");
		this.imageDB = imageDB;
	}

	@Override
	public void finalize() throws Throwable {
		try {
			this.imageDB = null;
			this.dimRange = null;
		} finally {
			super.finalize();
		}
	}

	@Override
	public int[][] getHist(IEvent event, ImageDBWaveParamPair[] dims, boolean left) {

		Matrix[] dimMatArr = this.imageDB.getImageParamForEv(event, dims, left);
		int depth = dims.length;
		int[][] returnHistos = new int[depth][];
		for (int i = 0; i < depth; i++) {
			returnHistos[i] = this.calcHisto(((DenseMatrix) dimMatArr[i]).array(), dims[i].parameter);
		}
		return returnHistos;
	}

	private int[] calcHisto(double[][] rawMatData, int dim) {
		int[] data = new int[this.histSize];
		Arrays.fill(data, 0);

		double shifVal = Math.abs(dimRange[0]);
		for (int j = 0; j < rawMatData.length; j++) {
			for (int i = 0; i < rawMatData[j].length; i++) {
				try {
					int idx = (int) (Math.abs(rawMatData[j][i] - shifVal) / binSize);
					if (idx < data.length)
						data[idx]++;
					else if (idx == data.length)
						data[idx - 1]++;
					else
						System.out.println("Out of bounds on dim (" + dim + ") value: " + rawMatData[j][i]);

				} catch (ArrayIndexOutOfBoundsException e) {
					System.out.println("Dim: " + dim);
					throw e;
				}
			}
		}
		return data;
	}
}