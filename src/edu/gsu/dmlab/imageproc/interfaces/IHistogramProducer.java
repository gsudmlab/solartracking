/**
 * SolarTracking Copyright (C) 2017 Georgia State University
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.gsu.dmlab.imageproc.interfaces;

import edu.gsu.dmlab.datatypes.ImageDBWaveParamPair;
import edu.gsu.dmlab.datatypes.interfaces.IEvent;

/**
 * Interface for methods of extracting histogram from a data source that
 * represents the passed in IEvent in the requested dimensions.
 * 
 * @author Dustin Kempton, Data Mining Lab, Georgia State University
 * @version {@value edu.gsu.dmlab.ConstValues#major_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#minor_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#build_ver}
 */
public interface IHistogramProducer {

	/**
	 * Computes the histogram of the passed in IEvent using the requested
	 * dimensions of the underlying data souce.
	 * 
	 * @param event
	 *            The event to extract a histogram of.
	 * @param params
	 *            The dimensions to use of the underlying data source.
	 * @param left
	 *            Indicates if the event is on the left side of a gap in
	 *            detections. If true the histogram is extracted from the frame
	 *            at the end of the event's valid time. Otherwise it is
	 *            extracted from the frame at the beginning of the passed in
	 *            event's valid time.
	 * @return The histogram that represents the passed in event.
	 */
	int[][] getHist(IEvent event, ImageDBWaveParamPair[] params, boolean left);
}