package edu.gsu.dmlab.geometry;

/**
 * @author Thaddeus Gholston, Data Mining Lab, Georgia State University
 * @version {@value edu.gsu.dmlab.ConstValues#major_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#minor_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#build_ver}
 */
@SuppressWarnings("serial")
public class Point2D extends java.awt.geom.Point2D.Double {

	public Point2D() {

	}

	public Point2D(double x, double y) {
		super(x, y);
	}
}
