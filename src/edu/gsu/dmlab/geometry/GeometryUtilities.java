/**
 * SolarTracking Copyright (C) 2017 Georgia State University
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.gsu.dmlab.geometry;

import java.awt.Polygon;
import java.awt.Rectangle;

/**
 * Set of geometry utilities used throughout the project.
 * 
 * @author Thaddeus Gholston, Data Mining Lab, Georgia State University
 * @version {@value edu.gsu.dmlab.ConstValues#major_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#minor_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#build_ver}
 */
public class GeometryUtilities {

	/**
	 * Scales the minimum bounding rectangle by the divisor value.
	 * 
	 * @param boundingBox
	 *            The rectangle to scale.
	 * @param divisor
	 *            The divisor used to scale.
	 * @return
	 */
	public static Rectangle scaleBoundingBox(Rectangle boundingBox, int divisor) {
		double x, y, width, height;
		x = boundingBox.getMinX() / divisor;
		y = boundingBox.getMinY() / divisor;
		width = boundingBox.getWidth() / divisor;
		height = boundingBox.getHeight() / divisor;
		Rectangle scaledBoundBox = new Rectangle();
		scaledBoundBox.setFrame(x, y, width, height);
		return scaledBoundBox;
	}

	/**
	 * Scales a polygon by the divisor value.
	 * 
	 * @param poly
	 *            The polygon to scale.
	 * @param divisor
	 *            The divisor used to scale.
	 * @return
	 */
	public static Polygon scalePolygon(Polygon poly, int divisor) {
		int[] xArr = poly.xpoints;
		int[] yArr = poly.ypoints;
		int[] scaledXArr = new int[xArr.length];
		int[] scaledYArr = new int[yArr.length];
		for (int i = 0; i < xArr.length; i++) {
			scaledXArr[i] = xArr[i] / divisor;
			scaledYArr[i] = yArr[i] / divisor;
		}

		Polygon scaledPoly = new Polygon(scaledXArr, scaledYArr, scaledXArr.length);
		return scaledPoly;
	}

}
