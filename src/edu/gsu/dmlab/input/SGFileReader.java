/**
 * SolarTracking Copyright (C) 2017 Georgia State University
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.gsu.dmlab.input;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import org.joda.time.Interval;

import edu.gsu.dmlab.datatypes.SGEvent;
import edu.gsu.dmlab.datatypes.interfaces.IEvent;
import edu.gsu.dmlab.input.interfaces.IInputFileReader;

/**
 * Classes that reads the input file for the sigmoid event type. This data file
 * comes from [Supporting Data: A large-scale dataset of solar event reports
 * from automated feature recognition
 * modules](https://doi.org/10.5281/zenodo.48187)
 * 
 * @author Dustin Kempton, Data Mining Lab, Georgia State University
 * @version {@value edu.gsu.dmlab.ConstValues#major_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#minor_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#build_ver}
 */
public class SGFileReader implements IInputFileReader {

	private String baseDir;
	private int count = 0;

	/**
	 * Constructor that takes in the base directory where to look for the file.
	 * 
	 * @param baseDir
	 *            The base directory where to look for the file.
	 */
	public SGFileReader(String baseDir) {
		this.baseDir = baseDir;
	}

	@Override
	public ArrayList<IEvent> readFile() throws IOException {
		ArrayList<IEvent> results = new ArrayList<IEvent>();
		String fileName = this.baseDir + File.separator + "SG.txt";

		DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.ENGLISH);

		try (BufferedReader br = Files.newBufferedReader(Paths.get(fileName))) {
			br.lines().skip(1).forEach(line -> {
				try {

					String[] splitLine = line.split("\t");
					// String event_type = splitLine[0];
					String event_starttime = splitLine[1];
					Date start = df.parse(event_starttime);
					// String event_endtime = splitLine[2];
					// Date end = df.parse(event_endtime);
					Interval period = new Interval(start.getTime(), start.getTime() + 1000 * 5300);
					String obs_observatory = splitLine[3];
					String obs_instrument = splitLine[4];
					String obs_channelid = splitLine[5];
					String hpc_coord = splitLine[6];
					String hpc_bbox = splitLine[7];
					String hpc_boundcc = splitLine[8];
					String ar_noaanum = splitLine[9];
					String sg_shape = splitLine[10];
					String sg_aspectratio = splitLine[11];
					String frm_versionnumber = splitLine[12];

					SGEvent event = new SGEvent(++count, period, obs_observatory, obs_instrument, obs_channelid,
							hpc_bbox, hpc_coord, hpc_boundcc, this.parseInt(ar_noaanum), sg_shape,
							this.parseDouble(sg_aspectratio), this.parseDouble(frm_versionnumber));
					results.add(event);
				} catch (Exception e) {
					e.printStackTrace();
				}
			});

		}

		return results;
	}

	private double parseDouble(String val) {
		double retVal;
		try {
			retVal = Double.parseDouble(val);
		} catch (Exception e) {
			retVal = Double.NaN;
		}
		return retVal;
	}

	private int parseInt(String val) {
		int retVal;
		try {
			retVal = Integer.parseInt(val);
		} catch (Exception e) {
			retVal = Integer.MIN_VALUE;
		}
		return retVal;
	}
}
