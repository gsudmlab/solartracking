/**
 * SolarTracking Copyright (C) 2017 Georgia State University
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.gsu.dmlab.input;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import org.joda.time.Interval;

import edu.gsu.dmlab.datatypes.CHEvent;
import edu.gsu.dmlab.datatypes.interfaces.IEvent;
import edu.gsu.dmlab.input.interfaces.IInputFileReader;

/**
 * Classes that reads the input file for the coronal hole event type. This data
 * file comes from [Supporting Data: A large-scale dataset of solar event
 * reports from automated feature recognition
 * modules](https://doi.org/10.5281/zenodo.48187)
 * 
 * @author Dustin Kempton, Data Mining Lab, Georgia State University
 * @version {@value edu.gsu.dmlab.ConstValues#major_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#minor_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#build_ver}
 */
public class CHFileReader implements IInputFileReader {

	private String baseDir;
	private int count = 0;

	/**
	 * Constructor that takes in the base directory where to look for the file.
	 * 
	 * @param baseDir
	 *            The base directory where to look for the file.
	 */
	public CHFileReader(String baseDir) {
		this.baseDir = baseDir;
	}

	@Override
	public ArrayList<IEvent> readFile() throws IOException {
		ArrayList<IEvent> results = new ArrayList<IEvent>();
		String fileName = this.baseDir + File.separator + "CH.txt";

		DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.ENGLISH);

		try (BufferedReader br = Files.newBufferedReader(Paths.get(fileName))) {
			br.lines().skip(1).forEach(line -> {
				try {

					String[] splitLine = line.split("\t");
					// String event_type = splitLine[0];
					String event_starttime = splitLine[1];
					Date start = df.parse(event_starttime);
					// String event_endtime = splitLine[2];
					// Date end = df.parse(event_endtime);
					Interval period = new Interval(start.getTime(), start.getTime() + 1000 * 4 * 60 * 60);
					String obs_observatory = splitLine[3];
					String obs_instrument = splitLine[4];
					String obs_channelid = splitLine[5];
					String hpc_coord = splitLine[6];
					String hpc_bbox = splitLine[7];
					String hpc_boundcc = splitLine[8];
					String intensmin = splitLine[9];
					String intensmax = splitLine[10];
					String intensmean = splitLine[11];
					String intensmedian = splitLine[12];
					String intensvar = splitLine[13];
					String intensskew = splitLine[14];
					String intenskurt = splitLine[15];
					String intenstotal = splitLine[16];
					String intensunit = splitLine[17];
					String area_atdiskcenter = splitLine[18];
					String area_atdiskcenteruncert = splitLine[19];
					String area_raw = splitLine[20];
					String area_uncert = splitLine[21];
					String area_unit = splitLine[22];
					String event_npixels = splitLine[23];
					String event_pixelunit = splitLine[24];
					String frm_versionnumber = splitLine[25];

					CHEvent event = new CHEvent(count++, period, obs_observatory, obs_instrument, obs_channelid,
							hpc_bbox, hpc_coord, hpc_boundcc, this.parseDouble(intensmin), this.parseDouble(intensmax),
							this.parseDouble(intensmean), this.parseDouble(intensmedian), this.parseDouble(intensvar),
							this.parseDouble(intensskew), this.parseDouble(intenskurt), this.parseDouble(intenstotal),
							intensunit, this.parseDouble(area_atdiskcenter), this.parseDouble(area_atdiskcenteruncert),
							this.parseDouble(area_raw), this.parseDouble(area_uncert), area_unit,
							this.parseDouble(event_npixels), event_pixelunit, this.parseDouble(frm_versionnumber));
					results.add(event);
				} catch (Exception e) {
					e.printStackTrace();
				}
			});

		}

		return results;
	}

	private double parseDouble(String val) {
		double retVal;
		try {
			retVal = Double.parseDouble(val);
		} catch (Exception e) {
			retVal = Double.NaN;
		}
		return retVal;
	}

//	private int parseSpokaId(String val) {
//		// String val2 = "SPoCA_v1.0_AR_0000000006";
//		int idx = val.lastIndexOf('_');
//		String val2 = val.substring(idx + 1);
//		return Integer.valueOf(val2);
//	}

}
