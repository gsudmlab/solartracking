/**
 * SolarTracking Copyright (C) 2017 Georgia State University
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.gsu.dmlab.input;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import org.joda.time.Interval;

import edu.gsu.dmlab.datatypes.FIEvent;
import edu.gsu.dmlab.datatypes.interfaces.IEvent;
import edu.gsu.dmlab.input.interfaces.IInputFileReader;

/**
 * Classes that reads the input file for the filament event type. This data file
 * comes from [Supporting Data: A large-scale dataset of solar event reports
 * from automated feature recognition
 * modules](https://doi.org/10.5281/zenodo.48187)
 * 
 * @author Dustin Kempton, Data Mining Lab, Georgia State University
 * @version {@value edu.gsu.dmlab.ConstValues#major_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#minor_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#build_ver}
 */
public class FIFileReader implements IInputFileReader {

	private String baseDir;
	private int count = 0;

	/**
	 * Constructor that takes in the base directory where to look for the file.
	 * 
	 * @param baseDir
	 *            The base directory where to look for the file.
	 */
	public FIFileReader(String baseDir) {
		this.baseDir = baseDir;
	}

	@Override
	public ArrayList<IEvent> readFile() throws IOException {
		ArrayList<IEvent> results = new ArrayList<IEvent>();
		String fileName = this.baseDir + File.separator + "FI.txt";

		DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.ENGLISH);

		try (BufferedReader br = Files.newBufferedReader(Paths.get(fileName))) {
			br.lines().skip(1).forEach(line -> {
				try {

					String[] splitLine = line.split("\t");
					// String event_type = splitLine[0];
					String event_starttime = splitLine[1];
					Date start = df.parse(event_starttime);
					// String event_endtime = splitLine[2];
					// Date end = df.parse(event_endtime);
					Interval period = new Interval(start.getTime(), start.getTime() + 1000 * 12 * 60 * 60);
					String obs_observatory = splitLine[3];
					String obs_instrument = splitLine[4];
					String obs_channelid = splitLine[5];
					String hpc_coord = splitLine[6];
					String hpc_bbox = splitLine[7];
					String hpc_boundcc = splitLine[8];
					String area_raw = splitLine[9];
					String area_unit = splitLine[10];
					String event_npixels = splitLine[11];
					String event_pixelunit = splitLine[12];
					String fi_length = splitLine[13];
					String fi_lengthunit = splitLine[14];
					String fi_tilt = splitLine[15];
					String fi_barbstot = splitLine[16];
					String fi_barbsr = splitLine[17];
					String fi_barbsl = splitLine[18];
					String fi_chirality = splitLine[19];
					String fi_barbsstartc1 = splitLine[20];
					String fi_barbsstartc2 = splitLine[21];
					String fi_barbsendc1 = splitLine[22];
					String fi_barbsendc2 = splitLine[23];
					String frm_versionnumber = splitLine[24];
					//String kb_archivid = splitLine[26];

					FIEvent event = new FIEvent(++count, period, obs_observatory, obs_instrument, obs_channelid,
							hpc_bbox, hpc_coord, hpc_boundcc, Double.valueOf(area_raw), area_unit,
							Double.valueOf(event_npixels), event_pixelunit, Double.valueOf(fi_length), fi_lengthunit,
							Double.valueOf(fi_tilt), Double.valueOf(fi_barbstot), Double.valueOf(fi_barbsr),
							Double.valueOf(fi_barbsl), Double.valueOf(fi_chirality), fi_barbsstartc1, fi_barbsstartc2,
							fi_barbsendc1, fi_barbsendc2, Double.valueOf(frm_versionnumber));
					results.add(event);
				} catch (Exception e) {
					e.printStackTrace();
				}
			});

		}

		return results;
	}

}
