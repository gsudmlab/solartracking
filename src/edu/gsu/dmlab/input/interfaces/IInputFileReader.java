/**
 * SolarTracking Copyright (C) 2017 Georgia State University
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.gsu.dmlab.input.interfaces;

import java.io.IOException;
import java.util.ArrayList;

import edu.gsu.dmlab.datatypes.interfaces.IEvent;

/**
 * Interface for classes that read the input files for event types.
 * 
 * @author Dustin Kempton, Data Mining Lab, Georgia State University
 * @version {@value edu.gsu.dmlab.ConstValues#major_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#minor_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#build_ver}
 */
public interface IInputFileReader {

	/**
	 * Method to read the input file of the particular event type.
	 * 
	 * @return A list of all the IEvent objects constructed from the file.
	 * @throws IOException
	 *             If file is improperly formated or not found.
	 */
	public ArrayList<IEvent> readFile() throws IOException;
}
