/**
 * SolarTracking Copyright (C) 2017 Georgia State University
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.gsu.dmlab.exceptions;

public class ExternalDatasourceException extends RuntimeException {

	public ExternalDatasourceException(String message, Throwable cause) {
		super(message, cause);
	}

	public ExternalDatasourceException(String message) {
		super(message);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 60399550131827095L;

}
