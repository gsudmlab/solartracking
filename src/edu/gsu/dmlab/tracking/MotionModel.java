/**
 * SolarTracking Copyright (C) 2017 Georgia State University
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.gsu.dmlab.tracking;

import edu.gsu.dmlab.datatypes.interfaces.IEvent;
import edu.gsu.dmlab.datatypes.interfaces.ITrack;
import edu.gsu.dmlab.geometry.Point2D;
import edu.gsu.dmlab.tracking.interfaces.IMotionModel;

/**
 * MotionModel does a comparison of two track fragments and gives a 0-1
 * likelihood value of being the same tracked object based on their mean
 * movement vectors.
 * 
 * @author Dustin Kempton, Data Mining Lab, Georgia State University
 * @version {@value edu.gsu.dmlab.ConstValues#major_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#minor_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#build_ver}
 */
public class MotionModel implements IMotionModel {

	@Override
	public double calcProbMotion(ITrack leftTrack, ITrack rightTrack) {
		if (leftTrack == null)
			throw new IllegalArgumentException("Left Track cannot be null.");
		if (rightTrack == null)
			throw new IllegalArgumentException("Right Track cannot be null.");

		double[] leftMotion = this.trackNormalizedMeanMovement(leftTrack);
		double[] rightMotion = this.trackNormalizedMeanMovement(rightTrack);

		double xdiff = leftMotion[0] - rightMotion[0];
		double ydiff = leftMotion[1] - rightMotion[1];

		double val = (xdiff * xdiff) + (ydiff * ydiff);
		val = Math.sqrt(val);

		double prob = 1.0 - (0.5 * val);
		return prob;
	}

	private double[] trackNormalizedMeanMovement(ITrack track) {
		double xMovement = 0;
		double yMovement = 0;
		IEvent tmp = track.getFirst();
		int count = 0;
		while (tmp.getNext() != null) {
			IEvent tmp2 = tmp.getNext();
			Point2D tmp2loc = tmp2.getLocation();
			Point2D tmpLoc = tmp.getLocation();
			xMovement += tmp2loc.getX() - tmpLoc.getX();
			yMovement += tmp2loc.getY() - tmpLoc.getY();
			tmp = tmp2;
			count++;
		}
		double[] motionNormMean = new double[2];
		if (count > 0) {
			// average the movement
			double xMean = xMovement / count;
			double yMean = yMovement / count;

			// Nor normalize the movement
			double val = (xMean * xMean) + (yMean * yMean);
			val = Math.sqrt(val);

			// Store in the array
			motionNormMean[0] = xMean / val;
			motionNormMean[1] = yMean / val;
		} else {
			motionNormMean[0] = 0;
			motionNormMean[1] = 0;
		}
		return motionNormMean;
	}
}
