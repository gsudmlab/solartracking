/**
 * SolarTracking Copyright (C) 2017 Georgia State University
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.gsu.dmlab.tracking.appearance.interfaces;

import java.awt.Rectangle;

import smile.math.matrix.SparseMatrix;

/**
 * Interface for classes used to create sparse histograms of events.
 * 
 * @author Dustin Kempton, Data Mining Lab, Georgia State University
 * @version {@value edu.gsu.dmlab.ConstValues#major_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#minor_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#build_ver}
 */
public interface ISparseHistoCreator {

	/**
	 * Returns a histogram representing the rows of the sparse input matrix.
	 * 
	 * @param alpha
	 *            The sparse matrix representing the area of interest to create
	 *            the histogram from the rows.
	 * @param bbox
	 *            The area that is represented by alpha.
	 * @return The histogram of the input matrix.
	 */
	public double[] createTargetHisto(SparseMatrix alpha, Rectangle bbox);

	/**
	 * Returns a histogram representing the rows of the sparse input matrix.
	 * 
	 * @param alpha
	 *            The sparse matrix representing the area of interest to create
	 *            the histogram from the rows.
	 * @param originalBBox
	 *            The original object area used to adjust weights by how much
	 *            the candidate needs to scale to match the original.
	 * @param candidateBBox
	 *            The candidate area that is represented by alpha.
	 * @return The histogram of the input matrix.
	 */
	public double[] createCandidateHisto(SparseMatrix alpha, Rectangle originalBBox, Rectangle candidateBBox);
}
