/**
 * SolarTracking Copyright (C) 2017 Georgia State University
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.gsu.dmlab.tracking;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import edu.gsu.dmlab.datatypes.interfaces.IEvent;
import edu.gsu.dmlab.datatypes.interfaces.ITrack;
import edu.gsu.dmlab.factory.interfaces.IGraphProblemFactory;
import edu.gsu.dmlab.graph.algo.interfaces.IGraphProblemSolver;
import edu.gsu.dmlab.graph.interfaces.IGraph;
import edu.gsu.dmlab.tracking.interfaces.IAssociationProblem;
import edu.gsu.dmlab.tracking.stages.interfaces.IEdgeWeightCalculator;

/**
 * This class is the graph used to find the optimal multi-commodity flow and
 * then use the results to associate the tracks into longer tracks.
 * 
 * @author Dustin Kempton, Data Mining Lab, Georgia State University
 * @version {@value edu.gsu.dmlab.ConstValues#major_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#minor_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#build_ver}
 */
public class GraphAssociationProblem implements IAssociationProblem {

	private IGraphProblemFactory factory;
	private IEdgeWeightCalculator weightCalculator;
	private List<ITrack> tracks;
	private IGraph grph;
	private boolean solved = false;

	/**
	 * Constructor constructs a new object.
	 * 
	 * @param tracks
	 *            The tracks to solve the association problem on.
	 * @param factory
	 *            A factory for creating stuff that this object needs.
	 * @param weightCalculator
	 *            The weight calculator used to calculate the weight on each of
	 *            the edges in the problem.
	 */
	public GraphAssociationProblem(List<ITrack> tracks, IGraphProblemFactory factory,
			IEdgeWeightCalculator weightCalculator) {

		if (tracks == null)
			throw new IllegalArgumentException("Tracks cannot be null.");
		if (factory == null)
			throw new IllegalArgumentException("Factory cannot be null.");
		if (weightCalculator == null)
			throw new IllegalArgumentException("Weight Calculator cannot be null.");

		this.tracks = tracks;
		this.factory = factory;
		this.weightCalculator = weightCalculator;

		this.grph = this.factory.getGraph(this.weightCalculator);
		this.buildGraph();
	}

	private void buildGraph() {
		for (ITrack trk : this.tracks) {
			this.grph.addTrackToGraph(trk);
		}
	}

	@Override
	public void addAssociationPossibility(ITrack from, ITrack to) {
		this.grph.addAssociationPossibility(from, to);

		if (this.solved == true)
			this.solved = false;
	}

	@Override
	public boolean solve() {

		if (this.grph.edgesOf(this.grph.getSourceName()).size() > 0) {
			IGraphProblemSolver solver = this.factory.getGraphSolver();
			ArrayList<ITrack[]> edgesList = solver.solve(grph);

			// The solver should have returned the list of tracks that the
			// solution traverses.
			for (int i = 0; i < edgesList.size(); i++) {
				ITrack[] edges = edgesList.get(i);
				// some of the edges are between the observation edges, just
				// ignore those.
				if (edges[0].getUUID() != edges[1].getUUID()) {
					// We only get here on the association edges. Link the two
					// tracks into one and move on.
					ITrack leftTrack = edges[0];
					ITrack rightTrack = edges[1];
					IEvent leftEvent = leftTrack.getLast();
					IEvent rightEvent = rightTrack.getFirst();
					leftEvent.setNext(rightEvent);
					rightEvent.setPrevious(leftEvent);
				}
			}
		}
		this.solved = true;
		return true;
	}

	@Override
	public ArrayList<ITrack> getTrackLinked() {
		// If solve wasn't called, then call it.
		if (this.solved == false) {
			this.solve();
		}

		// After solving, some of the track pointers are pointing to the same
		// linked list of events. We just want to prune the duplicates here.
		HashMap<UUID, ITrack> uniqueTracks = new HashMap<UUID, ITrack>();
		for (int i = 0; i < this.tracks.size(); i++) {
			ITrack tmpTrk = this.tracks.get(i);
			if (!uniqueTracks.containsKey(tmpTrk.getFirst().getUUID())) {
				uniqueTracks.put(tmpTrk.getFirst().getUUID(), tmpTrk);
			}
		}

		ArrayList<ITrack> returnVals = new ArrayList<ITrack>(uniqueTracks.values());
		return returnVals;
	}

}
