/**
 * SolarTracking Copyright (C) 2017 Georgia State University
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.gsu.dmlab.tracking;

import edu.gsu.dmlab.config.ConfigReader;
import edu.gsu.dmlab.config.EventConfig;
import edu.gsu.dmlab.databases.TrackDBConnection;
import edu.gsu.dmlab.databases.interfaces.ITrackDBConnection;
import edu.gsu.dmlab.datatypes.EventType;
import edu.gsu.dmlab.datatypes.Track;
import edu.gsu.dmlab.datatypes.interfaces.IEvent;
import edu.gsu.dmlab.datatypes.interfaces.ITrack;
import edu.gsu.dmlab.exceptions.InvalidConfigException;
import edu.gsu.dmlab.factory.SequentialBasicIndexFactory;
import edu.gsu.dmlab.factory.EventTrackingFactory;
import edu.gsu.dmlab.factory.interfaces.IEventTrackingFactory;
import edu.gsu.dmlab.factory.interfaces.IIndexFactory;
import edu.gsu.dmlab.indexes.interfaces.IEventIndexer;
import edu.gsu.dmlab.indexes.interfaces.ITrackIndexer;
import edu.gsu.dmlab.tracking.stages.interfaces.IProcessingStage;

import java.io.File;
import java.util.List;

import org.joda.time.Duration;

/**
 * This class is the core of the Iterative Tracking algorithm. It constructs the
 * config reader, database connections, and various factories needed to proceed
 * with the tracking of solar events.
 * 
 * @author Dustin Kempton, Data Mining Lab, Georgia State University
 * @version {@value edu.gsu.dmlab.ConstValues#major_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#minor_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#build_ver}
 */
public class TrackingAll {

	ITrackDBConnection trkdb;
	ConfigReader config;
	int expId;

	/**
	 * Constructor that takes in the config file reader.
	 *
	 * @param config
	 *            The config file reader.
	 *
	 */
	public TrackingAll(ConfigReader config) {
		this.config = config;
		this.expId = this.config.getExpId();
		this.trkdb = new TrackDBConnection(this.config.getTrackDBSource(), this.config.getStartDatetime(),
				this.config.getEndDatetime());
	}

	/**
	 * Method called to track a specific event type.
	 * 
	 * @param type
	 *            The type to track.
	 */
	public void trackEvents(EventType type) {
		IEventTrackingFactory factory;
		IIndexFactory idxFactory;

		EventConfig evConfig = this.config.getEventConfig(type);

		idxFactory = new SequentialBasicIndexFactory(this.config.getRegionDim(), this.config.getRegionDiv(),
				new Duration(evConfig.getTimeSpan() * 1000));
		factory = new EventTrackingFactory(config, type);

		List<IEvent> events = this.trkdb.getAllEvents(type);
		System.out.println("NumEvents: " + events.size());
		IEventIndexer eventIndexer = idxFactory.getEventIndexer(events);
		IProcessingStage stage4;
		{
			IProcessingStage stage3;
			{
				IProcessingStage stage2;
				{
					IProcessingStage stage1 = factory.getStage1(eventIndexer);
					List<ITrack> firstStageResults = stage1.process();
					System.out.println("Stage 1: " + firstStageResults.size());
					// this is to force the calculation of the best
					// features given the input from stage 1 if it
					// isn't in the database already.
					factory.getAppearanceModel(firstStageResults);
					firstStageResults.clear();
					// this is to break up the tracks again.
					for (IEvent ev : events) {
						ev.setNext(null);
						ev.setPrevious(null);
					}

					// create tracks because that is what stage 2 uses as input.
					// we are eliminating stage 1 for anything but getting
					// features so we make stage 2 have all the events as their
					// own tracklet.
					for (IEvent ev : events)
						firstStageResults.add(new Track(ev));

					System.out
							.println("End Feature Selection Number of Events Processing: " + firstStageResults.size());
					ITrackIndexer firstStageResultIdxr = idxFactory.getTrackIndexer(firstStageResults);
					stage2 = factory.getStage2(eventIndexer, firstStageResultIdxr, 1);
				}
				List<ITrack> secondStageResults = stage2.process();
				System.out.println("End Stage2: " + secondStageResults.size());
				ITrackIndexer secondStageResultIdxr = idxFactory.getTrackIndexer(secondStageResults);
				stage3 = factory.getStage3(eventIndexer, secondStageResultIdxr, 3);
			}
			List<ITrack> upperStageResults = stage3.process();
			System.out.println("End Stage3: " + upperStageResults.size());
			stage3 = null;
			int skip = 4;
			do {
				ITrackIndexer upperStageResultIdxr = idxFactory.getTrackIndexer(upperStageResults);
				stage4 = factory.getStage3(eventIndexer, upperStageResultIdxr, skip++);
				upperStageResults = stage4.process();
				System.out.println(
						"End Upper Stage With Skip " + (skip - 1) + " and Tracks: " + upperStageResults.size());
			} while (skip < 7);

			this.trkdb.insertTracks(upperStageResults, expId);
		}
	}

}
