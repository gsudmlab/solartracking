/**
 * SolarTracking Copyright (C) 2017 Georgia State University
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.gsu.dmlab.tracking.interfaces;

import java.util.ArrayList;

import edu.gsu.dmlab.datatypes.interfaces.ITrack;

/**
 * This is the interface of objects that solve the association problem of
 * tracks.
 * 
 * @author Dustin Kempton, Data Mining Lab, Georgia State University
 * @version {@value edu.gsu.dmlab.ConstValues#major_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#minor_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#build_ver}
 */
public interface IAssociationProblem {
	/**
	 * Returns the tracks from the solved association problem.
	 * 
	 * @return The tracks that were the result of the association problem.
	 */
	public ArrayList<ITrack> getTrackLinked();

	/**
	 * Adds a possible path to the association problem.
	 * 
	 * @param from
	 *            The ITrack the path starts from.
	 * @param to
	 *            The ITrack the path ends at.
	 * 
	 */
	public void addAssociationPossibility(ITrack from, ITrack to);

	/**
	 * Solves the association problem
	 * 
	 * @return True if the problem was solved.
	 */
	public boolean solve();
}
