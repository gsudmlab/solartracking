/**
 * SolarTracking Copyright (C) 2017 Georgia State University
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.gsu.dmlab.tracking.stages;

import edu.gsu.dmlab.datatypes.interfaces.IEvent;
import edu.gsu.dmlab.datatypes.interfaces.ITrack;
import edu.gsu.dmlab.factory.interfaces.IEventTrackingFactory;
import edu.gsu.dmlab.indexes.interfaces.IEventIndexer;
import edu.gsu.dmlab.indexes.interfaces.ITrackIndexer;
import edu.gsu.dmlab.tracking.interfaces.IAssociationProblem;
import edu.gsu.dmlab.tracking.stages.interfaces.IProcessingStage;
import edu.gsu.dmlab.util.Utility;
import edu.gsu.dmlab.util.interfaces.ISearchAreaProducer;

import org.joda.time.Interval;
import org.joda.time.Seconds;

import java.awt.Polygon;
import java.awt.Rectangle;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;
import java.util.stream.IntStream;

/**
 * Is the base of the second and third stages in the iterative tracking
 * algorithm of <a href="http://dx.doi.org/10.1016/j.ascom.2015.10.005">Kempton
 * et al.</a>
 * 
 * @author Thaddeus Gholston, Data Mining Lab, Georgia State University
 * @version {@value edu.gsu.dmlab.ConstValues#major_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#minor_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#build_ver}
 */
public class BaseUpperStage implements IProcessingStage {

	private ITrackIndexer tracksIdxr;
	private IEventIndexer evntIdxr;
	private ISearchAreaProducer predictor;
	private IEventTrackingFactory factory;
	private ForkJoinPool forkJoinPool = null;

	protected int maxFrameSkip;
	private int stage;

	/**
	 * Constructor
	 * 
	 * @param predictor
	 *            Used for predicting the location of an event given its current
	 *            location and an elapsed time period.
	 * @param factory
	 *            Factory used to create new objects needed to process the
	 *            tracks at this stage.
	 * @param tracksIdxr
	 *            Indexer of the current tracks to process in this stage.
	 * @param evntIdxr
	 *            Indexer of all events taken as input into the program at the
	 *            beginning. This is mostly used to determine the observation
	 *            probability in the association problem.
	 * @param maxFrameSkip
	 *            The maximum number of skipped frames allowed between a
	 *            detection and its possible association match.
	 * @param stage
	 *            The stage that this base is the base of. It tells the
	 *            association problem's edge weight calculator how to calculate
	 *            the edge weights as stage 2 is different than 3+.
	 * @param numThreads
	 *            The number of threads to use when processing batch. -1 for all
	 *            available > 0 for a specific number.
	 */
	public BaseUpperStage(ISearchAreaProducer predictor, IEventTrackingFactory factory, ITrackIndexer tracksIdxr,
			IEventIndexer evntIdxr, int maxFrameSkip, int stage, int numThreads) {

		if (predictor == null)
			throw new IllegalArgumentException("Search Area Producer cannot be null.");
		if (factory == null)
			throw new IllegalArgumentException("Event Tracking Factory cannot be null.");
		if (tracksIdxr == null)
			throw new IllegalArgumentException("Track Indexer cannot be null.");
		if (evntIdxr == null)
			throw new IllegalArgumentException("Event Indexer cannot be null.");
		if (maxFrameSkip < 0)
			throw new IllegalArgumentException("maxFameSkip cannot be less than zero.");
		if (stage < 1)
			throw new IllegalArgumentException("stage cannot be less than one.");
		if (numThreads < -1 || numThreads == 0)
			throw new IllegalArgumentException("numThreads must be -1 or > 0 in BaseUpperStage constructor.");

		this.predictor = predictor;
		this.tracksIdxr = tracksIdxr;
		this.evntIdxr = evntIdxr;
		this.maxFrameSkip = maxFrameSkip;
		this.factory = factory;
		this.stage = stage;

		if (numThreads == -1) {
			this.forkJoinPool = new ForkJoinPool();
		} else {
			this.forkJoinPool = new ForkJoinPool(numThreads);
		}

	}

	@Override
	public void finalize() throws Throwable {
		try {
			this.forkJoinPool.shutdownNow();
			this.forkJoinPool = null;

			this.tracksIdxr = null;
			this.evntIdxr = null;
			this.predictor = null;
			this.factory = null;
		} finally {
			super.finalize();
		}
	}

	@Override
	public List<ITrack> process() {

		List<ITrack> vectOfTracks = this.tracksIdxr.getAll();

		if (vectOfTracks.size() > 0) {
			IAssociationProblem graph = this.factory.getAssociationProblem(vectOfTracks, evntIdxr, this.stage);

			// for each track that we got back in the list we will find the
			// potential matches for that track and link it to the one with the
			// highest probability of being a match.
			try {
				this.forkJoinPool.submit(() -> {
					IntStream.range(0, vectOfTracks.size()).parallel().forEach(i -> {

						// we get the event associated with the end of our
						// current track as that is the last frame of the track
						// and has position information and image information
						// associated with it.
						ITrack currentTrack = vectOfTracks.get(i);
						IEvent currentEvent = currentTrack.getLast();

						double span;
						Polygon searchArea;
						HashMap<UUID, ITrack> potentialMap = new HashMap<UUID, ITrack>();

						// get the search area to find tracks that may belong
						// linked to the current one being processed
						span = Seconds.secondsIn(currentEvent.getTimePeriod()).getSeconds() / Utility.SECONDS_TO_DAYS;
						// set the time span to search in as the end of our
						// current track+the the span of the frame for the last
						// event in our current track being processed.
						Interval currentSearchTime = new Interval(currentEvent.getTimePeriod().getEnd(),
								currentEvent.getTimePeriod().toDuration());

						float[] motionVect = null;
						if (currentTrack.size() < 2) {
							searchArea = this.predictor.getSearchRegion(currentEvent.getBBox(), span);

						} else {
							motionVect = Utility.trackMovement(currentTrack);
							searchArea = this.predictor.getSearchRegion(currentEvent.getBBox(), motionVect, span);

						}
						List<ITrack> potentialTracks = this.tracksIdxr.search(currentSearchTime, searchArea);

						for (ITrack trk : potentialTracks) {
							if (!potentialMap.containsKey(trk.getUUID())
									&& trk.getStartTimeMillis() >= currentTrack.getEndTimeMillis()) {
								potentialMap.put(trk.getUUID(), trk);
							}
						}
						// search locations for potential matches up to the
						// maxFrameSkip away using the previously predicted
						// search area as the starting point for the next search
						// area.
						for (int j = 0; j < maxFrameSkip; j++) {

							// update the currentSearchTime to the next frame
							currentSearchTime = new Interval(currentSearchTime.getEnd(),
									currentSearchTime.toDuration());

							// if we don't have a motion vector then use diff
							// rotation
							if (motionVect == null) {
								// update search area for next frame
								Rectangle rect = searchArea.getBounds();
								searchArea = this.predictor.getSearchRegion(rect, span);

								// search next frame
								potentialTracks = this.tracksIdxr.search(currentSearchTime, searchArea);

							} else {
								// update search area for next frame using
								// motion vector
								Rectangle rect = searchArea.getBounds();
								searchArea = this.predictor.getSearchRegion(rect, motionVect, span);

								// search next frame
								potentialTracks = this.tracksIdxr.search(currentSearchTime, searchArea);

							}

							// put potential matches not in potentialTracks list
							// into the list
							for (ITrack trk : potentialTracks) {

								if (!potentialMap.containsKey(trk.getUUID())
										&& trk.getStartTimeMillis() >= currentTrack.getEndTimeMillis()) {
									potentialMap.put(trk.getUUID(), trk);
								}

							}
						}

						// if the map of potential matches has anything in it,
						// then we will process those tracks by adding them to
						// the graph problem
						if (potentialMap.size() >= 1) {
							for (ITrack tmpTrk : potentialMap.values()) {
								graph.addAssociationPossibility(currentTrack, tmpTrk);
							}
						}
					});
				}).get();
			} catch (InterruptedException | ExecutionException e) {
				e.printStackTrace();
			}

			// if the graph problem was solved then return the results
			System.out.println("Solving Association Problem.");
			if (graph.solve()) {
				System.out.println("Association Problem Solved, linking.");
				return graph.getTrackLinked();
			}
		}
		// if solve didn't work then just return the original list.
		return this.tracksIdxr.getAll();

	}

}