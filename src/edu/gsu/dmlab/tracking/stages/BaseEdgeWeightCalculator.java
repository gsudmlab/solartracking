/**
 * SolarTracking Copyright (C) 2017 Georgia State University
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.gsu.dmlab.tracking.stages;

import edu.gsu.dmlab.datatypes.interfaces.IEvent;
import edu.gsu.dmlab.datatypes.interfaces.ITrack;
import edu.gsu.dmlab.tracking.interfaces.ILocationProbCal;
import edu.gsu.dmlab.tracking.interfaces.IObsModel;
import edu.gsu.dmlab.tracking.stages.interfaces.IEdgeWeightCalculator;

/**
 * Base abstract class used for calculating edge weights on a DAG used for the
 * data association problem. Since different stages of
 * <a href="http://dx.doi.org/10.1016/j.ascom.2015.10.005">Kempton et al.</a>
 * use different components for edge weight calculation, the getEdgeProb method
 * must be implemented in classes derived from this one.
 * 
 * @author Dustin Kempton, Data Mining Lab, Georgia State University
 * @version {@value edu.gsu.dmlab.ConstValues#major_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#minor_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#build_ver}
 */
public abstract class BaseEdgeWeightCalculator implements IEdgeWeightCalculator {

	private ILocationProbCal enterLocProbCalc;
	private ILocationProbCal exitLocProbCalc;
	private IObsModel obsModel;

	private double entExitMult = 5.0;
	private double obsMult = 75.0;
	private double assocMult = 235.0;

	/**
	 * Constructor that uses the default values for entExitMult, obsMult, and
	 * assocMult.
	 * 
	 * @param enterLocProbCalc
	 *            The model used for calculating enter probability on a track.
	 * @param exitLocProbCalc
	 *            The model used for calculating exit probability on a track.
	 * @param obsModel
	 *            The model used for calculating the probability of a track
	 *            being plausible.
	 */
	public BaseEdgeWeightCalculator(ILocationProbCal enterLocProbCalc, ILocationProbCal exitLocProbCalc,
			IObsModel obsModel) {
		if (enterLocProbCalc == null)
			throw new IllegalArgumentException("Enter Probability Calculator cannot be null.");
		if (exitLocProbCalc == null)
			throw new IllegalArgumentException("Exit Probability Calculator cannot be null.");
		if (obsModel == null)
			throw new IllegalArgumentException("Observation Edge Model cannot be null.");
		this.enterLocProbCalc = enterLocProbCalc;
		this.exitLocProbCalc = exitLocProbCalc;
		this.obsModel = obsModel;
	}

	/**
	 * Constructor
	 * 
	 * @param enterLocProbCalc
	 *            The model used for calculating enter probability on a track.
	 * @param exitLocProbCalc
	 *            The model used for calculating exit probability on a track.
	 * @param obsModel
	 *            The model used for calculating the probability of a track
	 *            being plausible.
	 * @param entExitMult
	 *            The multiplier for enter and exit probabilities.
	 * @param obsMult
	 *            The multiplier for the observation edge.
	 * @param assocMult
	 *            The multiplier for the association edge.
	 */
	public BaseEdgeWeightCalculator(ILocationProbCal enterLocProbCalc, ILocationProbCal exitLocProbCalc,
			IObsModel obsModel, double entExitMult, double obsMult, double assocMult) {
		if (enterLocProbCalc == null)
			throw new IllegalArgumentException("Enter Probability Calculator cannot be null.");
		if (exitLocProbCalc == null)
			throw new IllegalArgumentException("Exit Probability Calculator cannot be null.");
		if (obsModel == null)
			throw new IllegalArgumentException("Observation Edge Model cannot be null.");
		this.enterLocProbCalc = enterLocProbCalc;
		this.exitLocProbCalc = exitLocProbCalc;
		this.obsModel = obsModel;
		this.entExitMult = entExitMult;
		this.obsMult = obsMult;
		this.assocMult = assocMult;
	}

	@Override
	public void finalize() throws Throwable {
		try {
			this.enterLocProbCalc = null;
			this.exitLocProbCalc = null;
			this.obsModel = null;
		} finally {
			super.finalize();
		}
	}

	@Override
	public double sourceEdgeWeight(IEvent event) {
		double entPd = this.enterLocProbCalc.calcProb(event);
		double entP = -(Math.log(entPd) * this.entExitMult);
		// System.out.println("Source Cost: " + entP);
		return entP;
	}

	@Override
	public double sinkEdgeWeight(IEvent event) {
		double exPd = this.exitLocProbCalc.calcProb(event);
		double exP = -(Math.log(exPd) * this.entExitMult);
		// System.out.println("Sink Cost: " + exP);
		return exP;
	}

	@Override
	public double observationEdgeWeight(IEvent event) {
		double retVal = this.obsModel.getObsProb(event);
		double obsCost = (Math.log((1 - retVal) / retVal) * this.obsMult);
		// System.out.println("Obs Cost: " + obsCost);
		return obsCost;
	}

	@Override
	public double associationEdgeWeight(ITrack leftTrack, ITrack rightTrack) {
		double prob = this.getEdgeProb(leftTrack, rightTrack);
		double weightValD = -(Math.log(prob) * this.assocMult);
		// System.out.println("Assoc Cost: " + weightValD);
		return weightValD;
	}

	protected abstract double getEdgeProb(ITrack leftTrack, ITrack rightTrack);

}