/**
 * SolarTracking Copyright (C) 2017 Georgia State University
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.gsu.dmlab.tracking.stages;

import edu.gsu.dmlab.factory.interfaces.IEventTrackingFactory;
import edu.gsu.dmlab.indexes.interfaces.IEventIndexer;
import edu.gsu.dmlab.indexes.interfaces.ITrackIndexer;
import edu.gsu.dmlab.util.interfaces.ISearchAreaProducer;

/**
 * The upper iterations of second stage in the iterative tracking algorithm of
 * Kempton et al., http://dx.doi.org/10.1016/j.ascom.2015.10.005.
 * 
 * @author Dustin Kempton, Data Mining Lab, Georgia State University
 * @version {@value edu.gsu.dmlab.ConstValues#major_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#minor_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#build_ver}
 */
public class StageThree extends BaseUpperStage {
	private static int stage = 3;

	/**
	 * Constructor
	 * 
	 * @param predictor
	 *            Used for predicting the location of an event given its current
	 *            location and an elapsed time period.
	 * @param factory
	 *            Factory used to create new objects needed to process the
	 *            tracks at this stage.
	 * @param tracksIdxr
	 *            Indexer of the current tracks to process in this stage.
	 * @param evntIdxr
	 *            Indexer of all events taken as input into the program at the
	 *            beginning. This is mostly used to determine the observation
	 *            probability in the association problem.
	 * @param maxFrameSkip
	 *            The maximum number of skipped frames allowed between a
	 *            detection and its possible association match.
	 * @param numThreads
	 *            The number of threads to use when processing batch. -1 for all
	 *            available > 0 for a specific number.
	 */
	public StageThree(ISearchAreaProducer predictor, IEventTrackingFactory factory, ITrackIndexer tracksIdxr,
			IEventIndexer evntIdxr, int maxFrameSkip, int numThreads) {
		super(predictor, factory, tracksIdxr, evntIdxr, maxFrameSkip, stage, numThreads);
	}

}
