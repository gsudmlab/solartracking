/**
 * SolarTracking Copyright (C) 2017 Georgia State University
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.gsu.dmlab.tracking.stages;

import edu.gsu.dmlab.datatypes.interfaces.IEvent;
import edu.gsu.dmlab.datatypes.interfaces.ITrack;
import edu.gsu.dmlab.factory.interfaces.IEventTrackingFactory;
import edu.gsu.dmlab.indexes.interfaces.IEventIndexer;
import edu.gsu.dmlab.tracking.stages.interfaces.IProcessingStage;
import edu.gsu.dmlab.util.Utility;
import edu.gsu.dmlab.util.interfaces.ISearchAreaProducer;
import org.joda.time.Interval;
import org.joda.time.Seconds;

import java.awt.Polygon;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;

/**
 * The first stage in the iterative tracking algorithm of Kempton et al.,
 * http://dx.doi.org/10.1016/j.ascom.2015.10.005. It uses the search area to
 * link events into tracks iff there is one and only one available event as a
 * possibility.
 * 
 * @author Dustin Kempton, Data Mining Lab, Georgia State University
 * @version {@value edu.gsu.dmlab.ConstValues#major_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#minor_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#build_ver}
 */
public class StageOne implements IProcessingStage {

	private ISearchAreaProducer searchAreaProducer;
	private IEventIndexer eventIndexer;
	private IEventTrackingFactory factory;
	private ForkJoinPool forkJoinPool = null;

	/**
	 * Constructor for stage one of tracking.
	 * 
	 * @param searchAreaProducer
	 *            The search area producer used to produce locations to search
	 *            for the next detection to match to.
	 * @param eventIndexer
	 *            An index object that contains the spatiotemporal location of
	 *            each object in the dataset.
	 * @param factory
	 *            The factory object used to create new track objects.
	 * @param numThreads
	 *            The number of threads to use when processing batch. -1 for all
	 *            available > 0 for a specific number.
	 */
	public StageOne(ISearchAreaProducer searchAreaProducer, IEventIndexer eventIndexer, IEventTrackingFactory factory,
			int numThreads) {
		if (eventIndexer == null)
			throw new InvalidParameterException("IEventIndexer cannot be null");
		if (searchAreaProducer == null)
			throw new InvalidParameterException("IPositionPredictor cannot be null");
		if (factory == null)
			throw new InvalidParameterException("Object Factory cannot be null");
		if (numThreads < -1 || numThreads == 0)
			throw new IllegalArgumentException("numThreads must be -1 or > 0 in BaseUpperStage constructor.");

		this.searchAreaProducer = searchAreaProducer;
		this.eventIndexer = eventIndexer;
		this.factory = factory;

		if (numThreads == -1) {
			this.forkJoinPool = new ForkJoinPool();
		} else {
			this.forkJoinPool = new ForkJoinPool(numThreads);
		}

	}

	@Override
	public void finalize() throws Throwable {
		try {
			this.searchAreaProducer = null;
			this.eventIndexer = null;
			this.factory = null;

			if (this.forkJoinPool != null) {
				this.forkJoinPool.shutdownNow();
				this.forkJoinPool = null;
			}
		} finally {
			super.finalize();
		}
	}

	@Override
	public ArrayList<ITrack> process() {
		/*
		 * for each event between start and end, find events that are in the
		 * next frame after the current one we are looking at. Then link an
		 * event and the current one together iff it is the only one in the
		 * search box determined by our position predictor and the length of the
		 * current event.
		 */

		List<IEvent> events = this.eventIndexer.getAll();

		try {
			this.forkJoinPool.submit(() -> {
				events.parallelStream().forEach(e -> linkToNext(e));
			}).get();
		} catch (InterruptedException | ExecutionException e) {
			e.printStackTrace();
		}

		// Now we determine how many tracks we have.
		HashMap<UUID, ITrack> tracksMap = new HashMap<>();
		for (int i = 0; i < events.size(); i++) {
			IEvent currEvent = events.get(i);
			while (currEvent.getPrevious() != null) {
				currEvent = currEvent.getPrevious();
			}

			// if it is already in the map then move along.
			UUID key = currEvent.getUUID();
			if (!tracksMap.containsKey(key)) {
				// create a track and add it to the map
				ITrack track = this.factory.getTrack(currEvent);
				tracksMap.put(key, track);
			}
		}

		ArrayList<ITrack> results = new ArrayList<ITrack>();

		tracksMap.forEach((id, track) -> {
			results.add(track);
		});
		return results;
	}

	private void linkToNext(IEvent event) {
		// only if the event isn't already linked to another event do we do
		// anything with it.
		if (event.getNext() == null) {
			// get the search are for the neighborhood
			double span = Seconds.secondsIn(event.getTimePeriod()).getSeconds() / Utility.SECONDS_TO_DAYS;
			Polygon nextSearchArea = this.searchAreaProducer.getSearchRegion(event.getBBox(), span);
			Polygon prevSearchArea = this.searchAreaProducer.getSearchRegionBack(event.getBBox(), span);
			// get the time interval for the neighborhood
			Interval time = event.getTimePeriod();
			Interval nextTime = new Interval(time.getEndMillis(), time.getEndMillis() + time.toDurationMillis());
			Interval prevTime = new Interval(time.getStartMillis() - time.toDurationMillis(), time.getStartMillis());

			// Get the events in the neighborhood
			List<IEvent> eventsInArea = this.eventIndexer.search(nextTime, nextSearchArea);
			List<IEvent> eventsInPrevArea = this.eventIndexer.search(prevTime, prevSearchArea);

			// Only if there is one do we process
			if (eventsInArea.size() == 1 && eventsInPrevArea.size() <= 1) {
				IEvent nextEvent = eventsInArea.get(0);
				// Only if the next event is not linked
				// do we link it to this event.
				if (nextEvent.getPrevious() == null) {
					event.setNext(nextEvent);
					nextEvent.setPrevious(event);
				}
			}
		}
	}
}
